const ZModule = require("../z-server").ZModule;
const SQLServer = require('../services/SQLServer').SQLServer;
const bcrypt = require('bcryptjs');
const uuidv4 = require('uuid/v4');
const nodemailer = require("nodemailer");
const smtpConfig = require("./Config").Config.instance.getConfig().smtp;
const adminConfig = require("./Config").Config.instance.getConfig().adminDefault;
let general = require("./Config").Config.instance.getConfig().general;
const fetch = require('node-fetch');
class Mantenedores extends ZModule {
    constructor() {
        super();
        this._transport = nodemailer.createTransport(smtpConfig);
    }
    static get instance() {
        if (!global.mantenedoresInstance) global.mantenedoresInstance = new Mantenedores();
        return global.mantenedoresInstance;
    }

    async getPrivilegiosSesion(zToken) {
        let rows;
        let params;
        try {
       
            let db = new SQLServer();
            params = {
                token: zToken
            }
              rows= await  db.executeDirect("EXEC [dbo].[PRO_getPrivilegiosSesion] @token = N'"+params.token+"'");  
                     
        } catch(error) {
            console.error(error);
            throw error;
        }
        if (!rows.length) throw "Sesión de Usuario inválida o caducada";
        let r = rows[0];
        if (r.activo != "S") throw "El usuario ha sido desactivado. Consulte al administrador del Sistema";
        return {login:r.login, admin:r.admin == "S"}
    }

//**************************************** R E P O R T E S ************************************************* */

    async getReportes(filtro) {
        try {
            let db = new SQLServer();            
            let params = {filtro : filtro.filtro}
            let rows = await  db.executeDirect("EXEC [dbo].[PRO_Busca_Reportes] @filtro=N'"+params.filtro+"'");
            return rows.map(r => {
                return {id:r.id, nombre:r.nombre, filtro:r.filtro, url:r.url, activo:r.activo == "S"}
            })
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async getReportesActivos() {
        try {
            let db = new SQLServer();            
            let rows = await  db.executeDirect("EXEC [dbo].[PRO_Busca_Reportes_Activos]");
            return rows.map(r => {
                return {id:r.id, nombre:r.nombre, filtro:r.filtro, url:r.url, activo:r.activo == "N"}
            })
        } catch(error) {
            console.error(error);
            throw error;
        }
    }
    async addReporte(zToken, reporte) {
        let p = await this.getPrivilegiosSesion(zToken);
        if (!p.admin) throw "Privilegios Insuficientes";

        let params;        
        try {
            let db = new SQLServer();            
            params = {
                nombre: reporte.nombre,
                filtro: reporte.filtro,
                url : reporte.url,
                activo:reporte.activo?"S":"N"
            }
            await  db.executeDirect("EXEC [dbo].[PRO_Add_Reporte] @nombre = N'"+params.nombre+"',@filtro = N'"+params.filtro+"', @url = N'"+params.url+"', @activo = N'"+params.activo+"'"); 
            return reporte;
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async saveReporte(zToken, reporte) {
        let params;
        let p = await this.getPrivilegiosSesion(zToken);
        if (!p.admin) throw "Privilegios Insuficientes";
        try {
            let db = new SQLServer();
            params = {
                id : reporte.id,
                nombre:reporte.nombre,
                filtro: reporte.filtro,
                url:reporte.url,
                activo:reporte.activo?"S":"N"
            }
            await  db.executeDirect("EXEC [dbo].[PRO_Update_Reporte] @nombre = N'"+params.nombre+"', @filtro = N'"+ params.filtro+"' ,@url = N'"+ params.url+"' , @activo = N'"+params.activo+"', @id =N'"+params.id+"'");           
            return reporte;
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async deleteReporte(zToken, reporte) {
        let p = await this.getPrivilegiosSesion(zToken);
        if (!p.admin) throw "Privilegios Insuficientes";
        
        try {
            let db = new SQLServer();            
            await  db.executeDirect("EXEC [dbo].[PRO_Delete_Reporte] @id = N'"+reporte.id+"'"); 

        } catch(error) {
            if (error.number >= 50001) throw error.message;
            console.error(error);
            throw error;
        }
    }

  

    //**************************************** S E R V I C I O S ************************************************* */

    async getServicios(filtro) {
        try {
            let db = new SQLServer();            
            let params = {filtro};

            let rows = await  db.executeDirect("EXEC [dbo].[PRO_Busca_Servicios] @filtro = N'"+params.filtro.filtro+"'");
            return rows.map(r => {
                if(params.filtro.clientecontrato){
                    return {id:r.id, nombre:r.nombre, filtro:r.filtro, activo:r.activo == "N"}
                }
                return {id:r.id, nombre:r.nombre, filtro:r.filtro, activo:r.activo == "S"}
            })
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async deleteServicio(zToken, servicio) {
        let p = await this.getPrivilegiosSesion(zToken);
        if (!p.admin) throw "Privilegios Insuficientes";

        try {
            let db = new SQLServer();            
            await  db.executeDirect("EXEC [dbo].[PRO_Delete_Servicio] @id = N'"+servicio.id+"'"); 

        } catch(error) {
            if (error.number >= 50001) throw error.message;
            console.error(error);
            throw error;
        }
    }

    async getServiciosPorUsuario(usuario){
        try {
            let db = new SQLServer();
            let rows = await  db.executeDirect("EXEC [dbo].[PRO_Get_Servicios_Usuario] @idUsuario = N'"+usuario.idUsuario+"'");
            return rows.map( r => {
                return {id:r.ID, nombre:r.NOMBRE,filtro:'', activo:r.activo ="S" }
            })
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async getReportesAsocServicioUsuario(usuario, servicio){
        try {
            let db = new SQLServer();
            let rows = await  db.executeDirect("EXEC [dbo].[PRO_Get_Reportes_Servicios_Usuario] @idUsuario = N'"+usuario.idUsuario+"', @idServicio = N'"+servicio.id+"'");
            return rows.map( r => {
                return {id:r.ID, nombre:r.NOMBRE,filtro:'', activo:r.activo ="S" }
            })
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async getServiciosClienteContrato(clientecontrato) {
        try {
            let db = new SQLServer();
            let params = {
                idCliente : clientecontrato.idCliente,
                idContrato: clientecontrato.idContrato
            };
            let rows = await  db.executeDirect("EXEC [dbo].[PRO_Busca_Servicios_Cliente_Contrato] @idCliente = N'"+params.idCliente+"' , @idContrato = N'"+params.idContrato+"'");
            return rows.map( r => {
                return {id:r.ID_SERVICIO, nombre:r.NOMBRE,filtro:'', activo:r.activo ="S" }
            })
        } catch(error) {
            console.error(error);
            throw error;
        }
    }
    async getServiciosClienteContratoMan(clientecontrato) {
        try {
            let db = new SQLServer();
            let params = {
                idCliente : clientecontrato.idCliente,
                idContrato: clientecontrato.idContrato
            };
            let rows = await  db.executeDirect("EXEC [dbo].[PRO_Busca_Servicios_Cliente_Contrato_Man] @idCliente = N'"+params.idCliente+"' , @idContrato = N'"+params.idContrato+"'");
            return rows.map( r => {
                return {id:r.ID, nombre:r.NOMBRE, activo:r.ID_SERVICIO?true:false}
            })
        } catch(error) {
            console.error(error);
            throw error;
        }
    }
    async getServiciosSinReportes() {
        try {
            let db = new SQLServer();            
            let rows = await  db.executeDirect("EXEC [dbo].[PRO_Busca_Servicios_Sin_Reportes]");
            return rows.map(r => {
                return {id:r.ID, nombre:r.NOMBRE}
            })
        } catch(error) {
            console.error(error);
            throw error;
        }
    }
    async getServiciosConReportes(filtro) {
        try {
            let db = new SQLServer();    
            let params = {
                filtro
            }  
            let rows = await  db.executeDirect("EXEC [dbo].[PRO_Busca_Servicios_Con_Reportes] @filtro = N'"+params.filtro.filtro+"'");
            return rows.map(r => {
                return {id:r.ID, nombre:r.NOMBRE, activo: "S"}
            })
        } catch(error) {
            console.error(error);
            throw error;
        }
    }
    async addServicio(zToken,servicio) {
        let params;  
        let p = await this.getPrivilegiosSesion(zToken);
        if (!p.admin) throw "Privilegios Insuficientes";      
        try {
            let db = new SQLServer();            
            params = {
                nombre: servicio.nombre,
                activo:servicio.activo?"S":"N"
            }
            await  db.executeDirect("EXEC [dbo].[PRO_Add_Servicio] @nombre = N'"+params.nombre+"', @activo = N'"+params.activo+"'",params); 
            return servicio;
        } catch(error) {
            console.error(error);
            throw error;
        }
    }
    async saveServicio(zToken, servicio) {
        let p = await this.getPrivilegiosSesion(zToken);
        if (!p.admin) throw "Privilegios Insuficientes";
        try {
            let db = new SQLServer();
            let params = {
                id : servicio.id,
                nombre:servicio.nombre,
                activo:servicio.activo?"S":"N"
            }
            await  db.executeDirect("EXEC [dbo].[PRO_Update_Servicio] @nombre = N'"+params.nombre+"', @activo = N'"+params.activo+"', @id =N'"+params.id+"'");           
            return servicio;
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async getReportesPorServicio(servicio){
        try {
            let db = new SQLServer();
            let params = {
                idServicio : servicio.id,
            }
            let rows =  await  db.executeDirect("EXEC [dbo].[PRO_Get_Reportes_Por_Servicio] @idServicio = N'"+params.idServicio+"'");           
            return rows.map(r => {
                return {id:r.id, nombre:r.nombre, filtro: r.filtro, url: r.url, activo:r.activo == "S"}
            })
        } catch(error) {
            console.error(error);
            throw error;
        }
    }
    async getReportesPorServicioAdm(servicio){
        try {
            let db = new SQLServer();
            let params = {
                idServicio : servicio.id
            }
            let rows =  await  db.executeDirect("EXEC [dbo].[PRO_Get_Reportes_Por_Servicio_Adm] @idServicio = N'"+params.idServicio+"'");           
            return rows.map(r => {
                return {id:r.id, nombre:r.nombre, filtro: r.filtro, url: r.url, activo:r.activo == "N"}
            })
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async getReportesPorUsuario(usuario){
        try {
            let db = new SQLServer();
            let params = {
                idServicio : usuario.idServicio,
                idUsuario: usuario.idUsuario
            }
            let rows =  await  db.executeDirect("EXEC [dbo].[PRO_Get_Reportes_Por_Usuario] @idServicio = N'"+params.idServicio+"', @idUsuario = N'"+params.idUsuario+"'");           
            return rows.map(r => {
                return {id:r.ID,nombre:r.NOMBRE, activo: r.ID_CCSR?true:false}
            })
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async addServicioReporte(zToken, servicio) {
        let p = await this.getPrivilegiosSesion(zToken);
        if (!p.admin) throw "Privilegios Insuficientes";
        
        try {
            let params = {
                idServicio: servicio.idServicio,
            }
            let db = new SQLServer();            
            servicio.listaReportes.forEach(reporte => {
                db.executeDirect("EXEC [dbo].[PRO_Add_Servicio_Reporte] @idServicio = N'"+params.idServicio+"', @idReporte = N'"+reporte+"'"); 
            });
            return servicio;
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async saveServicioReporte(zToken, servicio) {
        let rows;
        let p = await this.getPrivilegiosSesion(zToken);
        if (!p.admin) throw "Privilegios Insuficientes";
        try {
            let db = new SQLServer();

            if(servicio.listaReportes){
                servicio.listaReportes.forEach(reporte => {
                     db.executeDirect("EXEC [dbo].[PRO_Update_Reportes_Por_Servicio] @idServicio = N'"+servicio.idServicio+"', @idReporte = N'"+reporte+"'");           
                });
            }
            return servicio;
        } catch(error) {
            console.error(error);
            throw error;
        }
    }



    /*************************************  CLIENTE CONTRATO ***********************************/

    async getClientesConContrato(filtro) {
        try {
            let db = new SQLServer();   
            let rows = await  db.executeDirect("EXEC [dbo].[PRO_Busca_Clientes_Con_Contratos]");
            let fullclientescontratos =await  this.getFullClienteContrato();
            let reg = [];
            
            await rows.map(async  r => {
                let item = await fullclientescontratos.find( cc => cc.idCli == r.id_cliente && cc.idCon == r.id_contrato );
                if(item){
                    reg.push({idCliente:item.idCli, nombreCliente:item.nombreCli, idContrato: item.idCon, nombreContrato: item.nombreCon, activo: true});
                }else{
                    reg.push({idCliente:r.id_cliente, nombreCliente:'NO NAME', idContrato: r.id_contrato, nombreContrato: 'NO NAME', activo:false});
                }
            })     
            if(filtro.filtro == ''){
                return reg;
            }else{
                
                return  reg.filter(r => filtro.filtro.match(r.nombreCliente)); 
            }
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async saveClienteContratoServicio(zToken,clientecontratoservicio){
        let rows;
        let p = await this.getPrivilegiosSesion(zToken);
        if (!p.admin) throw "Privilegios Insuficientes";
        try {
            let db = new SQLServer();
            let params = {
                idCliente : clientecontratoservicio.idCliente,
                idContrato : clientecontratoservicio.idContrato,
                listaServicios : clientecontratoservicio.listaServicios
            }
            if(clientecontratoservicio.listaServicios){
                clientecontratoservicio.listaServicios.forEach(servicio => {
                     db.executeDirect("EXEC [dbo].[PRO_Update_Cliente_Reporte_Servicio] @idCliente = N'"+params.idCliente+"', @idContrato = N'"+params.idContrato+"', @idServicio = N'"+servicio+"'");           
                });
            }
            
            return {idCliente: params.idCliente, nombreCliente: clientecontratoservicio.nomCliente, idContrato: clientecontratoservicio.idContrato , nombreContrato: clientecontratoservicio.nomContrato, activo :'S'};
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async getClientesContratoSinServicios() {
        try {
            return this.getClientes();
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async deleteClienteContratoServicio(zToken, clientecontratoservicio){
        let p = await this.getPrivilegiosSesion(zToken);
        if (!p.admin) throw "Privilegios Insuficientes";
        try {
            let db = new SQLServer();            
            await db.executeDirect("EXEC [dbo].[PRO_Delete_Cliente_Contrato_Servicio] @idCliente = N'"+clientecontratoservicio.idCliente+"', @idContrato = N'"+clientecontratoservicio.idContrato+"'"); 

        } catch(error) {
            if (error.number >= 50001) throw error.message;
            console.error(error);
            throw error;
        }
    }
    async getAllContratosPorCliente(cliente){
        try{
            console.log(cliente.id);
            let contratos = await this.getContratos(cliente.id);
            return contratos;
        }catch(error){
            console.log(error);
            throw error;
        }
    }

    async getContratosPorClienteDisp(cliente){
        try{
            let db = new SQLServer();
            let rows = await  db.executeDirect("EXEC [dbo].[PRO_Busca_Clientes_Con_Contratos]");
            let contratos = await this.getContratos(cliente.id);
            let clienConDisponible = [];

            await contratos.map(async contrato => {
                let item = await  rows.filter(row => row.id_cliente == cliente.id && row.id_contrato == contrato.id);
                if(item.length == 0 ) clienConDisponible.push(contrato);
            });
            
            return clienConDisponible;
        }catch(error){
            console.log(error);
            throw error;
        }
    }
    async getClientes(){
        return fetch(general.urlPreimportacones+'/getClientes')
        .then(res => res.text())
        .then(json => {return json}); 
                 
        //return [{idCli : 1, nombreCli :'WOM'},{idCli : 2, nombreCli :'MOVISTAR'},{idCli : 3, nombreCli :'ENTEL'},{idCli:4 , nombreCli:'CLARO'}]
    }

    async getContratos(idCliente){
        return fetch(general.urlPreimportacones+'/getClienteContratos'+'?idCliente='+idCliente)
        .then(res => res.text())
        .then(json => {console.log(json) ;return JSON.parse(json);});  

        /*try{
            let contratosCliente = [{
                                    id: 1, 
                                    contratos :[{idCon : 1, nombreCon :'Ventas'},
                                                {idCon : 2, nombreCon :'Comercio'},
                                                {idCon : 3, nombreCon :'Marketing'}]
                                },{
                                    id:2,
                                    contratos :[{idCon : 1, nombreCon :'Ventas'},
                                                {idCon : 3, nombreCon :'Marketing'}]
                                },{
                                    id:3,
                                    contratos :[{idCon : 1, nombreCon :'Ventas'}]
                                },{
                                    id:4,
                                    contratos: [{idCon : 2, nombreCon :'Comercio'}]
                                }];
            
            let contratos = [{idCon : 1, nombreCon :'Ventas'},
                             {idCon : 2, nombreCon :'Comercio'},
                             {idCon : 3, nombreCon :'Marketing'},
                             {idCon : 4, nombreCon : 'Recursos Humanos'}]

            if(idCliente){
                let contratoFiltrado = await contratosCliente.filter(contratofilter => contratofilter.id == idCliente);
                return contratoFiltrado[0].contratos;
            }
             
            return contratos;
            
        }catch(error){
            console.log(error);
            throw error;
        }*/
    }

    async getFullClienteContrato(){
        return fetch(general.urlPreimportacones+'/getFullClientesContrato')
        .then(res => res.text())
        .then(json => {console.log(json) ;return JSON.parse(json);});  

       /* try{
            return [
                {idCli : 1, nombreCli :'WOM', idCon: 1, nombreCon:'Ventas'},
                {idCli : 1, nombreCli :'WOM', idCon: 2, nombreCon:'Comercio'},
                {idCli : 1, nombreCli :'WOM', idCon: 3, nombreCon:'Marketing'},
                {idCli : 2, nombreCli :'MOVISTAR', idCon: 1, nombreCon:'Ventas'},
                {idCli : 3, nombreCli :'ENTEL', idCon: 1, nombreCon:'Ventas'},
                {idCli : 3, nombreCli :'ENTEL', idCon: 2, nombreCon:'Comercio'},
                {idCli : 3, nombreCli :'ENTEL', idCon: 3, nombreCon:'Marketing'},
                {idCli : 4, nombreCli :'CLARO', idCon: 2, nombreCon:'Comercio'}
                ]
        }catch(error){
            console.log(error);
            throw error;
        }*/
    }



    /*************************************** CLIENTE CONTRATO SERVICIO ************************************************** */


    async addClienteContratoServicio(zToken, clientecontratoservicio){
        let p = await this.getPrivilegiosSesion(zToken);
        if (!p.admin) throw "Privilegios Insuficientes";
        try {
            let params = {
                idCliente : clientecontratoservicio.idCliente,
                idContrato : clientecontratoservicio.idContrato,
                listaServicios: clientecontratoservicio.listaServicios
            }
            let db = new SQLServer();            
            params.listaServicios.forEach(servicio => {
                db.executeDirect("EXEC [dbo].[PRO_Add_Cliente_Contrato_Servicio] @idCliente = N'"+params.idCliente+"', @idContrato = N'"+params.idContrato+"', @idServicio = N'"+servicio+"'"); 
            });
            return {};
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    /********************************************** HEADERS ****************************************************/
    async getHeaders(filtro){
        try {
            let params = {
                filtro
            }
            let db = new SQLServer();            
            let rows = await db.executeDirect("EXEC [dbo].[PRO_Get_Headers] @filtro = N'"+params.filtro+"'"); 
            return rows.map( h => {
                console.log(h);
                return {id: h.ID_HEADER, nombre: h.NOMBRE_HEADER, descripcion: h.DESCRIPCION, formato: h.FORMATO, tipo: h.TIPO, activo:h.ACTIVO =="S"}
            });
        } catch(error) {
            console.error(error);
            throw error;
        }
    }
    async addHeader(zToken,header){
        let p = await this.getPrivilegiosSesion(zToken);
        if (!p.admin) throw "Privilegios Insuficientes";
        try {
            let params = {
                nombre: header.nombre,
                tipo: header.tipo,
                formato: header.formato,
                descripcion: header.descripcion,
                activo: header.activo?'S':'N'
            }
            let db = new SQLServer();            
            await db.executeDirect("EXEC [dbo].[PRO_Add_Header] @nombre = N'"+params.nombre+"', @tipo = N'"+params.tipo+"', @formato = N'"+params.formato+"', @descripcion = N'"+params.descripcion+"', @activo = N'"+params.activo+"'"); 
            return header;
        } catch(error) {
            console.error(error);
            throw error;
        }

    }
    async saveHeader(zToken,header){
        let p = await this.getPrivilegiosSesion(zToken);
        if (!p.admin) throw "Privilegios Insuficientes";
        try {
            let db = new SQLServer();
            let params = {
                id : header.id,
                nombre: header.nombre,
                tipo:header.tipo,
                formato: header.formato,
                descripcion:header.descripcion,
                activo:header.activo?"S":"N"
            }
            await  db.executeDirect("EXEC [dbo].[PRO_Update_Header] @id = N'" + params.id + "', @nombre = N'"+params.nombre+"', @formato = N'"+params.formato+"', @tipo = N'"+params.tipo+"', @descripcion = N'"+ params.descripcion+"', @activo = N'"+params.activo+"'");      
             
            return header;
        } catch(error) {
            console.error(error);
            throw error;
        }
    }
    async deleteHeader(zToken,header){
        let p = await this.getPrivilegiosSesion(zToken);
        if (!p.admin) throw "Privilegios Insuficientes";
        try {
            let db = new SQLServer();            
            await  db.executeDirect("EXEC [dbo].[PRO_Delete_Header] @id = N'"+header.id+"'"); 
        } catch(error) {
            if (error.number >= 50001) throw error.message;
            console.error(error);
            throw error;
        }
    }

        /********************************************** MAPEOS DATOS ****************************************************/

        async getMapeos(filtro){
            try {
                let params = {
                    filtro
                }
                let db = new SQLServer();            
                let rows = await db.executeDirect("EXEC [dbo].[PRO_Get_Mapeos] @filtro = N'"+params.filtro+"'"); 
                console.log("rows dasd :", rows);
                return rows.map( m => {
                    console.log(m);
                    return {id: m.COD_SERVICIO, nombre: m.NOMBRE_SERVICIO, activo: m.ACTIVO}
                });
            } catch(error) {
                console.error(error);
                throw error;
            }
        }

        async getMapeoSinHeader(){
            try {
                let db = new SQLServer();            
                let rows = await db.executeDirect("EXEC [dbo].[PRO_Get_Mapeos_Sin_Header]"); 
                return rows.map( m => {
                    console.log(m);
                    return {id: m.COD_SERVICIO, nombre :m.NOMBRE_SERVICIO, activo:m.ACTIVO =="S"}
                });
            } catch(error) {
                console.error(error);
                throw error;
            }
        }
        async getHeaderActivos() {
            try {
                let db = new SQLServer();            
                let rows = await  db.executeDirect("EXEC [dbo].[PRO_Busca_Headers_Activos]");
                return rows.map(r => {
                    return {id:r.ID_HEADER, nombre:r.NOMBRE_HEADER, descripcion: r.DESCRIPCION,  activo:r.activo == "N"}
                })
            } catch(error) {
                console.error(error);
                throw error;
            }
        }

        async addMapeoHeader(zToken, maphead){
            let p = await this.getPrivilegiosSesion(zToken);
            if (!p.admin) throw "Privilegios Insuficientes";
            try {
                let params = {
                    idMapeo: maphead.idMapeo,
                    listaHeaders : maphead.listaHeaders
                }
                let db = new SQLServer();            
                maphead.listaHeaders.forEach(header => {
                    db.executeDirect("EXEC [dbo].[PRO_Add_Mapeo_Header] @idMapeo = N'"+params.idMapeo+"', @idHeader = N'"+header+"'"); 
                });
                return maphead;
            } catch(error) {
                console.error(error);
                throw error;
            }
        }

        async getHeaderAsocMapeo(mapeo){
            try {
                let db = new SQLServer();
                let params = {
                    idMapeo : mapeo.id,
                }
                let rows =  await  db.executeDirect("EXEC [dbo].[PRO_Get_Header_Por_Mapeo] @idMapeo = N'"+params.idMapeo+"'");           
                console.log(rows);
                return rows.map(r => {
                    return {id : r.ID_HEADER, nombre: r.NOMBRE_HEADER, descripcion : r.DESCRIPCION, activo:r.ACTIVO =='S'}
                })
            } catch(error) {
                console.error(error);
                throw error;
            }
        }

        async saveMapeoHeader(zToken,maphead) {
            let p = await this.getPrivilegiosSesion(zToken);
            if (!p.admin) throw "Privilegios Insuficientes";
            let rows;
            try {
                let db = new SQLServer();

                if(maphead.listaHeaders){
                    maphead.listaHeaders.forEach(header => {
                         db.executeDirect("EXEC [dbo].[PRO_Update_Mapeo_Header] @idMapeo = N'"+maphead.idMapeo+"', @idHeader = N'"+header+"'");           
                    });
                }
                return {id : maphead.idMapeo, nombre: maphead.nombre, activo : 'S'};
            } catch(error) {
                console.error(error);
                throw error;
            }
        }

        async deleteMapeo(zToken,maphead){
            let p = await this.getPrivilegiosSesion(zToken);
            if (!p.admin) throw "Privilegios Insuficientes";
            try {
                let db = new SQLServer();            
                await  db.executeDirect("EXEC [dbo].[PRO_Delete_Mapeo_Header] @idMapeo = N'"+maphead.id+"'"); 
            } catch(error) {
                if (error.number >= 50001) throw error.message;
                console.error(error);
                throw error;
            }
        }

        /********************************************** MAPEOS CliConSer DATOS ****************************************************/
        async addMapeoClienteContratoServicio(zToken, mccs){
            let p = await this.getPrivilegiosSesion(zToken);
            if (!p.admin) throw "Privilegios Insuficientes";
            try {
                let db = new SQLServer();   
                let params = {
                    codigo: mccs.codigo,
                    nombre: mccs.nombre,
                    cliente: mccs.cliente,
                    contrato: mccs.contrato,
                    tabla: mccs.tabla,
                    activo: mccs.activo?'S':'N'
                }       
                let rows = await db.executeDirect("EXEC [dbo].[PRO_Busca_Mapeo_Cliente_Contrato_Servicio] @codigo = N'"+params.codigo+"'"); 
                
                if(rows.length>0) throw "Ya existe un Mapeo Cliente contrato servicio con el mismo Código";
                await db.executeDirect("EXEC [dbo].[PRO_Add_Mapeo_Cliente_Contrato_Servicio] @codigo = N'"+params.codigo+"', @nombre = N'"+params.nombre+"', @idCliente = N'"+params.cliente+"', @idContrato = N'"+params.contrato+"', @tabla = N'"+params.tabla+"', @activo = N'"+params.activo+"'"); 
                return mccs;
            } catch(error) {
                console.error(error);
                throw error;
            }
        }

        async saveMapeoClienteContratoServicio(zToken, mccs){
            let p = await this.getPrivilegiosSesion(zToken);
            if (!p.admin) throw "Privilegios Insuficientes";
            try {
                let db = new SQLServer();   
                let params = {
                    codigo: mccs.codigo,
                    nombre: mccs.nombre,
                    cliente: mccs.cliente,
                    contrato: mccs.contrato,
                    tabla: mccs.tabla,
                    activo: mccs.activo?'S':'N'
                }       
                await db.executeDirect("EXEC [dbo].[PRO_Update_Mapeo_Cliente_Contrato_Servicio] @codigo = N'"+params.codigo+"', @nombre = N'"+params.nombre+"', @idCliente = N'"+params.cliente+"', @idContrato = N'"+params.contrato+"', @tabla = N'"+params.tabla+"', @activo = N'"+params.activo+"'"); 
                return {id:mccs.codigo, nombreServicio:mccs.nombre, nombreTabla: mccs.tabla, idCliente: mccs.cliente, idContrato :mccs.contrato,activo: mccs.activo };
            } catch(error) {
                console.error(error);
                throw error;
            }
        }

        async getMapeoClienteContratoServicio(filtro){
            try {
                let params = {
                    filtro
                }
                let db = new SQLServer();            
                let rows = await db.executeDirect("EXEC [dbo].[PRO_Get_Mapeo_Cliente_Contrato_Servicio] @filtro = N'"+params.filtro+"'"); 
                return rows.map( h => {
                    return {id: h.COD_SERVICIO, nombreServicio: h.NOMBRE_SERVICIO,nombreTabla: h.NOMBRE_TABLA, idCliente: h.ID_CLIENTE, idContrato: h.ID_CONTRATO, activo:h.ACTIVO =="S"}
                });
            } catch(error) {
                console.error(error);
                throw error;
            }
        }
        async deleteMapeoClienteContratoServicio(zToken, mccs){
            let p = await this.getPrivilegiosSesion(zToken);
            if (!p.admin) throw "Privilegios Insuficientes";

            try {
                console.log(mccs);
                let params = {
                    id : mccs.id
                }
                let db = new SQLServer();            
                await db.executeDirect("EXEC [dbo].[PRO_Delete_Mapeo_Cliente_Contrato_Servicio] @codigo = N'"+params.id+"'"); 
                
            } catch(error) {
                console.error(error);
                throw error;
            }
        }

        async getClientesDisponibles(){
            try {
                let db = new SQLServer();            
                let fullclicon = await this.getFullClienteContrato();
                let rows = await db.executeDirect("EXEC [dbo].[PRO_Get_Mapeo_Cliente_Contrato_Servicio] @filtro= N''"); 

                await rows.map(row => { 
                    let posicion = fullclicon.findIndex(r=> r.idCli == row.ID_CLIENTE && r.idCon == row.ID_CONTRATO);
                    fullclicon.splice(posicion,1);
                });
                
                const result = Array.from(new Set(fullclicon.map(s => s.idCli)))
                .map(id =>{
                    return {
                        idCli: id,
                        nombreCli: fullclicon.find(s=> s.idCli === id).nombreCli
                    };
                });
                console.log(result);
                return result;

            } catch(error) {
                console.error(error);
                throw error;
            }
        }

        async getContratosDisponibles(cliente){
            try {
                let db = new SQLServer();            
                let fullclicon = await this.getFullClienteContrato();
                let rows = await db.executeDirect("EXEC [dbo].[PRO_Get_Mapeo_Cliente_Contrato_Servicio] @filtro= N''"); 
                await rows.map(row => { 
                    let posicion = fullclicon.findIndex(r=> r.idCli == row.ID_CLIENTE && r.idCon == row.ID_CONTRATO);
                    fullclicon.splice(posicion,1);
                });
                let filtro = fullclicon.filter(f => f.idCli == cliente.id);
                return filtro.map( f => {
                    return{ idCon: f.idCon, nombreCon : f.nombreCon}
                })
            } catch(error) {
                console.error(error);
                throw error;
            }
        }
}

exports.Mantenedores = Mantenedores;