const ZModule = require("../z-server").ZModule;
const SQLServer = require('../services/SQLServer').SQLServer;
const Mantenedores = require('./Mantenedores').Mantenedores.instance;
const bcrypt = require('bcryptjs');
const uuidv4 = require('uuid/v4');
const nodemailer = require("nodemailer");
const smtpConfig = require("./Config").Config.instance.getConfig().smtp;
const cnfPwd = require("./Config").Config.instance.getConfig().configPassword;

const adminConfig = require("./Config").Config.instance.getConfig().adminDefault;
let general = require("./Config").Config.instance.getConfig().general;

class Seguridad extends ZModule {
    constructor() {
        super();
        this._transport = nodemailer.createTransport(smtpConfig);
    }
    static get instance() {
        if (!global.seguridadInstance) global.seguridadInstance = new Seguridad();
        return global.seguridadInstance;
    }

    async inicializa() {
        let params;
        let con;
        let db = new SQLServer();
        params = {};
  
         // Asegurar que exista un administrador activo. Si no existe, se crea admin/admin
        let admins = await db.executeDirect("EXEC [dbo].[PRO_existeAdministrador]");
        if (!admins.length || parseInt(admins[0].n) === 0) {
            // Verificar que no exista un usuario 'admin'. Si existe, se activa, se hace administrador y se cambia su clave a 'admin'
            params = {
                login : adminConfig.login
            }
            let admin = await db.executeDirect("EXEC [dbo].[PRO_Get_Administrador] @login = N'"+params.login+"'");

            if (!admin.length || parseInt(admin[0].n) === 0) {
                // Crear administrador
                let adminPwd = await this.encript(adminConfig.login);
                params = {
                    login	: adminConfig.login,
	                nombres : adminConfig.nombre,
	                password: adminPwd,
                    email   : adminConfig.email
                }

                await await db.executeDirect("EXEC [dbo].[PRO_Add_Administrador] @login = N'"+params.login+"',@nombres = N'"+params.nombres+"',@password = N'"+params.password+"',@email = N'"+params.email+"'");
                
            } else {
                // Actualizar Administrador
                params = {
                    login	: adminConfig.login,
                }
                await await db.executeDirect("EXEC [dbo].[PRO_Save_Administrador] @login = N'"+params.login+"'");
            }
        }     
    }

    encript(pwd) {
        return new Promise((onOk, onError) => {
            bcrypt.hash(pwd, 8, (err, hash) => {
                if (err) onError(err);
                else onOk(hash);
            });
        });
    }

    compareWithEncripted(pwd, hash) {
        return bcrypt.compare(pwd, hash);
    }

    async _creaTokenRecuperacion(login) {
        try {
            let token = uuidv4();
            let params;
            let con;
           
            let db = new SQLServer();
            params = {
                token: token,
                login:login
            }
            let rows = await db.executeDirect("EXEC [dbo].[PRO_Token_Recuperacion] @token = N'"+params.token+"', @login = N'"+params.login+"'");          
            return token;      
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async _getUsuarioPorLoginOEmail(loginOEmail) {
        try {
            let db = new SQLServer();
            let params = {
                login: loginOEmail
            }
            let rows = await db.executeDirect("EXEC [dbo].[PRO_GetUsuarioLoginoEmail] @login = N'"+params.login+"'");
            if (!rows.length) return null;
            return rows[0];
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async login(login, pwd) {
        let rowsPrivilegios;

        if(!this.validatePwd(pwd)) throw "La contraseña no cumple con el formato establecido.";
        let db = new SQLServer();
        const mensajeInvalido = "Usuario, Contraseña o tipo Inválidos";
        let u = await this._getUsuarioPorLoginOEmail(login);
        
        if (!u) throw mensajeInvalido;
        if (u.ACTIVO != "S") throw "El usuario ha sido desactivado. Consulte al administrador del Sistema";
        if (!(await this.compareWithEncripted(pwd, u.PASSWORD))) throw mensajeInvalido;
        // Buscar si existe una sesion activa para retornarla
        let params = {
            login: login 
        }
        let rows = await db.executeDirect("EXEC [dbo].[PRO_GetSesionUsuario] @login = N'"+params.login+"'");
            let sesion = {};
            if (rows.length){
                sesion.token = rows[0].token;
            } else {
                let token = uuidv4();
                params = {
                    login: login,
                    token: token
                }
                let rows = await db.executeDirect("EXEC [dbo].[PRO_Set_Sesion] @login = N'"+params.login+"', @token = N'"+params.token+"'");              
                sesion.token = token;
            }
            params = {
                idUsuario: u.ID==null ? -1 : u.ID
            }  
            sesion.usuario = { id: u.ID, login: u.LOGIN, nombre: u.NOMBRES, email: u.EMAIL, cliente: u.CLIENTE, contrato :u.CONTRATO,  admin: u.ES_ADMIN == "S", tipo: u.TIPO };
            return sesion;
    }

    async validatePwd(pwd){
        // console.log("cantidadMinusculas : ", cnfPwd.cantidadMinusculas);
        // console.log("cantidadMayusculas : ", cnfPwd.cantidadMayusculas);
        // console.log("cantidadNum :",cnfPwd.cantidadNum);
        // console.log("listadoCaracteresEsp : ", cnfPwd.listadoCaracteresEsp);
        // console.log("minCaracteresEspeciales :", cnfPwd.minCaracteresEspeciales);
        // console.log("minLarge :", cnfPwd.minLarge);
        // console.log("maxLarge :", cnfPwd.maxLarge);
        
        var regex = new RegExp('/^(?=.*[a-z]){'+cnfPwd.cantidadMinusculas+'}(?=.*[A-Z]){'+cnfPwd.cantidadMayusculas+'}(?=.*\d){'+cnfPwd.cantidadNum+'}(?=.*['+cnfPwd.listadoCaracteresEsp+']){'+cnfPwd.minCaracteresEspeciales+'}([A-Za-z\d'+cnfPwd.listadoCaracteresEsp+']){'+cnfPwd.minLarge+','+cnfPwd.maxLarge+'}$/');
      return regex.test(pwd);
    }

    async generaTokenRecuperacion(login) {
        let tokenRecuperacion;
        let usuario = await this._getUsuarioPorLoginOEmail(login);
        if (!usuario) throw "No se encontró ningún usuario con la identificación o dirección de correo indicado";
        if(usuario.ACTIVO=='N')throw "El usuario se encuentra inactivo en el sistema.";
        try {
            tokenRecuperacion = await this._creaTokenRecuperacion(usuario.LOGIN);
        } catch(error) {
            throw "No se puede crear el token de recuperación:" + error.toString();
        }
        try {
            let url = general.urlSitioCorreo + "?recupera=" + tokenRecuperacion;
            
            await this._sendMail(usuario.EMAIL, "Recuperación de Contraseña en Sistema", null, 
                "<html><body><hr />" +
                "<p><b>Sr(a). " + usuario.NOMBRES + ":</b></p>" +
                "<p>Se ha solicitado la creación de una nueva contraseña en el Sistema asociada a esta dirección de correo electrónico. Si usted no lo ha solicitado, sólo ignore este mensaje.</p>" + 
                "<p>Su identificación de usuario (login) en el sistema es <b>" + usuario.LOGIN + "</b></p>" +
                "<p>Para crear su nueva contraseña, por favor haga click en <a href='" + url + "'>este enlace</a></p>" +
                "<hr /><p><i>Este es un correo automático del Sistema, por favor no lo responda.</i></p></body></html>"
            );                
        } catch(error) {
            throw "No se puede enviar el correo al nuevo usuario:" + error.toString();
        }
    } 

    _sendMail(to, subject, text, html) {
        
        return new Promise((onOk, onError) => {
            let message = {
                from:smtpConfig.from,
                subject:subject,
                to:to,
                text:text,
                html:html
            }
            this._transport.sendMail(message, (err, info) => {

                if (err) onError(err);
                onOk(info);
            });
        })
    }

    async getInfoRecuperacion(token) {
        try {
            let params;
            let db = new SQLServer();
           
            params = {
                token: token 
            }

            let rows = await db.executeDirect("EXEC [dbo].[PRO_InfoRecuperacion] @token = N'"+params.token+"'");  

            if (!rows.length) throw "El token de recuperación de contraseña es inválido o ha caducado. Debe generar uno nuevo (opción 'Olvidé mi Contraseña' en la página inicial del sistema) o contactarse con el administrador del sistema";
            let u = rows[0];
            return {
                login:u.login, nombre:u.nombre
            }
        } catch(error) {
            console.error(error);
            throw error;
        }
    }


    async recuperaPwd(token, pwd1, pwd2) {
        try {
            let params;
            let db = new SQLServer();

            let info = await this.getInfoRecuperacion(token);
            if(!this.validatePwd(pwd1)) throw "La contraseña no cumple con el formato establecido.";
            
            if (pwd1 != pwd2) throw "La contraseña y su repetición son diferentes";
            if (pwd1.trim().length < 4) throw "La contraseña es obligatoria y debe contener al menos 4 caracteres";
            let pwd = await this.encript(pwd1.trim());
            
            params = {
                login: info.login,
                pwd: pwd
            }

            await db.executeDirect("EXEC [dbo].[PRO_Pwd_recuperacion] @login = N'"+params.login+"', @pwd = N'"+params.pwd+"'");  

            params = {
                token: token
            }

            await db.executeDirect("EXEC [dbo].[PRO_delete_token_recuperacion] @token = N'"+params.token+"'");  

            return await this.login(info.login, pwd1);
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async cambiaPwd(zToken,sesion, pwd, pwd1, pwd2) {
        try {
            let params;
            let db = new SQLServer(); 
            console.log(sesion);
            let u = await this._getUsuarioPorLoginOEmail(sesion.login);
            if (!u) throw "Usuario eliminado";
            if (u.ACTIVO != "S") throw "El usuario ha sido desactivado. Consulte al administrador del Sistema";
            if (! (await this.compareWithEncripted(pwd, u.PASSWORD))) throw "La contraseña actual es inválida";
            if(!this.validatePwd(pwd1)) throw "La contraseña no cumple con el formato establecido.";
            if (pwd1 != pwd2) throw "La nueva contraseña y su repetición son diferentes";
            if (pwd1.trim().length < 4) throw "La nueva contraseña es obligatoria y debe contener al menos 4 caracteres";

            let newPwd = await this.encript(pwd1.trim());
           
            params = {
                login: sesion.login,
                pwd: newPwd
            }

            await db.executeDirect("EXEC [dbo].[PRO_Pwd_recuperacion] @login = N'"+params.login+"', @pwd = N'"+params.pwd+"'");           

        } catch(error) {
            console.error(error);
            throw error;
        }
    }


    async logout(token) {
        try {
            let params;
            let db = new SQLServer();

            params = {
                token: token
            }
            await  db.executeDirect("EXEC [dbo].[PRO_delete_sesionUsuario] @token = N'"+params.token+"'");   

        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async getPrivilegiosSesion(zToken) {
        let rows;
        let params;
        try {
       
            let db = new SQLServer();
            params = {
                token: zToken
            }
              rows= await  db.executeDirect("EXEC [dbo].[PRO_getPrivilegiosSesion] @token = N'"+params.token+"'");  
                     
        } catch(error) {
            console.error(error);
            throw error;
        }
        if (!rows.length) throw "Sesión de Usuario inválida o caducada";
        let r = rows[0];
        if (r.activo != "S") throw "El usuario ha sido desactivado. Consulte al administrador del Sistema";
        return {login:r.login, admin:r.admin == "S"}
    }


    async getUsuarios(zToken, filtro) {
        let p = await this.getPrivilegiosSesion(zToken);
        if (!p.admin) throw "Privilegios Insuficientes";
        try {
            let db = new SQLServer();            
            let params = {
                filtro: filtro.filtro,
                cliente: filtro.cliente?filtro.cliente: '',
                contrato: filtro.contrato?filtro.contrato:'' 
            }
            let reg = []
            let clientescontratos = await Mantenedores.getFullClienteContrato();

            let rows = await  db.executeDirect("EXEC [dbo].[PRO_getUsuarios] @filtro = N'"+params.filtro+"', @cliente = N'"+params.cliente+"', @contrato = N'"+params.contrato+"'");   
            // Compara row con fullclientes, marcando con no name aquellos registros con cliente y contrato null.
            await rows.map(async r => {
                let item = await clientescontratos.find( cc=> cc.idCli == r.cliente && cc.idCon == r.contrato);
                if(item){
                    reg.push({id:r.id, login:r.login, nombre:r.nombre, email:r.email, activo:r.activo == "S", admin:r.admin == "S", tipo: r.tipo == 0?'SIXBELL':'CLIENTE',idContrato: item.idCon, contrato:item.nombreCon, idCliente: item.idCli, cliente: item.nombreCli});
                }else {
                    reg.push({id:r.id, login:r.login, nombre:r.nombre, email:r.email, activo:r.activo == "S", admin:r.admin == "S", tipo: r.tipo == 0?'SIXBELL':'CLIENTE', contrato:'NO NAME', cliente: 'NO NAME'});
                }
            })
            return reg;
            
        } catch(error) {
            console.error(error);
            throw error;
        }
    }



    
    async getClientesConContrato() {
        try {
            let db = new SQLServer();            
            let rows = await  db.executeDirect("EXEC [dbo].[PRO_Busca_Clientes_Con_Contratos]");
            let fullclientescontratos =await  this.getFullClienteContrato();
            let reg = [];
            
            await rows.map(async  r => {
                let item = await fullclientescontratos.find( cc => cc.idCli == r.id_cliente && cc.idCon == r.id_contrato );
                if(item){
                    reg.push({idCliente:item.idCli, nombreCliente:item.nombreCli, idContrato: item.idCon, nombreContrato: item.nombreCon, activo: true});
                }else{
                    reg.push({idCliente:r.id_cliente, nombreCliente:'NO NAME', idContrato: r.id_contrato, nombreContrato: 'NO NAME', activo:false});
                }
            })     
            return reg;    
        } catch(error) {
            console.error(error);
            throw error;
        }
    }


    async getUsuario(login) {
        try { 
            let rows;
            let db = new SQLServer();

            let params = {
                login: login
            }
            rows = await  db.executeDirect("EXEC [dbo].[PRO_getUsuario] @login = N'"+params.login+"'"); 

            if (!rows.length) return null;
            return rows[0];      
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async addUsuario(zToken, usuario) {
      
        let params;
        let p = await this.getPrivilegiosSesion(zToken);
        if (!p.admin) throw "Privilegios Insuficientes";
        if (await this.getUsuario(usuario.login)) throw "Ya existe un usuario con la misma identificación (email)";
        let pwd = await this.encript(usuario.login);
        let tokenRecuperacion;
       
        try {
            let config = require("./Config").Config.instance.getConfig();
            tokenRecuperacion = await this._creaTokenRecuperacion(usuario.login);
            let url = config.general.urlSitioCorreo + "?recupera=" + tokenRecuperacion;
            await this._sendMail(usuario.login, "Cuenta creada en Sistema", null, 
                "<html><body><h3><body>Cuenta Creada</h3><hr />" +
                "<p><b>Sr(a). " + usuario.nombre + ":</b></p>" +
                "<p>Se ha creado una nueva cuenta de para el sistema  asociada a esta dirección de correo electrónico.</p>" + 
                "<p>Su identificación de usuario (login) en el sistema es <b>" + usuario.login + "</b></p>" +
                "<p>Para crear su contraseña, por favor haga click en <a href='" + url + "'>este enlace</a></p>" +
                "<hr /><p><i>Este es un correo automático del Sistema, por favor no lo responda.</i></p></body></html>"
            );                
        } catch(error) {
            throw "No se puede enviar el correo al nuevo usuario:" + error.toString();
        }
        try {
            let db = new SQLServer();            
            params = {
                login: usuario.login,
                password:pwd,
                nombres:usuario.nombre,
                email:usuario.login,
                cliente: usuario.cliente?usuario.cliente: -1,
                contrato: usuario.contrato?usuario.contrato: -1,
                tipo: parseInt(usuario.tipo),
                activo:usuario.activo?"S":"N",
                esAdmin:usuario.admin?"S":"N" 
            }
            console.log( db.tdTYPES.Null.type);
            await  db.executeDirect("EXEC [dbo].[PRO_Add_Usuario] @login = N'"+params.login+"', @password = N'"+params.password+"', @nombres = N'"+params.nombres+"', @email = N'"+ params.login+"', @cliente = N'"+params.cliente+"', @contrato = N'"+params.contrato+"', @tipo = N'"+params.tipo+"', @activo = N'"+params.activo+"', @esAdmin = N'"+params.esAdmin+"'"); 
            let rows = await  db.executeDirect("EXEC [dbo].[PRO_getUsuario] @login = N'"+params.login+"'");      
            usuario.listaReportesPorServicios.map(lista => {
                if(lista.reportes){
                    lista.reportes.map(async reporte => {
                        await  db.executeDirect("EXEC [dbo].[PRO_Add_Usuario_Reportes] @idUsuario = N'"+rows[0].id+"', @idServicio = N'"+lista.servicio+"', @idReporte = N'"+reporte+"'");
                    })
                }
            });
            return usuario;         
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async saveUsuario(zToken, usuario) {
        let p = await this.getPrivilegiosSesion(zToken);
        if (!p.admin) throw "Privilegios Insuficientes";
        try {
            let db = new SQLServer();
            let params = {
                nombre:usuario.nombre,
                login: usuario.login,
                email:usuario.login,
                cliente: usuario.cliente,
                contrato:usuario.contrato,
                tipo:usuario.tipo,
                activo:usuario.activo?"S":"N",
                esAdmin:usuario.admin?"S":"N"
            }

            let filtro={
                filtro:usuario.login
            }
            let rows = await this.getUsuarios(zToken, filtro);
            console.log(rows);
            if(rows){
                //Si el administrador cambia a un usuario de cierto tipo de cliente u contrato, borra todo de la db y crea un nuevo registro.
                if(rows[0].cliente != usuario.cliente || rows[0].contrato != usuario.contrato)
                    await  db.executeDirect("EXEC [dbo].[PRO_Delete_Usuario_Servicio_Reporte] @idUsuario = N'"+ rows[0].id+"'");           
                await  db.executeDirect("EXEC [dbo].[PRO_Update_Usuario] @login = N'" + params.login + "', @nombres = N'"+params.nombre+"', @email = N'"+ params.email+"', @cliente = N'"+params.cliente+"', @contrato = N'"+params.contrato+"', @tipo = N'"+params.tipo+"', @activo = N'"+params.activo+"', @esAdmin = N'"+params.esAdmin+"'");      
                usuario.listaReportesPorServicios.map(lista => {
                    if(lista.reportes){
                        lista.reportes.map(async reporte => {
                            await  db.executeDirect("EXEC [dbo].[PRO_Update_Usuario_Reportes] @idUsuario = N'"+rows[0].id+"', @idServicio = N'"+lista.servicio+"', @idReporte = N'"+reporte+"'");
                        })
                    }
                });
            }
            return usuario;
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async deleteUsuario(zToken, usuario,loginModificacion) {
        let bit;
        let p = await this.getPrivilegiosSesion(zToken);
        if (!p.admin) throw "Privilegios Insuficientes";
        try {
            let db = new SQLServer();
            let params = {
                login: usuario.login,
            }

            let filtro={
                filtro:usuario.login   
            }

            let rows = await this.getUsuarios(zToken, filtro);
            await  db.executeDirect("EXEC [dbo].[PRO_delete_TodasLasSesionesDeUsuario] @login = N'"+params.login+"'"); 
            await  db.executeDirect("EXEC [dbo].[PRO_Delete_Usuario] @login = N'"+params.login+"'"); 
        } catch(error) {
            console.error(error);
            throw error;
        }
    }

    async getServiciosConUsuario(usuario) {
        try {
            let db = new SQLServer();            
            let params = { idUsuario:usuario.id }
            let rows = await  db.executeDirect("EXEC [dbo].[PRO_Busca_Servicios_porUsuario] @idUsuario = N'"+params.idUsuario+"'");   
            return rows.map(r => {
                return {id:r.id, nombre:r.nombre, activo:r.activo == "S",idServicio:r.idServicio}
            })
        } catch(error) {
            throw error;
        }
    }

    async getClientes(){
        try{
            return [{id : 1, nombre :'Cliente 1'},{id : 2, nombre :'Cliente 2'},{id : 3, nombre :'Cliente 3'}]
        }catch(error){
            console.log(error);
            throw error;
        }
    }

    async getContratos(filtro){
        try{
            return [{id : 1, nombre :'Contrato 1'},{id : 2, nombre :'Contrato 2'},{id : 3, nombre :'Contrato 3'}]
        }catch(error){
            console.log(error);
            throw error;
        }
    }

  
}

exports.Seguridad = Seguridad;