class CustomController extends ZCustomComponent {
    onThis_init() {
        this.refresca();
    }

    onEdFiltro_change() { this.refresca() }
    refresca() { this.listaUsuarios.refresh() }

    preparaFila(row) {
        delete row._rowClass;
        if (!row.activo) {
            row._rowClass="table-danger";
        } else if (row.admin) {
            row._rowClass="table-warning";
        }
        let edAdmin = 
            "<div class='togglebutton text-center' ><label style='line-height:0.6;'>" +
                "<input class='admin-toggler' data-login='" + row.login + "' type='checkbox'" + (row.admin?" checked='checked'":"") + ">" +
                "   <span class='toggle'></span>" +
            "</label></div>";
        row.edAdmin = edAdmin;
        let edActivo = 
            "<div class='togglebutton text-center'><label style='line-height:0.6;'>" +
                "<input class='activo-toggler' data-login='" + row.login + "' type='checkbox'" + (row.activo?" checked='checked'":"") + ">" +
                "   <span class='toggle'></span>" +
            "</label></div>";
        row.edActivo = edActivo;
       
        return row;
    }
    registraHandlersFilas() {
        this.listaUsuarios.view.find(".admin-toggler").change(e => {
            let c = $(e.currentTarget).prop("checked")?true:false;
            let login = $(e.currentTarget).data("login");  
                   
            let idx = this.listaUsuarios.rows.findIndex(r => r.login == login);        
            if (idx < 0) return;

            let r = this.listaUsuarios.rows[idx];
            r.admin = c;
            r.idPerfil=r.idPerfil ==null ?-1 : r.idPerfil;
            r.loginModificacion= window.app.sesion.usuario.login;
            r.listaReportes = '-1';
            if (window.app.sesion.usuario.login == login && window.app.sesion.usuario.admin){
                let msg = "¿Confirma que desea quitar los privilegios de 'Admin' para su usuario?"
                this.showDialog("common/WConfirm", {message: msg}, () => {
           
                    zPost("saveUsuario.seg", {zToken:window.zSecurityToken,usuario:r})
                    .then(() => {
                        this.preparaFila(r);
                        this.refresca();
                    })
                    .catch(error => this.showDialog("common/WError", {message:error.toString()}));
                },
                () => {
                    this.refresca();
                });
            }else{
                zPost("saveUsuario.seg", {zToken:window.zSecurityToken,usuario:r})
                .then(() => {
                    this.refresca();
                })
                .catch(error => this.showDialog("common/WError", {message:error.toString()}));
            }

        });
        this.listaUsuarios.view.find(".activo-toggler").change(e => {
            let c = $(e.currentTarget).prop("checked")?true:false;
            let login = $(e.currentTarget).data("login");            
            let idx = this.listaUsuarios.rows.findIndex(r => r.login == login);
            if (idx < 0) return;
            let r = this.listaUsuarios.rows[idx];
            r.activo = c;
            r.loginModificacion= window.app.sesion.usuario.login;

            if (window.app.sesion.usuario.login == login && !c){
                let msg = "¿Confirma que desea desactivar su usuario. Esto no le permitirá seguir operando en el sistema?"
                this.showDialog("common/WConfirm", {message: msg}, () => {
           
                    zPost("saveUsuario.seg", {zToken:window.zSecurityToken,usuario:r})
                    .then(() => {
                        this.refresca();
                    })
                    .catch(error => this.showDialog("common/WError", {message:error.toString()}));
                }
                , () => {
                    this.refresca();
                }
                );
            }else{
                zPost("saveUsuario.seg", {zToken:window.zSecurityToken,usuario:r})
                .then(() => {
                    this.refresca();
                })
                .catch(error => this.showDialog("common/WError", {message:error.toString()}));
            }    
        });
    }
    onListaUsuarios_getRows(cb) {
        let filtro = {
            filtro: this.edFiltro.val.trim(),
            cliente : window.app.sesion.usuario.cliente,
            contrato : window.app.sesion.usuario.contrato
        }
        zPost("getUsuarios.seg", {zToken:window.zSecurityToken,filtro:filtro}, usuarios => {  
            usuarios.forEach(u => this.preparaFila(u));
            cb(usuarios);
        });
    }
    onListaUsuarios_afterPaint() {
        this.registraHandlersFilas();
    }
    onCmdAgregarUsuario_click() {
        this.showDialog("./wed-usuario", {newRecord:true}, usuario => this.refresca());
    }
    onListaUsuarios_editRequest(idx, row) {
        this.showDialog("./wed-usuario", {record:row}, usuario => {
            this.listaUsuarios.updateRow(idx, this.preparaFila(usuario));
            this.refresca();
        });
       
    }
    onListaUsuarios_deleteRequest(idx, row) {
        this.showDialog("common/WConfirm", {message:"¿Confirma que desea eliminar el usuario '" + row.nombre + "'?"}, () => {
           
            zPost("deleteUsuario.seg", {zToken:window.zSecurityToken,usuario:row,loginModificacion: window.app.sesion.usuario.login})
                .then(() => {
                    this.listaUsuarios.deleteRow(idx);
                })
                .catch(error => this.showDialog("common/WError", {message:error.toString()}));
        });
    }    
}