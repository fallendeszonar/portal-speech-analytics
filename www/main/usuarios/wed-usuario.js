let serviceSelect = -1;
let arrListaReportesPorServicio = [[]];

class CustomController extends ZCustomComponent {
    onThis_init(options) {
        this.options = options;
        let sesion = window.app.sesion.usuario;
        this._arrListaReportesSel = [];
        this.mensajeError.hide();
        this.loading.hide();
        
        this.edTipousuario.setRows([{id:0, nombre:'Sixbell'},{id:1, nombre:'Cliente'}], options.newRecord ? null :  this.options.record.tipo=='SIXBELL'?0:1);

        if (options.newRecord) {
            this.title.view.text("Crear Nuevo Usuario");
            this.lblAccion.view.text("Nuevo Usuario: ");
            this.cmdOk.disable();
            this.edActivo.checked = true;
            this.edAdmin.checked = false;
            setTimeout(() => {
                this.edLogin.view.focus();
            }, 500);
            this.edContratos.disable();
            this.edServicios.disable();
            this.listaReportes.hide();
            this.cmdGuardar.hide();
            if(sesion.admin && sesion.tipo ==0){
                this.divClientes.hide();
                this.divContratos.hide();
                this.divServicios.hide();
            }
            if(sesion.admin && sesion.tipo ==1){
                console.log(sesion);
                
                this.edTipousuario.setRows([{id:0, nombre:'Sixbell'},{id:1, nombre:'Cliente'}], sesion.tipo);
                this.edTipousuario.disable();
                zPost("getClientes.man")
                .then(clientes => this.edClientes.setRows(clientes, sesion.cliente))
                .catch(error => console.error(error));
                this.edClientes.disable();
                
                let c = {
                    id: sesion.cliente
                }
                zPost("getAllContratosPorCliente.man", { cliente:c})
                .then(contratos => this.edContratos.setRows(contratos, sesion.contrato))
                .catch(error => console.error(error));
                
                c = {
                    idCliente: sesion.cliente,
                    idContrato: sesion.contrato
                }
                this.cmdGuardar.show();
                zPost("getServiciosClienteContrato.man", { clientecontrato: c })
                .then(servicios => this.edServicios.setRows(servicios, null))
                .catch(error => console.error(error));
                this.divServicios.show();
                this.edServicios.enable();
            }
            

        } else {
            this.title.view.text("Editar Usuario");
            this.lblAccion.view.text("Editar Usuario: ");
            let u = options.record;
            this.edLogin.disable();
            this.edLogin.val = u.login;
            this.edNombre.val = u.nombre;
            this.edActivo.checked = u.activo;
            this.edAdmin.checked = u.admin;
            if(u.admin){
                this.divServicios.hide();
                this.listaReportes.hide();
            }else{
                this.divServicios.show();
                this.listaReportes.show();
            }
            this.divClientes.show();
            this.divContratos.show();
            console.log(options.record);
            zPost("getClientes.man")
            .then(clientes => this.edClientes.setRows(clientes, options.newRecord ? null : options.record.idCliente))
            .catch(error => console.error(error));
            
            let c = {
                id: options.record.idCliente
            }
            zPost("getAllContratosPorCliente.man", { cliente:c})
            .then(contratos => this.edContratos.setRows(contratos, options.newRecord ? null : options.record.idContrato))
            .catch(error => console.error(error));
            
            c = {
                idCliente: options.record.idCliente,
                idContrato: options.record.idContrato
            }
            this.cmdGuardar.show();
            zPost("getServiciosClienteContrato.man", { clientecontrato: c })
            .then(servicios => this.edServicios.setRows(servicios, null))
            .catch(error => console.error(error));
            this.divServicios.show();
        }
    }
   

    onCmdGuardar_click(){
        arrListaReportesPorServicio.push({servicio : serviceSelect , reportes: this._arrListaReportesSel});        
        this._arrListaReportesSel = [];
        this.cmdOk.enable();
    }

    refresca() { this.listaReportes.refresh() }
    preparaFila(row) {
        delete row._rowClass;
        if (row.activo) {
            row._rowClass = null;
            row.imgActivo = "<span class='fa fa-check'></span>";
        } else {
            row.imgActivo = "<span class='fa fa-ban'></span>";
        }
        return row;
    }
    
    onListaReportes_columnClick(rowIndex, columnName, row) {
        this.cmdOk.disable();
        if (this._arrListaReportesSel.indexOf(row.id) >= 0) {
            this._arrListaReportesSel.splice(this._arrListaReportesSel.indexOf(row.id), 1);
        } else {  
            this._arrListaReportesSel.push(row.id);
        }
        switch (columnName) {
            case "imgActivo":
                row.activo = !row.activo;        
                this.preparaFila(row);
                this.listaReportes.updateRow(rowIndex, row);
                break;
        }
    }
    onListaReportes_getRows(cb) { 
        let s = {
            id: serviceSelect
        }
        if (this.options.newRecord) {
             zPost("getReportesPorServicioAdm.man", {servicio: s}, reportes => {
                reportes.map( reporte => {
                   // console.log(reporte); // informacion del reporte completo // {id: "8", nombre: "REPORTE 4", filtro: "FILTRO 4", url: "URL 4", activo: false}
                    if(arrListaReportesPorServicio){
                       arrListaReportesPorServicio.map(rs => {
                        if(rs.servicio == s.id){
                            rs.reportes.map(rep => {
                                if(rep == reporte.id) reporte.activo=true;
                            });
                        }
                       });
                   }
                   this.preparaFila(reporte);
                });
                cb(reportes);
            });
        } else {
            let u = {
                idServicio: this.edServicios.val,
                idUsuario: parseInt(options.record.id)
            }
            zPost("getReportesPorUsuario.man", {usuario: u}, reportes => {
                reportes.map(reporte =>{
                    this.preparaFila(reporte);
                });
                cb(reportes);
                this.cargaReportesUpdate(reportes);
            });
        }   
    }

    onEdTipousuario_change(){  
        if(this.edTipousuario.val == 1){
            zPost("getClientes.man")
            .then(clientes => this.edClientes.setRows(clientes, options.newRecord ? null : options.record.cliente))
            .catch(error => console.error(error));
            this.divClientes.show();
        }else{
            this.divClientes.hide();
            this.divServicios.hide();
            this.divContratos.hide();
            this.listaReportes.hide();
        }
        this.cmdOk.enable();
    }

    onEdClientes_change(){
        this.edContratos.enable();
        let c = {
            id: this.edClientes.val
        }
        this.divContratos.show();
        zPost("getAllContratosPorCliente.man", { cliente: c })
        .then(contratos => this.edContratos.setRows(contratos, options.newRecord ? null : options.record.contrato))
        .catch(error => console.error(error));
    }

    onEdContratos_change(){
        this.edServicios.enable();
        let c = {
            idCliente: this.edClientes.val,
            idContrato: this.edContratos.val
        }
        if(this.edTipousuario.val == 1 && this.edAdmin.checked){
            this.divServicios.hide();
        }else{
            this.cmdGuardar.show();
            zPost("getServiciosClienteContrato.man", { clientecontrato: c })
            .then(servicios => this.edServicios.setRows(servicios, options.newRecord ? null : options.record.id))
            .catch(error => console.error(error));
            this.divServicios.show();
        }  
    }

    onEdServicios_change(){

        serviceSelect = this.edServicios.val;
        this.listaReportes.show();
        this.refresca();
        this.cmdOk.disable();
    }


    cargaReportesUpdate(reportes){
        let thisController = this;
        $.each(this._arrListaReportesSel, function (item) {
            thisController._arrListaReportesSel.push(reportes[item].id);
        });
    }

    onCmdCancel_click() { this.cancel() }
    muestraError(txt) {
        this.textoMensajeError.view.text(txt);
        this.mensajeError.show();
    }
    onCmdOk_click() {
        let u = {
                login: this.edLogin.val.trim(),
                nombre: this.edNombre.val,
                activo: this.edActivo.checked,
                admin: this.edAdmin.checked,
                email : this.edLogin.val.trim(),
                tipo: this.edTipousuario.val,
                cliente: this.options.newRecord && this.edTipousuario.val == 0 ? null : this.edClientes.val,
                contrato: this.options.newRecord && this.edTipousuario.val == 0 ? null : parseInt(this.edContratos.val),
                loginModificacion: window.app.sesion.usuario.login,
                listaReportesPorServicios: arrListaReportesPorServicio,
                id: this.options.newRecord  ? -1 : this.options.record.id
            }
       console.log(u);
        let findError = false;
        if (!u.nombre) {
            findError = true;
            this.edNombre.setValidation(false);
        } else {
            this.edNombre.setValidation(true);
        }

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

        if (!u.login) {
            findError = true;
            this.edCorreoInvalid.text = "Debe ingresar un email.";
            this.edLogin.setValidation(false);
            } else if (!re.test(u.login)) {
            findError = true;
            this.edCorreoInvalid.text = "Debe ingresar un email válido.";
            this.edLogin.setValidation(false);
            } else {
            this.edLogin.setValidation(true);
        }
        if (findError) {
            this.muestraError(" Favor corregir la información según se indica.");
            return;
        }
        
        this.loading.show();
        this.mensajeError.hide();

        if (this.options.newRecord) {
            zPost("addUsuario.seg", { zToken: window.zSecurityToken, usuario: u })
                .then(usuario => {
                    this.cmdOk.enable();
                    this.loading.hide();
                    this.close(usuario)
                })
                .catch(error => this.showDialog("common/WError", { message: error.toString() }));
        } else {

            this.cmdOk.disable();
            zPost("saveUsuario.seg", { zToken: window.zSecurityToken, usuario: u })
                .then(usuario => {
                    this.cmdOk.enable();
                    this.loading.hide();
                    this.close(usuario)
                })
                .catch(error => this.showDialog("common/WError", { message: error.toString() })); 
            }
    }
    onEdAdmin_change() {
        if(this.edAdmin.checked){
            this.divServicios.hide();
            this.listaReportes.hide();
        }else{
            let c = {
                idCliente: this.edClientes.val,
                idContrato: this.edContratos.val
            }
            zPost("getServiciosClienteContrato.man", { clientecontrato: c })
            .then(servicios => this.edServicios.setRows(servicios, options.newRecord ? null : options.record.id))
            .catch(error => console.error(error));
            this.divServicios.show();
            this.divServicios.show();
            this.listaReportes.show();
        }
    }
}