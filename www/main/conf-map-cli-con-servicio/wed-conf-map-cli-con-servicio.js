class CustomController extends ZCustomComponent {
    onThis_init(options) {
        this.options = options;
        this.mensajeError.hide();
        this.loading.hide();
        console.log(options.record);

        if (options.newRecord) {
            this.title.view.text("Crear Nuevo Header");
            this.lblAccion.view.text("Nuevo Header: ");
            this.cmdOk.disable();

            zPost("getClientesDisponibles.man")
            .then(clientes => this.edClientes.setRows(clientes, null))
            .catch(error => console.error(error));

            this.edActivo.checked = true;
        } else {
            this.title.view.text("Editar Header");
            this.lblAccion.view.text("Editar Header: ");
            let u = options.record;
            console.log(u);
            
            zPost("getClientes.man")
            .then(clientes => this.edClientes.setRows(clientes,u.idCliente))
            .catch(error => console.error(error));
            
            let c = {
                id: u.idCliente
            }

            zPost("getAllContratosPorCliente.man", { cliente: c })
            .then(contratos => {
                console.log(contratos);
                this.edContratos.setRows(contratos,u.idContrato)})
            .catch(error => console.error(error));
        

            this.edCodigo.val = u.id;
            this.edCodigo.disable();
            this.edNombre.val = u.nombreServicio;
            this.edTabla.val = u.nombreTabla;
            this.edActivo.checked = u.activo;
        }
    }

    onEdClientes_change(){
        let c = {
            id: this.edClientes.val
        }
        zPost("getContratosDisponibles.man", {cliente:c})
        .then(contratos => {console.log(contratos);this.edContratos.setRows(contratos,this.options.newRecord ? null: this.options.record.idContrato)})
        .catch(error => console.error(error));
    }

    onEdContratos_change(){
        this.cmdOk.enable();
    }

    onCmdCancel_click() { this.cancel() }
    muestraError(txt) {
        this.textoMensajeError.view.text(txt);
        this.mensajeError.show();
    }
    onCmdOk_click() {

        let mccs = {
            codigo: this.edCodigo.val,
            nombre: this.edNombre.val,
            cliente: this.edClientes.val,
            contrato: this.edContratos.val,
            tabla: this.edTabla.val,
            activo: this.edActivo.checked 
        }
        let findError = false;

        if (!mccs.codigo) {
            findError = true;
            this.edCodigo.setValidation(false);
        } else {
            this.edCodigo.setValidation(true);
        }
        
        if (!mccs.nombre) {
            findError = true;
            this.edNombre.setValidation(false);
        } else {
            this.edNombre.setValidation(true);
        }

        if (!mccs.tabla) {
            findError = true;
            this.edTabla.setValidation(false);
        } else {
            this.edTabla.setValidation(true);
        }
        
        this.mensajeError.hide();

        if (this.options.newRecord) {
            zPost("addMapeoClienteContratoServicio.man", { zToken: window.zSecurityToken, mccs : mccs })
                .then(mapeoccs => {
                    this.cmdOk.enable();
                    this.loading.hide();
                    this.close(mapeoccs)
                })
                .catch(error => this.showDialog("common/WError", { message: error.toString() }));
        } else {
            this.cmdOk.disable();
            mccs['id'] = options.record.id;

            zPost("saveMapeoClienteContratoServicio.man", { zToken: window.zSecurityToken, mccs: mccs })
                .then(mapeoccs => {
                    this.cmdOk.enable();
                    this.loading.hide();
                    this.close(mapeoccs)
                }
                )
                .catch(error => this.showDialog("common/WError", { message: error.toString() }));
            }
    }
}