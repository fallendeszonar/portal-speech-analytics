class CustomController extends ZCustomComponent {
    onThis_init() {
        this.refresca();
        this.controller = this;
    }

    onEdFiltro_change() { this.refresca() }
    refresca() { this.listaClienteContratoServicio.refresh() }

    preparaFila(row) {
        delete row._rowClass;
        if (!row.activo) {
            row._rowClass="table-danger";
        } 
        let edActivo = 
            "<div class='togglebutton text-center'><label style='line-height:0.6;'>" +
                "<input class='activo-toggler' data-login='" + row.login + "' type='checkbox'" + (row.activo?" checked='checked'":"") + ">" +
                "   <span class='toggle'></span>" +
            "</label></div>";
        row.edActivo = edActivo;
       
        return row;
    }
    registraHandlersFilas() {
        this.listaClienteContratoServicio.view.find(".activo-toggler").change(e => {
            let c = $(e.currentTarget).prop("checked")?true:false;
            let login = $(e.currentTarget).data("login"); 
            let idx = this.listaClienteContratoServicio.rows.findIndex(r => r.login == login);
            if (idx < 0) return;
            let r = this.listaClienteContratoServicio.rows[idx];
            r.activo = c;
            r.loginModificacion= window.app.sesion.usuario.login;

           
        });
    }

    onListaClienteContratoServicio_getRows(cb) {
        let filtro = {
            filtro: this.edFiltro.val.trim() 
        }
        zPost("getMapeoClienteContratoServicio.man", {filtro:filtro.filtro}, mccs => {
            mccs.forEach(m=> this.preparaFila(m));
            cb(mccs);
        });
    }
    onListaClienteContratoServicio_afterPaint() {
        this.registraHandlersFilas();
    }
   
    onListaClienteContratoServicio_editRequest(idx, row) {
        this.showDialog("./wed-conf-map-cli-con-servicio", {record:row}, mccs => {
            this.listaClienteContratoServicio.updateRow(idx, this.preparaFila(mccs));
        });
    }
    onListaClienteContratoServicio_deleteRequest(idx, row) {
        console.log(idx, row);
        this.showDialog("common/WConfirm", {message:"¿Confirma que desea eliminar el Mapeo Servicio Cliente Contrato '" + row.nombreServicio + "'?"}, () => {
            zPost("deleteMapeoClienteContratoServicio.man", {zToken:window.zSecurityToken, mccs:row})
            .then(() => {   
                this.listaClienteContratoServicio.deleteRow(idx);
            })
            .catch(error => this.showDialog("common/WError", {message:error.toString()}));
        });
    }  
    onCmdAgregarCCS_click() {
        this.showDialog("./wed-conf-map-cli-con-servicio", {newRecord:true}, mccs => this.refresca());
    }
}