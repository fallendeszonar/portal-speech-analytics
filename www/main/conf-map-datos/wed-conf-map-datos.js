let serviceSelect;

class CustomController extends ZCustomComponent {
    onThis_init(options) {
        this.options = options;
        this._arrListaHeadersSel = [];
        this.mensajeError.hide();
        this.loading.hide();
        this.refresca();

        if (options.newRecord) {
            this.listaHeader.hide();
            this.edMapeo.enable();

            this.title.view.text("Crear Nuevo Mapeo / Header");
            this.lblAccion.view.text("Nuevo Mapeo / Header: ");
       
            zPost("getMapeoSinHeader.man")
            .then(mapeos => this.edMapeo.setRows(mapeos,null))
            .catch(error => console.error(error))
            
        } else {
            this.title.view.text("Editar Mapeo / Header");
            this.lblAccion.view.text("Editar Mapeo / Header: ");
            let u = options.record;
            this.edMapeo.enable();
            console.log(u);
            zPost("getMapeos.man",{filtro:''})
            .then(mapeos =>{console.log(mapeos); this.edMapeo.setRows(mapeos, u.id)})
            .catch(error => console.error(error));
            this.edMapeo.disable();

            console.log(u);
            this.listaHeader.show();

        } 
    }
    refresca() { this.listaHeader.refresh() }

    preparaFila(row) {
        delete row._rowClass;
        if (row.activo) {
            row._rowClass = null;
            row.imgActivo = "<span class='fa fa-check'></span>";
        } else {
            row.imgActivo = "<span class='fa fa-ban'></span>";
        }
        return row;
    }
    
    onListaHeader_columnClick(rowIndex, columnName, row) {
        if (this._arrListaHeadersSel.indexOf(row.id) >= 0) {
            this._arrListaHeadersSel.splice(this._arrListaHeadersSel.indexOf(row.id), 1);
        } else {  
            this._arrListaHeadersSel.push(row.id);
        }
        switch (columnName) {
            case "imgActivo":
                row.activo = !row.activo;        
                this.preparaFila(row);
                this.listaHeader.updateRow(rowIndex, row);
                break;
        }
    }
    onListaHeader_getRows(cb) { 
        if (this.options.newRecord) {
            zPost("getHeaderActivos.man",{}, headers => {
                headers.forEach(s => this.preparaFila(s));
                cb(headers);
            });
        } else {
            let s = {
                id : this.options.record.id
            }
            zPost("getHeaderAsocMapeo.man",{mapeo : s}, headers => {
                headers.forEach(c => this.preparaFila(c));
                cb(headers);
                this.cargaHeadersUpdate(headers);
            });
        }
    }
    cargaHeadersUpdate(headers){
        let thisController = this;
        $.each(this._arrListaHeadersSel, function (item) {
            thisController._arrListaHeadersSel.push(headers[item].id);
        });
    }



    onCmdCancel_click() { this.cancel() }
    
    onEdMapeo_change(){
        this.listaHeader.show();
    }
    
    onCmdOk_click() {
        this.loading.show();
        this.mensajeError.hide();

        if (this.options.newRecord) {
            let u = {
                idMapeo :  this.edMapeo.val,
                listaHeaders : this._arrListaHeadersSel,
            }
            console.log(u);
            zPost("addMapeoHeader.man", { zToken: window.zSecurityToken, maphead: u })
            .then(maphead => {
                this.cmdOk.enable();
                this.loading.hide();
                this.close(maphead)
            })
            .catch(error => this.showDialog("common/WError", { message: error.toString() }));
        } else {
            
            let u = {
                idMapeo :  this.options.record.id,
                listaHeaders : this._arrListaHeadersSel,
                nombre : this.options.record.nombre,
                activo: this.options.record.activo
            }  
            console.log(u);
                
            zPost("saveMapeoHeader.man", { zToken: window.zSecurityToken, maphead: u })
                .then(header => {
                    this.cmdOk.enable();
                    this.loading.hide();
                    this.close(header)
                })
                .catch(error => this.showDialog("common/WError", { message: error.toString() }));
            }
    }
}