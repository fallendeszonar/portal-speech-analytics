let idServicio='';
class CustomController extends ZCustomComponent {
    onThis_init() {
        this.refresca();
        this.controller = this;
    }

    onEdFiltro_change() { this.refresca() }
    refresca() { this.listaMapeoDatos.refresh(); this.listaHeaders.refresh(); }
    
    preparaFilaHeader(row) {
        delete row._rowClass;
        if (row.activo) {
            row._rowClass = null;
            row.imgActivo = "<span class='fa fa-check'></span>";
        } else {
            row.imgActivo = "<span class='fa fa-ban'></span>";
        }
        return row;
    }
    preparaFila(row) {
        delete row._rowClass;
        if (!row.activo) {
            row._rowClass="table-danger";
        } 
        let edActivo = 
            "<div class='togglebutton text-center'><label style='line-height:0.6;'>" +
                "<input class='activo-toggler' data-login='" + row.login + "' type='checkbox'" + (row.activo?" checked='checked'":"") + ">" +
                "   <span class='toggle'></span>" +
            "</label></div>";
        row.edActivo = edActivo;
       
        return row;
    }
    registraHandlersFilas() {
        this.listaMapeoDatos.view.find(".activo-toggler").change(e => {
            let c = $(e.currentTarget).prop("checked")?true:false;
            let login = $(e.currentTarget).data("login"); 
            let idx = this.listaMapeoDatos.rows.findIndex(r => r.login == login);
            if (idx < 0) return;
            let r = this.listaMapeoDatos.rows[idx];
            r.activo = c;
            r.loginModificacion= window.app.sesion.usuario.login;

            if (window.app.sesion.usuario.login == login && !c){
                let msg = "¿Confirma que desea desactivar el header. Esto no le permitirá seguir operando en el sistema?"
                this.showDialog("common/WConfirm", {message: msg}, () => {
           
                    zPost("saveMapeo.man", {zToken:window.zSecurityToken, header:r})
                    .then(() => {
                        this.refresca();
                    })
                    .catch(error => this.showDialog("common/WError", {message:error.toString()}));
                }
                , () => {
                    this.refresca();
                }
                );
            }else{
                zPost("saveMapeo.man", {zToken:window.zSecurityToken,header:r})
                .then(() => {
                    this.refresca();
                })
                .catch(error => this.showDialog("common/WError", {message:error.toString()}));
            }    
        });
    }

    onListaMapeoDatos_getRows(cb) {
        let filtro = {
            filtro: this.edFiltro.val.trim() 
        }
        zPost("getMapeos.man", {filtro:filtro.filtro}, mapeos => {  
            mapeos.forEach(h => this.preparaFila(h));
            
            cb(mapeos);
        });
    }
    onListaMapeoDatos_afterPaint() {
        this.registraHandlersFilas();
        
    }
   
    onListaMapeoDatos_editRequest(idx, row) {
        this.showDialog("./wed-conf-map-datos", {record:row}, header => {
            this.listaMapeoDatos.updateRow(idx, this.preparaFila(header));
            this.refresca();
        });
    }
    onListaMapeoDatos_deleteRequest(idx, row) {
        console.log(row);
        this.showDialog("common/WConfirm", {message:"¿Confirma que desea eliminar la asociacion de mapeo con headers '" + row.nombre + "'?"}, () => {
            zPost("deleteMapeo.man", {zToken:window.zSecurityToken, maphead:row})
                .then(() => {
                    this.listaMapeoDatos.deleteRow(idx);
                })
                .catch(error => this.showDialog("common/WError", {message:error.toString()})); 
        });
    }  
    onListaMapeoDatos_columnClick(rowIndex, columnName, row) {
        idServicio = row.id;
        this.listaHeaders.show();
        this.listaHeaders.refresh();
    }
    onListaHeaders_getRows(cb) { 
        if(idServicio == ''){
            zPost("getHeaderActivos.man",{}, headers => {
                headers.forEach(s => this.preparaFilaHeader(s));
                cb(headers);
            });
        }else{
            let m = {
                id : idServicio
            }
            zPost("getHeaderAsocMapeo.man",{mapeo :m}, headers => {
                headers.forEach(s => this.preparaFilaHeader(s));
                cb(headers);
            });
        }
    }
    

    onCmdAgregarMapeoHeader_click() {
        this.showDialog("./wed-conf-map-datos", {newRecord:true}, header => this.refresca());
    }
}