class CustomController extends ZCustomComponent {
    onThis_init() {
        this.refresca();
        this.controller = this;
    }

    onEdFiltro_change() { this.refresca() }
    refresca() { this.listaHeaders.refresh() }

    preparaFila(row) {
        delete row._rowClass;
        if (!row.activo) {
            row._rowClass="table-danger";
        } 
        let edActivo = 
            "<div class='togglebutton text-center'><label style='line-height:0.6;'>" +
                "<input class='activo-toggler' data-login='" + row.login + "' type='checkbox'" + (row.activo?" checked='checked'":"") + ">" +
                "   <span class='toggle'></span>" +
            "</label></div>";
        row.edActivo = edActivo;
       
        return row;
    }
    registraHandlersFilas() {
        this.listaHeaders.view.find(".activo-toggler").change(e => {
            let c = $(e.currentTarget).prop("checked")?true:false;
            let login = $(e.currentTarget).data("login"); 
            let idx = this.listaHeaders.rows.findIndex(r => r.login == login);
            if (idx < 0) return;
            let r = this.listaHeaders.rows[idx];
            r.activo = c;
            r.loginModificacion= window.app.sesion.usuario.login;

            if (window.app.sesion.usuario.login == login && !c){
                let msg = "¿Confirma que desea desactivar el header. Esto no le permitirá seguir operando en el sistema?"
                this.showDialog("common/WConfirm", {message: msg}, () => {
           
                    zPost("saveHeader.man", {zToken:window.zSecurityToken, header:r})
                    .then(() => {
                        this.refresca();
                    })
                    .catch(error => this.showDialog("common/WError", {message:error.toString()}));
                }
                , () => {
                    this.refresca();
                }
                );
            }else{
                zPost("saveHeader.man", {zToken:window.zSecurityToken,header:r})
                .then(() => {
                    this.refresca();
                })
                .catch(error => this.showDialog("common/WError", {message:error.toString()}));
            }    
        });
    }

    onListaHeaders_getRows(cb) {
        let filtro = {
            filtro: this.edFiltro.val.trim() 
        }
        zPost("getHeaders.man", {filtro:filtro.filtro}, headers => {  
            headers.forEach(h => this.preparaFila(h));
            cb(headers);
        });
    }
    onListaHeaders_afterPaint() {
        this.registraHandlersFilas();
    }
   
    onListaHeaders_editRequest(idx, row) {
        this.showDialog("./wed-conf-header", {record:row}, header => {
            this.listaHeaders.updateRow(idx, this.preparaFila(header));
        });
    }
    onListaHeaders_deleteRequest(idx, row) {

        this.showDialog("common/WConfirm", {message:"¿Confirma que desea eliminar el header '" + row.tipo + "'?"}, () => {
            zPost("deleteHeader.man", {zToken:window.zSecurityToken, header:row})
                .then(() => {
                    this.listaHeaders.deleteRow(idx);
                })
                .catch(error => this.showDialog("common/WError", {message:error.toString()}));
        });
    }  

    onCmdAgregarHeader_click() {
        this.showDialog("./wed-conf-header", {newRecord:true}, header => this.refresca());
    }
}