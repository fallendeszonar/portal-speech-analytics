class CustomController extends ZCustomComponent {
    onThis_init(options) {
        this.options = options;
        this.mensajeError.hide();
        this.loading.hide();
        if (options.newRecord) {
            this.title.view.text("Crear Nuevo Header");
            this.lblAccion.view.text("Nuevo Header: ");
            this.edActivo.checked = true;
        } else {
            this.title.view.text("Editar Header");
            this.lblAccion.view.text("Editar Header: ");
            let u = options.record;
            this.edNombre.val = u.nombre;
            this.edTipo.val = u.tipo;
            this.edFormato.val = u.formato;
            this.edDescripcion.val = u.descripcion;
            this.edActivo.checked = u.activo;

        }
        let filtro = {
            filtro: "",
            activo: "S"
        }
    }

    onCmdCancel_click() { this.cancel() }
    muestraError(txt) {
        this.textoMensajeError.view.text(txt);
        this.mensajeError.show();
    }
    onCmdOk_click() {
        let h = {
            nombre: this.edNombre.val,
            tipo: this.edTipo.val,
            formato: this.edFormato.val,
            descripcion: this.edDescripcion.val,
            activo: this.edActivo.checked
        }
        
        this.cmdOk.disable();
        this.loading.show();
        this.mensajeError.hide();

        if (this.options.newRecord) {

            zPost("addHeader.man", { zToken: window.zSecurityToken, header: h })
                .then(header => {
                    this.cmdOk.enable();
                    this.loading.hide();
                    this.close(header)
                })
                .catch(error => this.showDialog("common/WError", { message: error.toString() }));
        } else {
            this.cmdOk.disable();
            h['id'] = options.record.id;

            zPost("saveHeader.man", { zToken: window.zSecurityToken, header: h })
                .then(header => {
                    this.cmdOk.enable();
                    this.loading.hide();
                    this.close(header)
                }
                )
                .catch(error => this.showDialog("common/WError", { message: error.toString() }));
        }
    }
}