let serviceSelect;

class CustomController extends ZCustomComponent {
    onThis_init(options) {
        this.options = options;
        this._arrListaReportesSel = [];
        this.mensajeError.hide();
        this.loading.hide();
        this.refresca();
        if (options.newRecord) {
            this.listaReportes.hide()
            this.title.view.text("Crear Nuevo Servicio / Reporte");
            this.lblAccion.view.text("Nuevo Servicio / Reporte: ");
            
        } else {
            this.title.view.text("Editar Servicio / Reporte");
            this.lblAccion.view.text("Editar Servicio / Reporte: ");
            let u = options.record;
            this.edServicios.hide();
        }
        let filtro = {
            filtro: "",
            activo: "S"
        }
        zPost("getServiciosSinReportes.man")
        .then(servicios =>{ this.edServicios.setRows(servicios, options.newRecord ? null : options.record.id);})
        .catch(error => console.error(error));
 
    }
    refresca() { this.listaReportes.refresh() }


    preparaFila(row) {
        delete row._rowClass;
        if (row.activo) {
            row._rowClass = null;
            row.imgActivo = "<span class='fa fa-check'></span>";
        } else {
            row.imgActivo = "<span class='fa fa-ban'></span>";
        }
        return row;
    }
    
    onListaReportes_columnClick(rowIndex, columnName, row) {
        if (this._arrListaReportesSel.indexOf(row.id) >= 0) {
            this._arrListaReportesSel.splice(this._arrListaReportesSel.indexOf(row.id), 1);
        } else {  
            this._arrListaReportesSel.push(row.id);
        }
        switch (columnName) {
            case "imgActivo":
                row.activo = !row.activo;        
                this.preparaFila(row);
                this.listaReportes.updateRow(rowIndex, row);
                break;
        }
    }
    onListaReportes_getRows(cb) { 
        if (this.options.newRecord) {
            zPost("getReportesActivos.man",{}, reportes => {
                reportes.forEach(s => this.preparaFila(s));
                cb(reportes);
            });
        } else {
            let s = {
                id : parseInt(this.options.record.id)
            }
            zPost("getReportesPorServicio.man",{servicio : s}, reportes => {
                reportes.forEach(s => this.preparaFila(s));
                cb(reportes);
                this.cargaReportesUpdate(reportes);
            });
        }
    }
    cargaReportesUpdate(reportes){
        let thisController = this;
        $.each(this._arrListaReportesSel, function (item) {
            thisController._arrListaReportesSel.push(reportes[item].id);
        });
    }

    onEdServicios_change(){
        this.listaReportes.show();
    }
    onCmdCancel_click() { this.cancel() }
    muestraError(txt) {
        this.textoMensajeError.view.text(txt);
        this.mensajeError.show();
    }
    onCmdOk_click() {
        this.loading.show();
        this.mensajeError.hide();
        
        if (this.options.newRecord) {
            let u = {
                idServicio :  this.edServicios.val,
                listaReportes : this._arrListaReportesSel
            }   
            if(!this.edServicios.val){}
            zPost("addServicioReporte.man", { zToken: window.zSecurityToken, servicio: u })
                .then(servicio => {
                    this.cmdOk.enable();
                    this.loading.hide();
                    this.close(servicio)
                })
                .catch(error => this.showDialog("common/WError", { message: error.toString() }));
        } else {
            let u = {
                idServicio :  this.options.record.id,
                listaReportes : this._arrListaReportesSel,
                id: this.options.record.id,
                nombre : this.options.record.nombre,
                activo: this.options.record.activo
            }       
            zPost("saveServicioReporte.man", { zToken: window.zSecurityToken, servicio: u })
                .then(servicio => {
                    this.cmdOk.enable();
                    this.loading.hide();
                    this.close(servicio)
                }
                )
                .catch(error => this.showDialog("common/WError", { message: error.toString() }));
        
        }
    }
}