class CustomController extends ZCustomComponent {
    onThis_init(options) {
        this.options = options;
        this.mensajeError.hide();
        this.loading.hide();
        if (options.newRecord) {
            this.title.view.text("Crear Nuevo Privilegio");
            this.lblAccion.view.text("Nuevo Privilegio: ");
            this.edActivo.checked = true;
        } else {
            this.title.view.text("Editar Privilegio");
            this.lblAccion.view.text("Editar Privilegio: ");
            let u = options.record;
            this.edNombre.val = u.nombre;
            this.edActivo.checked = u.activo;
            this.edCodigo.val = u.codigo;
        }

        let filtro = {
            filtro: "",
            activo: "S"
        }
    }

    onCmdCancel_click() { this.cancel() }
    muestraError(txt) {
        this.textoMensajeError.view.text(txt);
        this.mensajeError.show();
    }
    onCmdOk_click() {
        let u = {
            nombre: this.edNombre.val,
            codigo: this.edCodigo.val,
            activo: this.edActivo.checked
        }
        let findError = false;

        if (!u.nombre) {
            findError = true;
            this.edNombre.setValidation(false);
        } else {
            this.edNombre.setValidation(true);
        }

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        this.cmdOk.disable();
        this.loading.show();
        this.mensajeError.hide();

        if (this.options.newRecord) {
         
            zPost("addPrivilegio.seg", { zToken: window.zSecurityToken, privilegio: u })
                .then(privilegio => {
                    this.cmdOk.enable();
                    this.loading.hide();
                    this.close(privilegio)
                })
                .catch(error => this.showDialog("common/WError", { message: error.toString() }));
        } else {
            this.cmdOk.disable();
            zPost("savePrivilegio.seg", { zToken: window.zSecurityToken, privilegio: u })
                .then(privilegio => {
                    this.cmdOk.enable();
                    this.loading.hide();
                    this.close(privilegio)
                }
                )
                .catch(error => this.showDialog("common/WError", { message: error.toString() }));
        }
    }
}