class CustomController extends ZCustomComponent {
    onThis_init() {
        this.refresca();
    }
 
    onEdFiltro_change() { this.refresca() }
    refresca() { this.listaPrivilegios.refresh() }

    preparaFila(row) {
        delete row._rowClass;
        if (!row.activo) {
            row._rowClass = "table-danger";
        }
        let edActivo =
            "<div class='togglebutton text-center'><label style='line-height:0.6;'>" +
            "<input class='activo-toggler' data-id-privilegio='" + row.id + "'  type='checkbox'" + (row.activo ? " checked='checked'" : "") + ">" +
            "   <span class='toggle'></span>" +
            "</label></div>";

        row.edActivo = edActivo;
        return row;
    }

    registraHandlersFilas() {
        this.listaPrivilegios.view.find(".activo-toggler").change(e => {
            let c = $(e.currentTarget).prop("checked") ? true : false;
            let idPrivilegio = $(e.currentTarget).data("data-id-privilegio");
            let idx = this.listaPrivilegios.rows.findIndex(r => r.id == idPrivilegio);
            if (idx < 0) return;
            let r = this.listaPrivilegios.rows[idx];
            r.activo = c;
            r.listaPrivilegios = '-1';
            r.loginModificacion= window.app.sesion.usuario.login;
            zPost("savePrivilegio.seg", { privilegio: r })
                    .then(() => {
                        this.refresca();
                    })
                    .catch(error => this.showDialog("common/WError", { message: error.toString() }));
            });
    }
    onListaPrivilegios_afterPaint() {
        this.registraHandlersFilas();
    }
    onListaPrivilegios_getRows(cb) {
 
        let filtro = {
            filtro: this.edFiltro.val.trim()
        }
        zPost("getPrivilegios.seg", { filtro: filtro }, privilegios => {

            privilegios.forEach(u => this.preparaFila(u));
            cb(privilegios);
        });
    }

    onCmdAgregarPrivilegio_click() {
        this.showDialog("./wed-privilegios", { newRecord: true }, privilegio => this.refresca());
    }
    onListaPrivilegios_editRequest(idx, row) {
        this.showDialog("./wed-privilegios", { record: row }, privilegio => {
            this.refresca();
        });
    }
    onListaPrivilegios_deleteRequest(idx, row) {
        this.showDialog("common/WConfirm", { message: "¿Confirma que desea eliminar el privilegio '" + row.privilegio + "'?" }, () => {
            zPost("deletePrivilegio.seg", { privilegio: row, loginModificacion: window.app.sesion.usuario.login })
                .then(() => {
                    this.refresca();
                })
                .catch(error => this.showDialog("common/WError", { message: error.toString() }));

        });
    }
}