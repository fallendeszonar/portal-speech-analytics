class CustomController extends ZCustomComponent {
    onThis_init() {
        this.refresca();
        this.controller = this;
    }

    onEdFiltro_change() { this.refresca() }
    refresca() { this.listaClientesContrato.refresh() }

    preparaFila(row) {
        delete row._rowClass;
        if (!row.activo) {
            row._rowClass="table-danger";
        } 
        let edActivo = 
            "<div class='togglebutton text-center'><label style='line-height:0.6;'>" +
                "<input class='activo-toggler' data-login='" + row.login + "' type='checkbox'" + (row.activo?" checked='checked'":"") + ">" +
                "   <span class='toggle'></span>" +
            "</label></div>";
        row.edActivo = edActivo;
       
        return row;
    }
    registraHandlersFilas() {
        this.listaClientesContrato.view.find(".activo-toggler").change(e => {
            let login = $(e.currentTarget).data("login"); 
            let idx = this.listaClientesContrato.rows.findIndex(r => r.login == login);
            if (idx < 0) return;
            let r = this.listaClientesContrato.rows[idx];
            r.loginModificacion= window.app.sesion.usuario.login;
 
        });
    }
    onListaClientesContrato_getRows(cb) {
        let filtro = {
            filtro: this.edFiltro.val.trim() 
        }
        zPost("getClientesConContrato.man", {filtro:filtro}, clientescontrato => { 
            clientescontrato.forEach(u => this.preparaFila(u));
            cb(clientescontrato);
        });
    }
    onListaClientesContrato_afterPaint() {
        this.registraHandlersFilas();
    }
    onCmdAgregarClienteContrato_click() {
        this.showDialog("./wed-cliente_contrato", {newRecord:true}, cliente => this.refresca());
    }
    onListaClientesContrato_editRequest(idx, row) {
        this.showDialog("./wed-cliente_contrato", {record:row}, cliente => {

            this.listaClientesContrato.updateRow(idx, this.preparaFila(cliente));
        });
    }
    onListaClientesContrato_deleteRequest(idx, row) {
        this.showDialog("common/WConfirm", {message:"¿Confirma que desea eliminar la relación "+row.nombreCliente+" / "+row.nombreContrato+", se eliminaran también su asociación con los servicios ?"}, () => {
            zPost("deleteClienteContratoServicio.man", {zToken:window.zSecurityToken, clientecontratoservicio:row})
                .then(() => {
                    this.listaClientesContrato.deleteRow(idx);
                })
                .catch(error => this.showDialog("common/WError", {message:error.toString()}));
            });
    }    
}