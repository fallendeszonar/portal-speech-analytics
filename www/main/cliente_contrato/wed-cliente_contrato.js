let serviceSelect;

class CustomController extends ZCustomComponent {
onThis_init(options) {
    this.options = options;
    this._arrListaServiciosSel = [];
    this.mensajeError.hide();
    this.loading.hide();
    this.refresca();

    
    if (options.newRecord) {
        this.divContratos.hide();
        this.divListaServicios.hide();

        this.title.view.text("Crear Nuevo Cliente / Contrato");
        this.lblAccion.view.text("Nuevo Cliente / Contrato: ");
       
        zPost("getClientesContratoSinServicios.man")
        .then(clientes =>{ this.edClientes.setRows(clientes, null);})
        .catch(error => console.error(error));
        
    } else {
        this.title.view.text("Editar Cliente / Contrato");
        this.lblAccion.view.text("Editar Cliente / Contrato: ");
        let u = options.record;

        zPost("getClientes.man")
        .then(clientes =>{ this.edClientes.setRows(clientes, options.record.idCliente);})
        .catch(error => console.error(error));
        this.edClientes.disable();
        let c = {
            id: options.record.idCliente
        }
        zPost("getAllContratosPorCliente.man", { cliente: c })
        .then(contratos => {
            this.edContratos.setRows(contratos, options.record.idContrato);})
        .catch(error => console.error(error));
        this.edContratos.disable();
    }
}
refresca() { this.listaServicios.refresh() }


    preparaFila(row) {
        delete row._rowClass;
        if (row.activo) {
            row._rowClass = null;
            row.imgActivo = "<span class='fa fa-check'></span>";
        } else {
            row.imgActivo = "<span class='fa fa-ban'></span>";
        }
        return row;
    }
    onListaServicios_columnClick(rowIndex, columnName, row) {
        if (this._arrListaServiciosSel.indexOf(row.id) >= 0) {
            this._arrListaServiciosSel.splice(this._arrListaServiciosSel.indexOf(row.id), 1);
        } else {  
            this._arrListaServiciosSel.push(row.id);
        }
        
        switch (columnName) {
            case "imgActivo":
                row.activo = !row.activo;        
                this.preparaFila(row);
                this.listaServicios.updateRow(rowIndex, row);
                break;
        }
    }
    onListaServicios_getRows(cb) { 
        if (this.options.newRecord) {

            zPost("getServicios.man",{filtro:{filtro :'', clientecontrato:true}}, servicios => {
                servicios.forEach(s => this.preparaFila(s));
                cb(servicios);
            });
        } else {
            let ccs = {
                idCliente : options.record.idCliente,
                idContrato :  options.record.idContrato
            }   
            zPost("getServiciosClienteContratoMan.man",{clientecontrato : ccs}, servicios => {
                servicios.forEach(c => this.preparaFila(c));
                cb(servicios);
                this.cargaServiciosUpdate(servicios);
            });
        }
    }
    cargaServiciosUpdate(servicios){
        let thisController = this;
        $.each(this._arrListaServiciosSel, function (item) {
            thisController._arrListaServiciosSel.push(servicios[item].id);
        });
    }


    onEdClientes_change(){
       this.divContratos.show();
       this.edContratos.show();

       let c = {
            id: this.edClientes.val
        }
        zPost("getContratosPorClienteDisp.man", { cliente: c })
        .then(contratos => {this.edContratos.setRows(contratos);})
        .catch(error => console.error(error));

    }

    onEdContratos_change(){
        this.divListaServicios.show();
        this.listaServicios.show();
    }

    onCmdCancel_click() { this.cancel() }
    muestraError(txt) {
        this.textoMensajeError.view.text(txt);
        this.mensajeError.show();
    }
    onCmdOk_click() {
        this.loading.show();
        this.mensajeError.hide();
        
        let ccs = {
            idCliente : this.edClientes.val,
            nomCliente : options.newRecord?'sda' : options.record.nombreCliente,
            idContrato :  this.edContratos.val,
            nomContrato : options.newRecord?'sda' : options.record.nombreContrato,
            listaServicios : this._arrListaServiciosSel
        } 
        
        let findError = false;
        if(ccs.listaServicios.length == 0){
            findError=true;
         }
        if (findError) {
            this.muestraError(" Es necesario seleccionar al menos un Servicio.");
            return;
        }

        this.mensajeError.hide();

        if (this.options.newRecord) {            
            zPost("addClienteContratoServicio.man", { zToken: window.zSecurityToken, clientecontratoservicio: ccs })
                .then(servicio => {
                    this.cmdOk.enable();
                    this.loading.hide();
                    this.close(servicio)
                })
                .catch(error => this.showDialog("common/WError", { message: error.toString() }));
            } else {     
            zPost("saveClienteContratoServicio.man", { zToken: window.zSecurityToken, clientecontratoservicio: ccs })
                .then(clientecontratoservicio => {
                    this.cmdOk.enable();
                    this.loading.hide();
                    this.close(clientecontratoservicio)
                }
                )
                .catch(error => this.showDialog("common/WError", { message: error.toString() }));
        
        }
    }
}