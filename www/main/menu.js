let html = '';
class CustomController extends ZCustomComponent {
	onThis_init() {
		
		this.edNombreUsuario.view.html(window.app.sesion.usuario.nombre);
		let sesion = window.app.sesion.usuario;
		console.log(sesion);
		let filtro = {
            filtro: "",
            activo: "S"
		}
		this.divClienteContrato.hide();
		
		this.tituloRep.hide();
		this.edListaMenu.hide();
		
		this.tituloAdm.hide();
		this.liAdministracion.hide();
		
		this.cmdReportes.hide();
		this.cmdServicios.hide();
		this.cmdServiciosReportes.hide();
		this.cmdClienteContrato.hide();
		this.cmdUsuarios.hide();
		this.cmdHeader.hide();
		this.cmdTabla2.hide();
		this.cmdTabla3.hide();

		// ADMINISTRADOR SIXBELL
		if(sesion.admin && sesion.tipo==0){

			this.divClienteContrato.show();
			
			this.tituloRep.show();
			this.edListaMenu.show();

			this.tituloAdm.show();
			this.liAdministracion.show();
			this.cmdReportes.show();
			this.cmdServicios.show();
			this.cmdServiciosReportes.show();
			this.cmdClienteContrato.show();
			this.cmdUsuarios.show();
			this.cmdHeader.show();
			this.cmdTabla2.show();
			this.cmdTabla3.show();
			zPost("getClientes.man", {})
			.then(clientes => {console.log(clientes); this.edClientes.setRows(clientes); })
			.catch(error => console.error(error));

		// ADMINISTRADOR CLIENTE
		}else if(sesion.admin && sesion.tipo==1){
			
			this.tituloRep.show();
			this.edListaMenu.show();

			this.tituloAdm.show();

			this.edListaMenu.show();

			this.construirMenu2(sesion.cliente,sesion.contrato);
			
			this.liAdministracion.show();			
			this.cmdUsuarios.show();



		// USUARIO SIXBELL
		}else if(!sesion.admin && sesion.tipo==0){
			let usuario = {
				idUsuario : sesion.id,
				idCliente : sesion.cliente,
				idContrato : sesion.contrato
			}
			this.construirMenu(usuario);
		// USUARIO CLIENTE	
		}else if(!sesion.admin && sesion.tipo==1){
			
			this.tituloRep.show();
			this.edListaMenu.show();

			let usuario = {
				idUsuario : sesion.id,
				idCliente : sesion.cliente,
				idContrato : sesion.contrato
			}
			this.construirMenu(usuario);
		}
	}

	onEdClientes_change(){
		this.edContratos.enable();
        let c = {
            id: this.edClientes.val
        }
        zPost("getAllContratosPorCliente.man", { cliente: c })
        .then(contratos => this.edContratos.setRows(contratos))
        .catch(error => console.error(error));
	}
	
	onEdContratos_change(){
		html='';
		this.edListaMenu.view.html(html);

		this.construirMenu2(this.edClientes.val, this.edContratos.val);
	
	}
	onOpLogout_click() {
	    this.triggerEvent("logout");
	}

	construirMenu(usuario){
		zPost("getServiciosPorUsuario.man", {usuario : usuario}, servicios =>{
			servicios.map(async servicio => {
				await zPost("getReportesAsocServicioUsuario.man", {usuario:usuario, servicio: servicio}, reportes => {
					html+="<li  id='cmdServicioBase"+servicio.id+"'>";
					html+="<a href='#homeSubmenu"+servicio.id+"' data-toggle='collapse' aria-expanded='false' class='dropdown-toggle'>"+servicio.nombre+"</a>";
					html+="<ul class='collapse list-unstyled' id='homeSubmenu"+servicio.id+"'>";
					reportes.map(reporte => {
						html+="<li><a>";
						html+=reporte.nombre;
						html+='</a></li>';
					});
					html+= "</ul></li>";
					this.edListaMenu.view.html(html);
				});
			});
		});
	}

	construirMenu2(idCliente,idContrato){

		let cc = {
			idCliente : idCliente,
			idContrato : idContrato
		}
		zPost("getServiciosClienteContrato.man", {clientecontrato : cc}, servicios =>{
			servicios.map(async servicio => {
				await zPost("getReportesPorServicioAdm.man", {servicio:servicio}, reportes => {
					html+="<li id='cmdServicioBase"+servicio.id+"'>";
					html+="<a href='#homeSubmenu"+servicio.id+"' data-toggle='collapse' aria-expanded='false' class='dropdown-toggle'>"+servicio.nombre+"</a>";
					html+="<ul class='collapse list-unstyled' id='homeSubmenu"+servicio.id+"'>";
					reportes.map(reporte => {
						html+="<li><a>";
						html+=reporte.nombre;
						html+='</a></li>';
					});
					html+= "</ul></li>";
					this.edListaMenu.view.html(html);
				});
			});
		});
	}	
	// Opciones menú configurar
	onCmdUsuarios_click() {
	this.mainLoader.load("./usuarios/usuarios");
	}
	onCmdPerfiles_click() {
		this.mainLoader.load("./perfiles/perfiles");
	}
	onCmdPrivilegios_click() {
		this.mainLoader.load("./privilegios/privilegios");
	}
	onCmdReportes_click() {
		this.mainLoader.load("./reportes/reportes");
	}
	onCmdServicios_click() {
		this.mainLoader.load("./servicios/servicios");
	}
	onCmdCambiarPwd_click() {
		this.showDialog("../login/WCambiarPwd");
	}
	onCmdHome_click(){
		this.mainLoader.load("./home");
	}
	onCmdServiciosReportes_click(){
		this.mainLoader.load("./servicio_reporte/servicio_reporte");
	}
	onCmdClienteContrato_click(){
		this.mainLoader.load("./cliente_contrato/cliente_contrato");
	}
	onCmdHeader_click(){
		this.mainLoader.load("./conf-header/conf-header");
	}
	onCmdTabla2_click(){
		this.mainLoader.load("./conf-map-datos/conf-map-datos");
	}
	onCmdTabla3_click(){
		this.mainLoader.load("./conf-map-cli-con-servicio/conf-map-cli-con-servicio");
	}
};
