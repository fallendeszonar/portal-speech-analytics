class CustomController extends ZCustomComponent {
    onThis_init() {
        this.refresca();
        this.controller = this;
    }

    onEdFiltro_change() { this.refresca() }
    refresca() { this.listaReportes.refresh() }

    preparaFila(row) {
        delete row._rowClass;
        if (!row.activo) {
            row._rowClass="table-danger";
        } 
        let edActivo = 
            "<div class='togglebutton text-center'><label style='line-height:0.6;'>" +
                "<input class='activo-toggler' data-login='" + row.login + "' type='checkbox'" + (row.activo?" checked='checked'":"") + ">" +
                "   <span class='toggle'></span>" +
            "</label></div>";
        row.edActivo = edActivo;
       
        return row;
    }
    registraHandlersFilas() {
        this.listaReportes.view.find(".activo-toggler").change(e => {
            let c = $(e.currentTarget).prop("checked")?true:false;
            let login = $(e.currentTarget).data("login"); 
            let idx = this.listaReportes.rows.findIndex(r => r.login == login);
            if (idx < 0) return;
            let r = this.listaReportes.rows[idx];
            r.activo = c;
            r.loginModificacion= window.app.sesion.usuario.login;

            if (window.app.sesion.usuario.login == login && !c){
                let msg = "¿Confirma que desea desactivar su reporte. Esto no le permitirá seguir operando en el sistema?"
                this.showDialog("common/WConfirm", {message: msg}, () => {
           
                    zPost("saveReporte.man", {zToken:window.zSecurityToken,reporte:r})
                    .then(() => {
                        this.refresca();
                    })
                    .catch(error => this.showDialog("common/WError", {message:error.toString()}));
                }
                , () => {
                    this.refresca();
                }
                );
            }else{
                zPost("saveReporte.man", {zToken:window.zSecurityToken,reporte:r})
                .then(() => {
                    this.refresca();
                })
                .catch(error => this.showDialog("common/WError", {message:error.toString()}));
            }    
        });
    }

    onListaReportes_getRows(cb) {
        let filtro = {
            filtro: this.edFiltro.val.trim() 
        }
        zPost("getReportes.man", {zToken:window.zSecurityToken,filtro:filtro}, reportes => {  
            reportes.forEach(u => this.preparaFila(u));
            cb(reportes);
        });
    }
    onListaReportes_afterPaint() {
        this.registraHandlersFilas();
    }
    onCmdAgregarReporte_click() {
        this.showDialog("./wed-reportes", {newRecord:true}, reporte => this.refresca());
    }
    onListaReportes_editRequest(idx, row) {
        this.showDialog("./wed-reportes", {record:row}, reporte => {
            this.listaReportes.updateRow(idx, this.preparaFila(reporte));
        });
    }
    onListaReportes_deleteRequest(idx, row) {
        this.showDialog("common/WConfirm", {message:"¿Confirma que desea eliminar el reporte '" + row.nombre + "', si elimina este reporte, automáticamente se eliminara la asociación en todos aquellos servicios que tengan asociado el Reporte?"}, () => {
            zPost("deleteReporte.man", {zToken:window.zSecurityToken, reporte:row, loginModificacion: window.app.sesion.usuario.login})
                .then(() => {
                    this.listaReportes.deleteRow(idx);
                })
                .catch(error => this.showDialog("common/WError", {message:error.toString()}));
        });
    }    
}