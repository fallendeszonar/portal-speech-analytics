class CustomController extends ZCustomComponent {
    onThis_init(options) {
        this.options = options;
        this.mensajeError.hide();
        this.loading.hide();
        if (options.newRecord) {
            this.title.view.text("Crear Nuevo Reporte");
            this.lblAccion.view.text("Nuevo Reporte: ");
            this.edActivo.checked = true;
        } else {
            this.title.view.text("Editar Reporte");
            this.lblAccion.view.text("Editar Reporte: ");
            let u = options.record;
            this.edNombre.val = u.nombre;
            this.edFiltro.val = u.filtro;
            this.edURL.val = u.url;
            this.edActivo.checked = u.activo;
        }

        let filtro = {
            filtro: "",
            activo: "S"
        }
    }

    onCmdCancel_click() { this.cancel() }
    muestraError(txt) {
        this.textoMensajeError.view.text(txt);
        this.mensajeError.show();
    }
    onCmdOk_click() {
        let u = {
            id: this.options.newRecord  ? -1 : this.options.record.id,//idUser            nombre: this.edNombre.val,
            nombre : this.edNombre.val,
            filtro: this.edFiltro.val,
            url : this.edURL.val,
            activo: this.edActivo.checked
        }
        let findError = false;

        if (!u.nombre) {
            findError = true;
            this.edNombre.setValidation(false);
        } else {
            this.edNombre.setValidation(true);
        }

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        this.cmdOk.disable();
        this.loading.show();
        this.mensajeError.hide();

        if (this.options.newRecord) {

            zPost("addReporte.man", { zToken: window.zSecurityToken, reporte: u })
                .then(reporte => {
                    this.cmdOk.enable();
                    this.loading.hide();
                    this.close(reporte)
                })
                .catch(error => this.showDialog("common/WError", { message: error.toString() }));
        } else {
            this.cmdOk.disable();
            zPost("saveReporte.man", { zToken: window.zSecurityToken, reporte: u })
                .then(reporte => {
                    this.cmdOk.enable();
                    this.loading.hide();
                    this.close(reporte)
                }
                )
                .catch(error => this.showDialog("common/WError", { message: error.toString() }));
        }
    }
}