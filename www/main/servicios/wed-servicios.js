class CustomController extends ZCustomComponent {
    onThis_init(options) {
        this.options = options;
        this.mensajeError.hide();
        this.loading.hide();
        if (options.newRecord) {
            this.title.view.text("Crear Nuevo Servicio");
            this.lblAccion.view.text("Nuevo Servicio: ");
            this.edActivo.checked = true;
        } else {
            this.title.view.text("Editar Servicio");
            this.lblAccion.view.text("Editar Servicio: ");
            let u = options.record;
            this.edNombre.val = u.nombre;
            this.edActivo.checked = u.activo;
        }
        let filtro = {
            filtro: "",
            activo: "S"
        }
    }

    onCmdCancel_click() { this.cancel() }
    muestraError(txt) {
        this.textoMensajeError.view.text(txt);
        this.mensajeError.show();
    }
    onCmdOk_click() {
        let u = {
            nombre: this.edNombre.val,
            activo: this.edActivo.checked
        }
        let findError = false;

        if (!u.nombre) {
            findError = true;
            this.edNombre.setValidation(false);
        } else {
            this.edNombre.setValidation(true);
        }

        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        
        this.cmdOk.disable();
        this.loading.show();
        this.mensajeError.hide();

        if (this.options.newRecord) {

            zPost("addServicio.man", { zToken: window.zSecurityToken, servicio: u })
                .then(servicio => {
                    this.cmdOk.enable();
                    this.loading.hide();
                    this.close(servicio)
                })
                .catch(error => this.showDialog("common/WError", { message: error.toString() }));
        } else {
            this.cmdOk.disable();
            u['id'] = options.record.id;

            zPost("saveServicio.man", { zToken: window.zSecurityToken, servicio: u })
                .then(servicio => {
                    this.cmdOk.enable();
                    this.loading.hide();
                    this.close(servicio)
                }
                )
                .catch(error => this.showDialog("common/WError", { message: error.toString() }));
        }
    }
}