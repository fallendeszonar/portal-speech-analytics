class CustomController extends ZCustomComponent {
    onThis_init() {
        this.refresca();
        this.controller = this;
    }

    onEdFiltro_change() { this.refresca() }
    refresca() { this.listaServicios.refresh() }

    preparaFila(row) {
        delete row._rowClass;
        if (!row.activo) {
            row._rowClass="table-danger";
        } 
        let edActivo = 
            "<div class='togglebutton text-center'><label style='line-height:0.6;'>" +
                "<input class='activo-toggler' data-login='" + row.login + "' type='checkbox'" + (row.activo?" checked='checked'":"") + ">" +
                "   <span class='toggle'></span>" +
            "</label></div>";
        row.edActivo = edActivo;
       
        return row;
    }
    registraHandlersFilas() {
        this.listaServicios.view.find(".activo-toggler").change(e => {
            let c = $(e.currentTarget).prop("checked")?true:false;
            let login = $(e.currentTarget).data("login"); 
            let idx = this.listaServicios.rows.findIndex(r => r.login == login);
            if (idx < 0) return;
            let r = this.listaServicios.rows[idx];
            r.activo = c;
            r.loginModificacion= window.app.sesion.usuario.login;

            if (window.app.sesion.usuario.login == login && !c){
                let msg = "¿Confirma que desea desactivar su servicio. Esto no le permitirá seguir operando en el sistema?"
                this.showDialog("common/WConfirm", {message: msg}, () => {
           
                    zPost("saveServicio.man", {zToken:window.zSecurityToken, servicio:r})
                    .then(() => {
                        this.refresca();
                    })
                    .catch(error => this.showDialog("common/WError", {message:error.toString()}));
                }
                , () => {
                    this.refresca();
                }
                );
            }else{
                zPost("saveServicio.man", {zToken:window.zSecurityToken,servicio:r})
                .then(() => {
                    this.refresca();
                })
                .catch(error => this.showDialog("common/WError", {message:error.toString()}));
            }    
        });
    }

    onListaServicios_getRows(cb) {
        let filtro = {
            filtro: this.edFiltro.val.trim() 
        }
        zPost("getServicios.man", {filtro:filtro}, servicios => {  
            servicios.forEach(u => this.preparaFila(u));
            cb(servicios);
        });
    }
    onListaServicios_afterPaint() {
        this.registraHandlersFilas();
    }
    onCmdAgregarServicio_click() {
        this.showDialog("./wed-servicios", {newRecord:true}, servicio => this.refresca());
    }
    onListaServicios_editRequest(idx, row) {
        this.showDialog("./wed-servicios", {record:row}, servicio => {
            this.listaServicios.updateRow(idx, this.preparaFila(servicio));
        });
    }
    onListaServicios_deleteRequest(idx, row) {
        this.showDialog("common/WConfirm", {message:"¿Confirma que desea eliminar el servicio '" + row.nombre + "'', si elimina este servicio, automáticamente se eliminara su asociación con sus reportes?"}, () => {
            zPost("deleteServicio.man", {zToken:window.zSecurityToken, servicio:row})
                .then(() => {
                    this.listaServicios.deleteRow(idx);
                })
                .catch(error => this.showDialog("common/WError", {message:error.toString()}));
        });
    }    
}