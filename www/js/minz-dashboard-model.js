class MinZDashboard {
    constructor(options) {
        this._options = {
            title:"Titulo Default",
            minTemporality:"5m",
            defaultTemporality:"1d"
        }
        if (options) {
            Object.keys(options).forEach(name => this._options[name] = options[name]);
        }
        this._rows = [];
        this._period = options.period?options.period.clone():null;
        this._paramListeners = {};  // {componentId:{paramName:[listeners...], ...}}, ...}
        this._paramValues = {}      // {paramName:value, ...}
        this._guiContainer = null;
    }
    get rows() {return this._rows}
    get options() {return this._options}
    get title() {return this.options.title}
    get minTemporality() {return this.options.minTemporality}
    get defaultTemporality() {return this.options.defaultTemporality}
    get period() {return this._period}
    set period(p) {this._period = p; this.setParam("period", p);}  
    get guiContainer() {return this._guiContainer}
    set guiContainer(c) {this._guiContainer = c}
    addRow(options) {
        let newRow = new MinZRow(this, null, options);
        this.rows.push(newRow);
        return newRow;
    }
    get components() {
        let ret = [];
        this.rows.forEach(row => row.addComponents(ret));
        return ret;
    }
    registerParamListener(component, paramName, listener) {
        var componentListeners = this._paramListeners[component.id];
        if (!componentListeners) {
            componentListeners = {};
            this._paramListeners[component.id] = componentListeners;
        }
        var listeners = componentListeners[paramName];        
        if (!listeners) {
            listeners = [];
            componentListeners[paramName] = listeners;
        }
        listeners.push(listener);
    }    
    unregisterParamListenersForComponent(component) {
        delete this._paramListeners[component.id];
    }
    extractParamNamesFromText(txt) {
        var params = [];
        var p0 = txt.indexOf("${");
        while (p0 >= 0) {
            var p1 = txt.indexOf("}", p0);
            if (p1 > p0) {
                var paramName = txt.substring(p0+2, p1);
                if (!params.find(p => p == paramName)) {
                    params.push(paramName);
                }
                p0 = txt.indexOf("${", p1);
            } else {
                p0 = -1;
            }
        }
        return params;
    }
    registerParamListenersFromText(component, text, listener) {
        var params = this.extractParamNamesFromText(text);
        params.forEach(p => this.registerParamListener(component, p, listener));
        return params.length;
    }
    getParam(name) {
        if (name.startsWith("period.")) {
            let subperiod = name.substring(7);
            return this._paramValues["period"].getSubperiod(subperiod);
        } else {
            return this._paramValues[name];
        }
    }
    setParam(name, value) {
        this._paramValues[name] = value;
        Object.keys(this._paramListeners).forEach(componentId => {
            var listeners = this._paramListeners[componentId][name];
            if (listeners) listeners.forEach(l => this.invokeDelayedListener(l, value));
        });
    }
    invokeDelayedListener(listener, value) {
        if (listener.timer) clearTimeout(listener.timer);
        listener.timer = setTimeout(() => {
            listener.timer = null;
            listener(value);
        }, 200);
    }
    expandFilter(filterSpec, forExplanation) {
        var filter = this.prepareFilterFrom(filterSpec, forExplanation);
        return filter;
    }
    prepareFilterFrom(filterSpec, forExplanation) {
        if (!filterSpec) return null;
        if (Array.isArray(filterSpec)) {
            return filterSpec;
        } else if (typeof filterSpec == "object") {
            if (filterSpec.code) {
                if (forExplanation) {
                    return {code:filterSpec.code, name:filterSpec.name};
                } else {
                    return filterSpec.code;
                }
            }
            var obj = {};
            Object.keys(filterSpec).forEach(name => {
                if (name != "_id" && name != "order" && name != "name") {
                    var v = this.prepareFilterFrom(filterSpec[name], forExplanation);
                    if (v) obj[name] = v;
                    //obj[name] = v;
                }
            });
            if (!Object.keys(obj).length) return null;
            return obj;
        } else if (typeof filterSpec == "string") {
            if (filterSpec.startsWith("${") && filterSpec.endsWith("}")) {
                var paramName = filterSpec.substring(2, filterSpec.length - 1);
                var v = this.getParam(paramName);
                if (!v) return null;
                return this.prepareFilterFrom(v, forExplanation);
            } else {
                // Fixed dim code
                return filterSpec;
            }
        }
    }
    explainFilterFrom(filterSpec) {
        if (!filterSpec) return null;
        if (typeof filterSpec == "object") {
            if (filterSpec.name) return " '" + filterSpec.name + "'";
            var obj = " en (";
            Object.keys(filterSpec).forEach(name => {
                if (name != "_id" && name != "order" && name != "name") {
                    if (obj.length > 5) obj += " y ";
                    obj += name.substring(0,1).toUpperCase() + name.substring(1);
                    obj += this.explainFilterFrom(filterSpec[name]);
                }
            });
            if (obj.length <= 5) return "";
            return obj + ")";
        } else if (typeof filterSpec == "string") {
            if (filterSpec.startsWith("${") && filterSpec.endsWith("}")) {
                var paramName = filterSpec.substring(2, filterSpec.length - 1);
                var v = this.getParam(paramName);
                if (!v) return "";
                return this.explainFilterFrom(v);
            } else {
                // Fixed dim code
                return " con código:'" + filterSpec + "'";
            }
        }
    }
    explainFilter(filterSpec) {
        var exp = this.explainFilterFrom(this.expandFilter(filterSpec, true));
        return exp?exp.trim():"";
    }
    expandText(txt, localParams) {
        var p0 = txt.indexOf("${");
        var st = p0 >= 0?txt.substring(0, p0):txt;
        while (p0 >= 0) {
            var p1 = txt.indexOf("}", p0);
            if (p1 > p0) {
                var paramName = txt.substring(p0+2, p1);
                var value = localParams?localParams[paramName]:null;
                if (!value) value = this.getParam(paramName);
                if (!value) {
                    value = "[Todos]";
                } else if (typeof value == "object") {
                    if (paramName == "period" || paramName.startsWith("period.")) {
                        value = value.describe();
                    } else if (value.name) {
                        value = value.name;
                    } else {
                        value = this.explainFilter(value);
                    }
                }
                st += value;
                p0 = txt.indexOf("${", p1);
                if (p0 > 0) st += txt.substring(p1+1, p0);
                else st += txt.substring(p1 + 1);
            } else {
                st += txt.substring(p0);
                p0 = -1;
            }
        }
        return st;
    }
    selectOption(title, options) {
        this.guiContainer.selectOption(title, options);
    }
    push(dir, file, options) {
        this.guiContainer.push(dir, file, options);
    }
    showDialog(path, options, onOk, onCancel) {this.guiContainer.showDialog(path, options, onOk, onCancel)}
}

class MinZRow {
    constructor(dashboard, parentCol, options) {
        this._dashboard = dashboard;
        this._options = options?options:{};
        this._cols = [];
        this._parentCol = parentCol;
    }
    get dashboard() {return this._dashboard}
    get options() {return this._options}
    get height() {return this.options.height}
    get cols() {return this._cols}
    addCol(options) {
        let newCol = new MinZCol(this, options);
        this.cols.push(newCol);
        return newCol;
    }
    end() {return this._parentCol?this._parentCol:this._dashboard}
    addComponents(ret) {
        this.cols.forEach(col => col.addComponents(ret));
    }
}

class MinZCol {
    constructor(row, options) {
        this._row = row;
        this._options = options?options:{};
        this._rows = [];
        this._component = null;
    }
    get row() {return this._row}
    get dashboard() {return this.row.dashboard}
    get options() {return this._options}
    get width() {return this.options.width}
    get rows() {return this._rows}
    addRow(options) {
        let newRow = new MinZRow(this.dashboard, this, options);
        this.rows.push(newRow);
        return newRow;
    }
    end() {return this._row}
    setComponent(id, c) {
        this._component = c;
        c.id = id;
        c.dashboard = this.dashboard;
        this.dashboard[id] = c;
        return this;
    }
    get component() {return this._component}
    addComponents(ret) {
        if (this.component) {
            ret.push(this.component);
        } else {
            this.rows.forEach(row => row.addComponents(ret));
        }
    }
}

class MinZDashboardComponent {
    constructor(defaults, options) {
        this._options = defaults?defaults:{};
        if (options) {
            Object.keys(options).forEach(name => this._options[name] = options[name]);
        }
        this._id = null;
        this._dashboard = null;
        this._container = null;
    }
    get id() {return this._id}
    set id(id) {this._id = id}
    get dashboard() {return this._dashboard}
    set dashboard(d) {this._dashboard = d};
    get container() {return this._container}
    set container(element) {this._container = element}
    get options() {return this._options}    
    firstRender(container) {
        this.container = container;
    }
    doResize() {}
    init() {}
    showDialog(path, options, onOk, onCancel) {this.dashboard.guiContainer.showDialog(path, options, onOk, onCancel)}
}

class MinZClientPlugin {
    constructor() {        
    }
    get code() {return "abstractPlugin"}
    getRequiredCSSs() {return []}
    getRequiredScripts() {return []}
    start() {}
}

class MinZPeriod {
    constructor(temporalityLevel, startTime, endTime) {
        // Special initializers
        if (temporalityLevel == "last30days") {
            this.temporality = "1d";
            let dt1 = new Date(); dt1.setHours(0); dt1.setMinutes(0); dt1.setSeconds(0); dt1.setMilliseconds(0);
            let dt0 = new Date(dt1.getTime());
            dt0.setDate(dt0.getDate() - 30);
            dt1.setDate(dt1.getDate() + 1);
            this.startTime = dt0.getTime();
            this.endTime = dt1.getTime();
        } else {
            this.temporality = temporalityLevel;
            this.startTime  = startTime;
            this.endTime = endTime;
            if (!startTime && !endTime) this.initDefaultPeriod();
            else if (!endTime) this.initDefaultEndTime();        
        }
    }

    clone() {
        return new MinZPeriod(this.temporality, this.startTime, this.endTime);
    }
    initDefaultPeriod() {
        var dt = new Date(), dt2;
        switch(this.temporality) {
            case "5m":     //  5m
                dt.setMinutes(5 * parseInt(dt.getMinutes() / 5));
                dt.setSeconds(0);
                dt.setMilliseconds(0);
                dt2 = new Date(dt.getTime());
                dt2.setMinutes(dt2.getMinutes() + 5);
                this.startTime = dt.getTime();
                this.endTime = dt2.getTime();
                break;
            case "15m":     // 15m
                dt.setMinutes(15 * parseInt(dt.getMinutes() / 15));
                dt.setSeconds(0);
                dt.setMilliseconds(0);
                dt2 = new Date(dt.getTime());
                dt2.setMinutes(dt2.getMinutes() + 15);
                this.startTime = dt.getTime();
                this.endTime = dt2.getTime();
                break;
            case "30m":     // 30m
                dt.setMinutes(30 * parseInt(dt.getMinutes() / 30));
                dt.setSeconds(0);
                dt.setMilliseconds(0);
                dt2 = new Date(dt.getTime());
                dt2.setMinutes(dt2.getMinutes() + 30);
                this.startTime = dt.getTime();
                this.endTime = dt2.getTime();
                break;
            case "1h":     //  1h
                dt.setMinutes(0);
                dt.setSeconds(0);
                dt.setMilliseconds(0);
                dt2 = new Date(dt.getTime());
                dt2.setHours(dt2.getHours() + 1);
                this.startTime = dt.getTime();
                this.endTime = dt2.getTime();
                break;
            case "6h":     //  6h
                dt.setHours(6 * parseInt(dt.getHours() / 6));
                dt.setMinutes(0);
                dt.setSeconds(0);
                dt.setMilliseconds(0);
                dt2 = new Date(dt.getTime());
                dt2.setHours(dt2.getHours() + 6);
                this.startTime = dt.getTime();
                this.endTime = dt2.getTime();
                break;
            case "12h":     // 12h
                dt.setHours(12 * parseInt(dt.getHours() / 12));
                dt.setMinutes(0);
                dt.setSeconds(0);
                dt.setMilliseconds(0);
                dt2 = new Date(dt.getTime());
                dt2.setHours(dt2.getHours() + 12);
                this.startTime = dt.getTime();
                this.endTime = dt2.getTime();
                break;
            case "1d":     //  1d
                dt.setHours(0);
                dt.setMinutes(0);
                dt.setSeconds(0);
                dt.setMilliseconds(0);
                dt2 = new Date(dt.getTime());
                dt2.setDate(dt2.getDate() + 1);
                this.startTime = dt.getTime();
                this.endTime = dt2.getTime();
                break;
            case "1M":     //  1M
                dt.setDate(1);
                dt.setHours(0);
                dt.setMinutes(0);
                dt.setSeconds(0);
                dt.setMilliseconds(0);
                dt2 = new Date(dt.getTime());
                dt2.setMonth(dt2.getMonth() + 1);
                this.startTime = dt.getTime();
                this.endTime = dt2.getTime();
                break;
            case "3M":     //  3M
                dt.setMonth(3 * parseInt(dt.getMonth() / 3));
                dt.setDate(1);
                dt.setHours(0);
                dt.setMinutes(0);
                dt.setSeconds(0);
                dt.setMilliseconds(0);
                dt2 = new Date(dt.getTime());
                dt2.setMonth(dt2.getMonth() + 3);
                this.startTime = dt.getTime();
                this.endTime = dt2.getTime();
                break;
            case "4M":     //  4M
                dt.setMonth(4 * parseInt(dt.getMonth() / 4));
                dt.setDate(1);
                dt.setHours(0);
                dt.setMinutes(0);
                dt.setSeconds(0);
                dt.setMilliseconds(0);
                dt2 = new Date(dt.getTime());
                dt2.setMonth(dt2.getMonth() + 4);
                this.startTime = dt.getTime();
                this.endTime = dt2.getTime();
                break;
            case "6M":    //  6M
                dt.setMonth(6 * parseInt(dt.getMonth() / 6));
                dt.setDate(1);
                dt.setHours(0);
                dt.setMinutes(0);
                dt.setSeconds(0);
                dt.setMilliseconds(0);
                dt2 = new Date(dt.getTime());
                dt2.setMonth(dt2.getMonth() + 6);
                this.startTime = dt.getTime();
                this.endTime = dt2.getTime();
                break;
            case "1y":    //  1y
                dt.setMonth(0);
                dt.setDate(1);
                dt.setHours(0);
                dt.setMinutes(0);
                dt.setSeconds(0);
                dt.setMilliseconds(0);
                dt.setFullYear(dt.getFullYear() - 2);
                dt2 = new Date(dt.getTime());
                dt2.setFullYear(dt2.getFullYear() + 3);
                this.startTime = dt.getTime();
                this.endTime = dt2.getTime();
                break;
        }
    }
    initDefaultEndTime() {
        var dt = new Date(this.startTime);
        switch(this.temporality) {
            case "5m":     //  5m
                dt.setMinutes(dt.getMinutes() + 5);
                this.endTime = dt.getTime();
                break;
            case "15m":     // 15m
                dt.setMinutes(dt.getMinutes() + 15);
                this.endTime = dt.getTime();
                break;
            case "30m":     // 30m
                dt.setMinutes(dt.getMinutes() + 30);
                this.endTime = dt.getTime();
                break;
            case "1h":     //  1h
                dt.setHours(dt.getHours() + 1);
                this.endTime = dt.getTime();
                break;
            case "6h":     //  6h
                dt.setHours(dt.getHours() + 6);
                this.endTime = dt.getTime();
                break;
            case "12h":     // 12h
                dt.setHours(dt.getHours() + 12);
                this.endTime = dt.getTime();
                break;
            case "1d":     //  1d
                dt.setDate(dt.getDate() + 1);
                this.endTime = dt.getTime();
                break;
            case "1M":     //  1M
                dt.setMonth(dt.getMonth() + 1);
                this.endTime = dt.getTime();
                break;
            case "3M":     //  3M
                dt.setMonth(dt.getMonth() + 3);
                this.endTime = dt.getTime();
                break;
            case "4M":     //  4M
                dt.setMonth(dt.getMonth() + 4);
                this.endTime = dt.getTime();
                break;
            case "6M":    //  6M
                dt.setMonth(dt.getMonth() + 6);
                this.endTime = dt.getTime();
                break;
            case "1y":    //  1y
                dt.setFullYear(dt.getFullYear() + 1);
                this.endTime = dt.getTime();
                break;
        }
    }
    describe() {
        var dt0 = new Date(this.startTime);
        var yy0 = dt0.getFullYear();
        var MM0 = dt0.getMonth();
        var dd0 = dt0.getDate();
        var hh0 = dt0.getHours();
        var mm0 = dt0.getMinutes();
        var dt1 = new Date(this.endTime);
        var yy1 = dt1.getFullYear();
        var MM1 = dt1.getMonth();
        var dd1 = dt1.getDate();
        var hh1 = dt1.getHours();
        var mm1 = dt1.getMinutes();
        
        // 1.One Year:    2018-00-01 00:00 - 2019-00-01 00:00     -> [2018]
        // 2.Years:       2015-00-01 00:00 - 2018-00-01 00:00     -> [2015 - 2017]
        // 3 One Month    2015-03-01 00:00 - 2015-04-01 00:00     -> [04/2015]
        // 4.Months:      2015-03-01 00:00 - 2015-06-01 00:00     -> [04/2015 - 06/2015]
        // 5 One Day:     2015-03-07 00:00 - 2015-03-08 00:00     -> [07/04/2015]
        // 6.Days:        2015-03-04 00:00 - 2015-05-09 00:00     -> [04/04/2015 - 08/04/2015]
        // 7.Hours:       2015-05-03 13:00 - 2015-05-06 16:00     -> [03/06/2015 13:00 - 06/06/2015 15:59]
        // 8.General:     2015-05-03 13:25 - 2015-05-06 16:30     -> [03/06/2015 13:25 - 06/05/2015 16:30[
        // 9.End Hour:    2015-05-03 13:25 - 2015-05-06 16:00     -> [03/06/2015 13:25 - 06/05/2015 15:59]
    
        var dt2, pattern = 8;
        if (mm0 === 0 && mm1 === 0) {
            if (hh0 === 0 && hh1 === 0) {
                if (dd0 == 1 && dd1 == 1) {
                    if (MM0 === 0 && MM1 === 0) {
                        if (yy0 == (yy1 -1)) {
                            pattern = 1;
                        } else {
                            pattern = 2;
                        }
                    } else {
                        // if end month is start month +1 (as current date is /1 00:00) then is case 3
                        dt2 = new Date(dt1.getTime());
                        dt2.setMonth(dt2.getMonth() - 1);
                        if (dt0.getMonth() == dt2.getMonth()) {
                            pattern = 3;
                        } else {
                            pattern = 4;
                        }
                    }
                } else {
                    // if end date is start date +1 (as current time is 00:00) then is case 5
                    dt2 = new Date(dt1.getTime());
                    dt2.setDate(dt2.getDate() - 1);
                    if (dt0.getDate() == dt2.getDate()) {
                        pattern = 5;
                    } else {
                        pattern = 6;
                    }
                }
            } else {
                pattern = 7;
            }
        } else {
            if (mm1 === 0) {
                pattern = 9;
            } else {
                pattern = 8;
            }
        }
        
        var meses = ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"];
        
        switch (pattern) {
            case 9:
                dt1.setHours(dt1.getHours() - 1);
                yy1 = dt1.getFullYear();
                MM1 = dt1.getMonth();
                dd1 = dt1.getDate();
                hh1 = dt1.getHours();
                MM0++; MM1++;
                return  "[" + (dd0<10?"0":"") + dd0 + "/" + (MM0<10?"0":"") + MM0 + "/" + yy0 +
                        " " + (hh0<10?"0":"") + hh0 + ":" + (mm0<10?"0":"") + mm0 +
                        " - " + (dd1<10?"0":"") + dd1 + "/" + (MM1<10?"0":"") + MM1 + "/" + yy1 +
                        " " + (hh1<10?"0":"") + hh1 + ":59]";
            case 8:
                MM0++; MM1++;
                return  "[" + (dd0<10?"0":"") + dd0 + "/" + (MM0<10?"0":"") + MM0 + "/" + yy0 +
                        " " + (hh0<10?"0":"") + hh0 + ":" + (mm0<10?"0":"") + mm0 +
                        " - " + (dd1<10?"0":"") + dd1 + "/" + (MM1<10?"0":"") + MM1 + "/" + yy1 +
                        " " + (hh1<10?"0":"") + hh1 + ":" + (mm1<10?"0":"") + mm1 + "[";
            case 7:
                dt1.setHours(dt1.getHours() - 1);
                yy1 = dt1.getFullYear();
                MM1 = dt1.getMonth();
                dd1 = dt1.getDate();
                hh1 = dt1.getHours();
                MM0++; MM1++;
                return  "[" + (dd0<10?"0":"") + dd0 + "/" + (MM0<10?"0":"") + MM0 + "/" + yy0 +
                        " " + (hh0<10?"0":"") + hh0 + ":00" +
                        " - " + (dd1<10?"0":"") + dd1 + "/" + (MM1<10?"0":"") + MM1 + "/" + yy1 +
                        " " + (hh1<10?"0":"") + hh1 + ":59]";
            case 6:
                dt1.setDate(dt1.getDate() -1);
                yy1 = dt1.getFullYear();
                MM1 = dt1.getMonth();
                dd1 = dt1.getDate();
                MM0++; MM1++;
                return  "[" + (dd0<10?"0":"") + dd0 + "/" + (MM0<10?"0":"") + MM0 + "/" + yy0 +
                        " - " + (dd1<10?"0":"") + dd1 + "/" + (MM1<10?"0":"") + MM1 + "/" + yy1 + "]";
            case 5:
                MM0++; MM1++;
                return  "[" + (dd0<10?"0":"") + dd0 + "/" + (MM0<10?"0":"") + MM0 + "/" + yy0 + "]";
            case 4:
                dt1.setMonth(dt1.getMonth() - 1);
                yy1 = dt1.getFullYear();
                MM1 = dt1.getMonth();
                /*
                MM0++; MM1++;
                return "[" + (MM0<10?"0":"") + MM0 + "/" + yy0 + " - " + (MM1<10?"0":"") + MM1 + "/" + yy1 + "]";
                */
                return "[" + meses[MM0] + "/" + yy0 + " - " + meses[MM1] + "/" + yy1 + "]";
            case 3:
                /*
                MM0++;
                return "[" + (MM0<10?"0":"") + MM0 + "/" + yy0 + "]";
                */
                return "[" + meses[MM0] + "/" + yy0 + "]";
            case 2:
                return "[" + yy0 + " - " + (yy1 - 1) + "]";
            case 1:
                return "[" + yy0 + "]";
        }
    }
    getSubperiod(part) {
        switch (part) {
            case "lastDay":
                let newEnd = new Date(this.endTime);
                newEnd.setDate(newEnd.getDate() - 1);
                return new MinZPeriod("1d", newEnd.getTime());
            default: throw "Unknow period part '" + part + "'";
        }
    }
}

class MinZDataSet {
    constructor(type, query) {
        this._type = type;
        this._query = query;
        this._result = null;
    }
    get type() {return this._type}
    get query() {return this._query}
    get result() {return this._result}
    execute() {
        return new Promise((onOk, onError) => {
            zPost("calculateDataset.dash", {type:this.type, dsConf:this.query}, result => {
                this._result = result;
                onOk(this);
            }, error => onError(error));
        });
    }
    getFieldValue(field, point) {
        switch(field) {
            case "n":       return point.n !== undefined?point.n:0;
            case "min":     return point.min !== undefined?point.min:null;
            case "max":     return point.max !== undefined?point.max:null;
            case "value":   return point.value !== undefined?point.value:null;
            case "avg":     return (point.n > 0 && point.value !== undefined)?point.value / point.n:0;
        }
    }
    buildTimeSerieForField(field, extra) {
        var rows = [];
        this._result.forEach(point => {
            var t = point.localTime;
            var time = (new Date(t.year, t.month - 1, t.day, t.hour, t.minute, 0, 0)).getTime();
            var y = this.getFieldValue(field, point);
            if (this._query.transform) y = this._query.transform(y);
            var seriePoint = {x:time, y:y};
            if (extra) Object.keys(extra).forEach(key => seriePoint[key] = extra[key]);
            rows.push(seriePoint);
        });
        return rows;
    }
    completeTimeSerieWithValue(rows, temporality, fillerValue = 0) {
        if (rows.length < 2) return;
        var rows2 = [rows[0]];
        for (var i=1; i<rows.length; i++) {
            let t0 = rows[i-1].x;
            let t1 = rows[i].x;
            let tx = this.advanceTime(t0, temporality);
            while(tx < t1) {
                rows2.push({x:tx, y:fillerValue});
                tx = this.advanceTime(tx, temporality);
            }
            rows2.push(rows[i]);
        }
        return rows2;
    }
    advanceTime(time, temporality) {
        var d = new Date(time);
        switch(temporality) {
            case "5m":  d.setMinutes(d.getMinutes() + 5);   return d.getTime();
            case "15m": d.setMinutes(d.getMinutes() + 15);  return d.getTime();
            case "30m": d.setMinutes(d.getMinutes() + 30);  return d.getTime();                
            case "1h":  d.setHours(d.getHours() + 1);       return d.getTime();                
            case "6h":  d.setHours(d.getHours() + 6);       return d.getTime();
            case "12h": d.setHours(d.getHours() + 12);      return d.getTime();
            case "1d":  d.setDate(d.getDate() + 1);         return d.getTime();
            case "1M":  d.setMonth(d.getMonth() + 1);       return d.getTime();
            case "3M":  d.setMonth(d.getMonth() + 3);       return d.getTime();
            case "4M":  d.setMonth(d.getMonth() + 4);       return d.getTime();
            case "6M":  d.setMonth(d.getMonth() + 6);       return d.getTime();
            case "1y":  d.setFullYear(d.getFullYear() + 1); return d.getTime();
            default:
                throw("Temporality '" + temporality + "' not handled");
        }
    }
    normalizeTime(temporality, time) {
        var d = new Date(time);
        d.setSeconds(0);
        d.setMilliseconds(0);
        switch(temporality) {
            case "5m":
                d.setMinutes(5 * parseInt(d.getMinutes() / 5));
                return d.getTime();
            case "15m":
                d.setMinutes(15 * parseInt(d.getMinutes() / 15));
                return d.getTime();
            case "30m":
                d.setMinutes(30 * parseInt(d.getMinutes() / 30));
                return d.getTime();
            case "1h":
                d.setMinutes(0);
                return d.getTime();
            case "6h":
                d.setMinutes(0);
                d.setHours(6 * parseInt(d.getHours() / 6));
                return d.getTime();
            case "12h":
                d.setMinutes(0);
                d.setHours(12 * parseInt(d.getHours() / 12));
                return d.getTime();
            case "1d":
                d.setMinutes(0);                
                d.setHours(0);
                return d.getTime();
            case "1M":
                d.setMinutes(0);                
                d.setHours(0);
                d.setDate(1);
                return d.getTime();
            case "3M":
                d.setMinutes(0);                
                d.setHours(0);
                d.setDate(1);
                d.setMonth(3 * parseInt(d.getMonth() / 3));
                return d.getTime();
            case "4M":
                d.setMinutes(0);                
                d.setHours(0);
                d.setDate(1);
                d.setMonth(4 * parseInt(d.getMonth() / 4));
                return d.getTime();
            case "6M":
                d.setMinutes(0);                
                d.setHours(0);
                d.setDate(1);
                d.setMonth(6 * parseInt(d.getMonth() / 6));
                return d.getTime();
            case "1y":
                d.setMinutes(0);                
                d.setHours(0);
                d.setDate(1);
                d.setMonth(0);
                return d.getTime();
            default:
                throw("Temporality '" + temporality + "' not handled");
        }
    }
    getTimeMarks(temporality, startTime, endTime) {
        var dt = new Date(this.normalizeTime(temporality, startTime));
        var rows = [];
        while (dt.getTime() < endTime) {
            rows.push(dt.getTime());
            switch(temporality) {
                case "5m":dt.setMinutes(dt.getMinutes() + 5);break;
                case "15m":dt.setMinutes(dt.getMinutes() + 15);break;
                case "30m":dt.setMinutes(dt.getMinutes() + 30);break;
                case "1h":dt.setHours(dt.getHours() + 1);break;
                case "6h":dt.setHours(dt.getHours() + 6);break;
                case "12h":dt.setHours(dt.getHours() + 12);break;
                case "1d":dt.setDate(dt.getDate() + 1);break;
                case "1M":dt.setMonth(dt.getMonth() + 1);break;
                case "3M":dt.setMonth(dt.getMonth() + 3);break;
                case "4M":dt.setMonth(dt.getMonth() + 4);break;
                case "6M":dt.setMonth(dt.getMonth() + 6);break;
                case "1y":dt.setFullYear(dt.getFullYear() + 1);break;
                default: throw "Temporality " + temporality + " not handled";
            }
        }
        return rows;
    }
    formatDateFromTemporality(temporality, time) {
        var meses = ["ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"];
        var dt = new Date(time);
        var yyyy = dt.getFullYear();
        var MM = meses[dt.getMonth()];
        var dd = dt.getDate(); dd = (dd<10?"0":"") + dd;
        var hh = dt.getHours(); hh = (hh<10?"0":"") + hh;
        var mm = dt.getMinutes(); mm = (mm<10?"0":"") + mm;
        switch(temporality) {
            case "5m":
            case "15m":
            case "30m":return dd + "/" + MM + "/" + yyyy + " " + hh + ":" + mm;
            case "1h":
            case "6h":
            case "12h":return dd + "/" + MM + "/" + yyyy + " " + hh + ":00";
            case "1d":return dd + "/" + MM + "/" + yyyy;
            case "1M":
            case "3M":
            case "4M":
            case "6M":return MM + "/" + yyyy;
            case "1y":return yyyy;
            default: throw "Temporality " + temporality + " not handled";
        }
    }
    buildTimeDimTable(temporality, startTime, endTime, field) {
        var timeMap = {};
        var dims = {};
        this.result.forEach(row => {
            var dt = new Date(row.localTime.year, row.localTime.month - 1, row.localTime.day, row.localTime.hour, row.localTime.minute, 0,0);
            var time = dt.getTime();
            var dimMap = timeMap[time];
            if (!dimMap) {
                dimMap = {};
                timeMap[time] = dimMap;
            }
            dimMap[row.dim.code] = this.getFieldValue(field, row);
            dims[row.dim.code] = row.dim;
        });
        // sort dimension values
        var dimArray = Object.keys(dims).map(code => dims[code]);
        dimArray.sort((d1, d2) => d1.order - d2.order);
        var timeRows = this.getTimeMarks(temporality, startTime, endTime);
        var rows = [];
        timeRows.forEach(time => {
            var row = {time:time, cols:[], formattedTime:this.formatDateFromTemporality(temporality, time)};
            var dimMap = timeMap[time];
            dimArray.forEach(d => {
                row.cols.push(dimMap?dimMap[d.code]:undefined);
            })
            rows.push(row);
        });
        return {rows:rows, cols:dimArray};
    }
    buildTimeSeriesFromDim(field, type) {
        var series = {}; //{code:{name:"", data:[{x:time, y:value}]}};
        this.result.forEach(row => {
            var dt = new Date(row.localTime.year, row.localTime.month - 1, row.localTime.day, row.localTime.hour, row.localTime.minute, 0,0);
            var time = dt.getTime();
            var serie = series[row.dim.code];
            if (!serie) {
                serie = {name:row.dim.name, type:type, data:[], dim:row.dim};
                series[row.dim.code] = serie;
            }
            var seriePoint = {
                x:time,
                y:this.getFieldValue(field, row)
            };
            serie.data.push(seriePoint);
        });
        return Object.keys(series).map(code => series[code]).sort((s1, s2) => s1.dim.order - s2.dim.order);
    }
    buildDimDimTable(field, transform) {
        var hDimMap = {}, vDimMap = {};
        this.result.forEach(row => {
            hDimMap[row.hDim.code] = row.hDim;
            vDimMap[row.vDim.code] = row.vDim;
        });
        var hDim = Object.keys(hDimMap).map(code => hDimMap[code]).sort((d1, d2) => d1.order - d2.order);
        var vDim = Object.keys(vDimMap).map(code => vDimMap[code]).sort((d1, d2) => d1.order - d2.order);
        var hDimIdx = {}, vDimIdx = {};
        hDim.forEach((d, i) => hDimIdx[d.code] = i);
        vDim.forEach((d, i) => vDimIdx[d.code] = i);
        var rows = [];
        this.result.forEach(row => {
            var ih = hDimIdx[row.hDim.code];
            var iv = vDimIdx[row.vDim.code];
            var value = this.getFieldValue(field, row);
            if (transform) value = transform(value);
            rows.push({x:ih, y:iv, xLabel:row.hDim.name, yLabel:row.vDim.name, value:value});
        })
        return {hDim:hDim, vDim:vDim, rows:rows};
    }
}