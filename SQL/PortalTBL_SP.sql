USE [PR_Sixbell]
GO
/****** Object:  Table [dbo].[Imported_sessions]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Imported_sessions](
	[creation_time] [datetime] NOT NULL,
	[seq_num] [bigint] IDENTITY(1,1) NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[unique_identifier] [varchar](64) NULL,
	[direction] [tinyint] NULL,
	[number_of_holds] [smallint] NULL,
	[number_of_conferences] [smallint] NULL,
	[number_of_transfers] [smallint] NULL,
	[total_hold_time] [int] NULL,
	[extension] [int] NULL,
	[time_offset] [smallint] NOT NULL,
	[pbx_login_id] [varchar](255) NULL,
	[switch_name] [varchar](32) NULL,
	[agent_name] [varchar](32) NULL,
	[group_name] [varchar](50) NULL,
	[ani] [varchar](15) NULL,
	[dnis] [varchar](15) NULL,
	[contact_id] [varchar](64) NULL,
	[custom_data_01] [varchar](128) NULL,
	[custom_data_02] [varchar](64) NULL,
	[custom_data_03] [varchar](64) NULL,
	[custom_data_04] [varchar](64) NULL,
	[custom_data_05] [varchar](64) NULL,
	[custom_data_06] [varchar](64) NULL,
	[custom_data_07] [varchar](64) NULL,
	[custom_data_08] [varchar](64) NULL,
	[custom_data_09] [varchar](64) NULL,
	[custom_data_10] [varchar](64) NULL,
	[custom_data_11] [varchar](64) NULL,
	[custom_data_12] [varchar](64) NULL,
	[custom_data_13] [varchar](64) NULL,
	[custom_data_17] [varchar](32) NULL,
	[custom_data_18] [varchar](32) NULL,
	[custom_data_19] [varchar](32) NULL,
	[custom_data_20] [varchar](32) NULL,
	[custom_data_21] [varchar](32) NULL,
	[custom_data_22] [varchar](32) NULL,
	[custom_data_23] [varchar](32) NULL,
	[custom_data_24] [int] NULL,
	[custom_data_25] [int] NULL,
	[internal_dateTime] [datetime] NULL,
 CONSTRAINT [pk_creation_time_seq_num] PRIMARY KEY CLUSTERED 
(
	[seq_num] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Imported_sessions_audio]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Imported_sessions_audio](
	[seq_num] [bigint] NOT NULL,
	[channel_id] [bit] NOT NULL,
	[recording_order] [tinyint] NOT NULL,
	[audio_url] [varchar](300) NOT NULL,
	[start_time] [datetime] NOT NULL,
	[end_time] [datetime] NULL,
	[creation_date] [datetime] NULL,
	[internal_01] [int] NULL,
	[internal_02] [int] NULL,
	[internal_03] [varchar](32) NULL,
	[internal_04] [varchar](32) NULL,
	[internal_05] [varchar](64) NULL,
	[internal_06] [varchar](64) NULL,
	[internal_07] [varchar](128) NULL,
 CONSTRAINT [pk_unique_identifier_channel_id] PRIMARY KEY CLUSTERED 
(
	[seq_num] ASC,
	[recording_order] ASC,
	[channel_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_CLIENTE_CONTRATO_SERVICIO]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_CLIENTE_CONTRATO_SERVICIO](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_CLIENTE] [bigint] NULL,
	[ID_SERVICIO] [bigint] NULL,
	[ACTIVO] [varchar](1) NULL,
	[ID_CONTRATO] [bigint] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_HEADER]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_HEADER](
	[ID_HEADER] [bigint] IDENTITY(1,1) NOT NULL,
	[FORMATO] [varchar](200) NULL,
	[DESCRIPCION] [varchar](200) NULL,
	[TIPO] [varchar](20) NULL,
	[ACTIVO] [char](1) NULL,
	[NOMBRE_HEADER] [varchar](50) NULL,
 CONSTRAINT [PK_TBL_HEADER] PRIMARY KEY CLUSTERED 
(
	[ID_HEADER] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_MAP_DATOS]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_MAP_DATOS](
	[ID_MAPEO] [bigint] NULL,
	[ID_HEADER] [bigint] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_MAPEO_CLI_CONTR_SERV]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_MAPEO_CLI_CONTR_SERV](
	[COD_SERVICIO] [bigint] NOT NULL,
	[ID_CLIENTE] [bigint] NULL,
	[ID_CONTRATO] [bigint] NULL,
	[ACTIVO] [char](1) NULL,
	[NOMBRE_SERVICIO] [varchar](50) NULL,
	[NOMBRE_TABLA] [varchar](50) NULL,
 CONSTRAINT [PK_TBL_MAPEO_CLI_CONTR_SERV] PRIMARY KEY CLUSTERED 
(
	[COD_SERVICIO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_REPORTES]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_REPORTES](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[NOMBRE] [varchar](200) NULL,
	[FILTRO] [varchar](200) NULL,
	[URL] [varchar](200) NULL,
	[ACTIVO] [varchar](1) NULL,
 CONSTRAINT [PK_TBL_REPORTES] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_SERVICIO_REPORTE]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_SERVICIO_REPORTE](
	[ID_SERVICIO] [bigint] NOT NULL,
	[ID_REPORTE] [bigint] NOT NULL,
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
 CONSTRAINT [PK_TBL_SERVICIO_REPORTE] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_SERVICIOS]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_SERVICIOS](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[NOMBRE] [varchar](200) NULL,
	[ACTIVO] [varchar](1) NULL,
 CONSTRAINT [PK_TBL_SERVICIOS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_SESION_USUARIO]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_SESION_USUARIO](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TOKEN] [varchar](40) NOT NULL,
	[FECHA] [datetime] NOT NULL CONSTRAINT [DF_TBL_SESION_USUARIO_FECHA]  DEFAULT (getdate()),
	[LOGIN] [varchar](40) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_TOKEN_RECUPERACION]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_TOKEN_RECUPERACION](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TOKEN] [varchar](40) NOT NULL,
	[FECHA_CREACION] [datetime] NOT NULL CONSTRAINT [DF_TBL_TOKEN_RECUPERACION_FECHA]  DEFAULT (getdate()),
	[LOGIN] [varchar](40) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_USUARIO]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TBL_USUARIO](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[NOMBRES] [varchar](255) NOT NULL,
	[LOGIN] [varchar](50) NOT NULL,
	[PASSWORD] [varchar](255) NOT NULL,
	[ACTIVO] [varchar](1) NOT NULL,
	[ES_ADMIN] [varchar](1) NOT NULL,
	[EMAIL] [varchar](50) NOT NULL,
	[CODIGO_RECUPERACION] [varchar](50) NULL,
	[TIEMPO_RECUPERACION] [date] NULL,
	[TIPO] [int] NULL,
	[CLIENTE] [bigint] NULL,
	[CONTRATO] [bigint] NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TBL_USUARIO_SERVICIOS]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_USUARIO_SERVICIOS](
	[ID_USUARIO] [bigint] NULL,
	[ID_SERVICIO] [bigint] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TBL_USUARIOS_CLIENTE_CONTRATO_SERVICIO_REPORTE]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TBL_USUARIOS_CLIENTE_CONTRATO_SERVICIO_REPORTE](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ID_SERVICIO] [bigint] NULL,
	[ID_REPORTE] [bigint] NULL,
	[ID_USUARIO] [bigint] NULL
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Imported_sessions] ADD  DEFAULT (getutcdate()) FOR [creation_time]
GO
ALTER TABLE [dbo].[Imported_sessions] ADD  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Imported_sessions] ADD  DEFAULT ((0)) FOR [direction]
GO
ALTER TABLE [dbo].[Imported_sessions] ADD  DEFAULT ((0)) FOR [number_of_holds]
GO
ALTER TABLE [dbo].[Imported_sessions] ADD  DEFAULT ((0)) FOR [number_of_conferences]
GO
ALTER TABLE [dbo].[Imported_sessions] ADD  DEFAULT ((0)) FOR [number_of_transfers]
GO
ALTER TABLE [dbo].[Imported_sessions] ADD  DEFAULT ((0)) FOR [total_hold_time]
GO
ALTER TABLE [dbo].[Imported_sessions] ADD  DEFAULT ((0)) FOR [time_offset]
GO
ALTER TABLE [dbo].[Imported_sessions_audio] ADD  DEFAULT ((0)) FOR [channel_id]
GO
ALTER TABLE [dbo].[Imported_sessions_audio] ADD  DEFAULT ((1)) FOR [recording_order]
GO
ALTER TABLE [dbo].[Imported_sessions_audio] ADD  DEFAULT (getutcdate()) FOR [creation_date]
GO
ALTER TABLE [dbo].[TBL_SERVICIO_REPORTE]  WITH CHECK ADD  CONSTRAINT [FK_TBL_REPORTES] FOREIGN KEY([ID_REPORTE])
REFERENCES [dbo].[TBL_REPORTES] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TBL_SERVICIO_REPORTE] CHECK CONSTRAINT [FK_TBL_REPORTES]
GO
ALTER TABLE [dbo].[TBL_SERVICIO_REPORTE]  WITH CHECK ADD  CONSTRAINT [FK_TBL_SERVICIOS] FOREIGN KEY([ID_SERVICIO])
REFERENCES [dbo].[TBL_SERVICIOS] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[TBL_SERVICIO_REPORTE] CHECK CONSTRAINT [FK_TBL_SERVICIOS]
GO
/****** Object:  StoredProcedure [dbo].[PRO_Add_Administrador]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Add_Administrador] 
	@login	    varchar(255),
	@nombres	varchar(255),
	@password	varchar(255),
	@email		varchar(255)
AS
BEGIN
	SET NOCOUNT ON;
	
	insert into TBL_USUARIO(LOGIN, NOMBRES, PASSWORD, ES_ADMIN, ACTIVO, EMAIL) values (@login, @nombres, @password, 'S','S', @email);
			
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Add_Cliente_Contrato_Servicio]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Add_Cliente_Contrato_Servicio] 
	@idCliente BIGINT,
	@idContrato BIGINT,
	@idServicio BIGINT
AS
BEGIN
	SET NOCOUNT ON;
	  INSERT INTO TBL_CLIENTE_CONTRATO_SERVICIO (ID_SERVICIO, ID_CLIENTE, ID_CONTRATO, ACTIVO ) VALUES (@idServicio, @idCliente, @idContrato, 'S');			
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Add_Header]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Add_Header]
	@nombre		varchar(200), 
	@tipo	    varchar(200),
	@formato	varchar(200),
	@descripcion varchar(200),
	@activo      varchar(1)
	
	

AS
BEGIN
	SET NOCOUNT ON;
	  insert into TBL_HEADER(NOMBRE_HEADER, TIPO, FORMATO, DESCRIPCION, ACTIVO) values (@nombre, @tipo,@formato,@descripcion,@activo);		
END





GO
/****** Object:  StoredProcedure [dbo].[PRO_Add_Mapeo_Cliente_Contrato_Servicio]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Add_Mapeo_Cliente_Contrato_Servicio]	
	@codigo BIGINT,
	@nombre VARCHAR(50), 
	@idCliente BIGINT,
	@idContrato BIGINT,
	@tabla VARCHAR(50),
	@activo CHAR(1)
AS
BEGIN
	SET NOCOUNT ON;
	  INSERT INTO TBL_MAPEO_CLI_CONTR_SERV (COD_SERVICIO, NOMBRE_SERVICIO, NOMBRE_TABLA, ID_CLIENTE, ID_CONTRATO, ACTIVO ) VALUES (@codigo, @nombre, @tabla, @idCliente, @idContrato, @activo);			
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Add_Mapeo_Header]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Add_Mapeo_Header] 
	@idMapeo BIGINT,
	@idHeader BIGINT
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO TBL_MAP_DATOS(ID_MAPEO,ID_HEADER)values(@idMapeo, @idHeader);
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Add_PerfilConPrivilegios]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Add_PerfilConPrivilegios] 
	@nombre				VARCHAR(255),
	@activo				VARCHAR(1),
	@listaPrivilegios	VARCHAR(255)
AS
BEGIN
	DECLARE
	  @strValor VARCHAR(255),
	  @intBandera BIT,
	  @intTamano SMALLINT,
	  @idPerfil INT,
	  @idPrivilegio BIGINT,
	  @idPrivilegio1 BIGINT	

	SET NOCOUNT ON;
	
	--insertar en tabla perfil  
	INSERT INTO TBL_PERFIL(nombre, activo) values (@nombre, @activo);  
	select @idPerfil = scope_identity();

	SET @intBandera = 0
	--SET @listaPrivilegios = '1,2,3'
	WHILE @intBandera = 0
		BEGIN
		  BEGIN TRY
			SET @strValor = RIGHT(LEFT(@listaPrivilegios,CHARINDEX(',', @listaPrivilegios,1)-1),CHARINDEX(',', @listaPrivilegios,1)-1)
			PRINT @strValor /* En esta variable se guarda un número */
			SET @idPrivilegio = CONVERT(BIGINT,@strValor)
		    INSERT INTO TBL_PERFIL_PRIVILEGIO(ID_PERFIL, ID_PRIVILEGIO) values (@idPerfil, @idPrivilegio)	
			
			SET @intTamano = LEN(@strValor) 
			SET @listaPrivilegios = SUBSTRING(@listaPrivilegios,@intTamano + 2, LEN(@listaPrivilegios)) 
		  END TRY	
		  BEGIN CATCH 
			PRINT @listaPrivilegios
			SET @intBandera = 1
			SET @idPrivilegio1 = CONVERT(BIGINT,@listaPrivilegios)
			insert into TBL_PERFIL_PRIVILEGIO(ID_PERFIL, ID_PRIVILEGIO) values (@idPerfil, @idPrivilegio1);	
		  END CATCH 
		END
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Add_Reporte]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Add_Reporte] 
	@nombre	    varchar(200),
	@filtro	varchar(200),
	@url	varchar(200),
	@activo      varchar(1)
	
	

AS
BEGIN
	SET NOCOUNT ON;
 

	  insert into TBL_REPORTES(nombre, filtro, url, activo) values (@nombre, @filtro, @url, @activo);
	
			
END




GO
/****** Object:  StoredProcedure [dbo].[PRO_Add_Servicio]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Add_Servicio] 
	@nombre	    varchar(200),
	@activo      varchar(1)
	
	

AS
BEGIN
	SET NOCOUNT ON;
 

	  insert into TBL_SERVICIOS(nombre,activo) values (@nombre, @activo);
	
			
END





GO
/****** Object:  StoredProcedure [dbo].[PRO_Add_Servicio_Reporte]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Add_Servicio_Reporte] 
	@idServicio BIGINT,
	@idReporte BIGINT
AS
BEGIN
	SET NOCOUNT ON;
 

	  insert into TBL_SERVICIO_REPORTE(ID_SERVICIO, ID_REPORTE )values(@idServicio, @idReporte);
	
			
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Add_Usuario]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Add_Usuario] 
	@login	    varchar(255),
	@password	varchar(255),
	@nombres	varchar(255),
	@email      varchar(255),
	@cliente	bigint,
	@contrato	bigint,
	@tipo int,
	@activo     varchar(5),
	@esAdmin    varchar(5)
	

AS
BEGIN
	SET NOCOUNT ON;
	IF @cliente = -1 AND @contrato = -1
		insert into TBL_Usuario(login, password, nombres, email, cliente, contrato, tipo, activo, es_Admin )values (@login, @password, @nombres, @email,null, null, @tipo, @activo, @esAdmin)
	ELSE
		insert into TBL_Usuario(login, password, nombres, email, cliente, contrato, tipo, activo, es_Admin )values (@login, @password, @nombres, @email,@cliente, @contrato, @tipo, @activo, @esAdmin)
	
			
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Add_Usuario_Reportes]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Add_Usuario_Reportes] 
	@idUsuario bigint,
	@idReporte bigint,
	@idServicio bigint
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @idCliente bigint;
	DECLARE @idContrato bigint;

	SELECT @idCliente = [cliente], @idContrato = [contrato] FROM TBL_USUARIO WHERE ID = @idUsuario;
	INSERT INTO TBL_USUARIOS_CLIENTE_CONTRATO_SERVICIO_REPORTE (ID_USUARIO, ID_SERVICIO , ID_REPORTE) VALUES (@idUsuario, @idServicio, @idReporte);
	
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Busca_Clientes_Con_Contratos]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Busca_Clientes_Con_Contratos]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT id_cliente, id_contrato 
	FROM [PR_Sixbell].[dbo].[TBL_CLIENTE_CONTRATO_SERVICIO] WHERE ACTIVO = 'S'
  END





GO
/****** Object:  StoredProcedure [dbo].[PRO_Busca_Clientes_Sin_Contratos]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Busca_Clientes_Sin_Contratos]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT DISTINCT [ID_CLIENTE]
	FROM [PR_Sixbell].[dbo].[TBL_CLIENTE_CONTRATO] WHERE ID_CONTRATO IS NULL AND ACTIVO = 'S'
  END





GO
/****** Object:  StoredProcedure [dbo].[PRO_Busca_Codigo_Privilegios_porPerfil]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Busca_Codigo_Privilegios_porPerfil]
@idPerfil		INT,
@idUsuario      INT
AS
BEGIN
	SET NOCOUNT ON;

	declare 
	@esAdmin varchar(100)

	select @esAdmin = es_admin from TBL_USUARIO WHERE ES_ADMIN='S' and id=@idUsuario  ;

	if(@esAdmin='S')
	select  codigo from  TBL_PRIVILEGIO 

	ELSE 

	select  P.codigo from TBL_PERFIL_PRIVILEGIO PP join TBL_PRIVILEGIO P ON P.ID = PP.ID_PRIVILEGIO 
	where PP.ID_PERFIL = @idPerfil
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Busca_Contratos_Por_Cliente]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [dbo].[PRO_Busca_Contratos_Por_Cliente]
@idCliente	BIGINT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT *	
	FROM TBL_CLIENTE_CONTRATO 
	WHERE ID_CLIENTE = @idCliente AND ACTIVO = 'S' AND ID_CONTRATO IS NOT NULL
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Busca_Headers_Activos]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Busca_Headers_Activos]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT *
	  FROM TBL_HEADER where ACTIVO ='S'
	  ORDER BY ID_HEADER
END




GO
/****** Object:  StoredProcedure [dbo].[PRO_Busca_Mapeo_Cliente_Contrato_Servicio]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Busca_Mapeo_Cliente_Contrato_Servicio]
@codigo		bigint
AS
BEGIN
	SET NOCOUNT ON;
	SELECT * FROM TBL_MAPEO_CLI_CONTR_SERV
	WHERE (COD_SERVICIO = @codigo)
	ORDER BY COD_SERVICIO
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Busca_Reportes]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Busca_Reportes]
@filtro varchar(20)
AS
BEGIN
	SET NOCOUNT ON;

	select id, nombre, filtro, url, activo
	  from TBL_REPORTES where NOMBRE like '%' + @filtro + '%' 
	  order by id
END




GO
/****** Object:  StoredProcedure [dbo].[PRO_Busca_Reportes_Activos]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Busca_Reportes_Activos]
AS
BEGIN
	SET NOCOUNT ON;

	select id, nombre, filtro, url, activo
	  from TBL_REPORTES where ACTIVO ='S'
	  order by id
END




GO
/****** Object:  StoredProcedure [dbo].[PRO_Busca_Servicio]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

--exec PRO_Busca_Perfiles 'nue','','N'
CREATE PROCEDURE [dbo].[PRO_Busca_Servicio]
@idServicio		BIGINT
AS
BEGIN
	SET NOCOUNT ON; 
	SELECT id,nombre,activo FROM TBL_SERVICIOS WHERE ID = @idServicio order by id ;
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Busca_Servicios]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Busca_Servicios]
@filtro varchar(200)
AS
BEGIN
	SET NOCOUNT ON;

	select id, nombre, activo
	  from TBL_SERVICIOS WHERE nombre LIKE '%'+@filtro+'%'
	  order by id
END

GO
/****** Object:  StoredProcedure [dbo].[PRO_Busca_Servicios_Cliente_Contrato]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Busca_Servicios_Cliente_Contrato]
@idCliente varchar(5),
@idContrato varchar(5)

AS
BEGIN
	SET NOCOUNT ON;

	SELECT * FROM TBL_CLIENTE_CONTRATO_SERVICIO  CCS
	INNER JOIN TBL_SERVICIOS S ON CCS.ID_SERVICIO = S.ID
	WHERE CCS.ID_CLIENTE = @idCliente AND CCS.ID_CONTRATO = @idContrato
	 
END

GO
/****** Object:  StoredProcedure [dbo].[PRO_Busca_Servicios_Cliente_Contrato_Man]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Busca_Servicios_Cliente_Contrato_Man]
@idCliente varchar(5),
@idContrato varchar(5)

AS
BEGIN
	SET NOCOUNT ON;
	SELECT * FROM TBL_SERVICIOS S
	LEFT JOIN (SELECT ID_SERVICIO FROM TBL_CLIENTE_CONTRATO_SERVICIO CCS WHERE ID_CLIENTE = @idCliente AND ID_CONTRATO = @idContrato) CCS
	ON S.ID = CCS.ID_SERVICIO
	
	 
END

GO
/****** Object:  StoredProcedure [dbo].[PRO_Busca_Servicios_Con_Reportes]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Busca_Servicios_Con_Reportes]
@filtro VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * FROM TBL_SERVICIOS WHERE ID IN(SELECT DISTINCT ID_SERVICIO FROM [dbo].[TBL_SERVICIO_REPORTE] SR INNER JOIN [dbo].[TBL_SERVICIOS] S ON S.ID = SR.ID_SERVICIO) AND NOMBRE LIKE '%' + @filtro + '%' 
END





GO
/****** Object:  StoredProcedure [dbo].[PRO_Busca_Servicios_porUsuario]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

--exec PRO_Busca_Privilegios_porPerfil 1

CREATE PROCEDURE [dbo].[PRO_Busca_Servicios_porUsuario]
@idUsuario		VARCHAR(20)
AS
BEGIN
	SET NOCOUNT ON;
	SET @idUsuario = CAST(@idUsuario as INT)
	select 
		  isnull(S.id,0) as id, S.nombre,
		  CASE when  UP.ID_SERVICIO is null
		       THEN 'N'
			   else isnull(S.activo, '')
			   end as activo,
			   --  isnull(P.activo, '') as activo,   
			   isnull(UP.ID_SERVICIO,0) as idServicio
	
	from TBL_USUARIO_SERVICIOS UP
	 RIGHT join TBL_SERVICIOS S ON S.ID = UP.ID_SERVICIO AND UP.ID_USUARIO = @idUsuario
	--where PP.ID_PERFIL = @idPerfil
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Busca_Servicios_Sin_Reportes]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Busca_Servicios_Sin_Reportes]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT * FROM TBL_SERVICIOS WHERE ID NOT IN(SELECT DISTINCT ID_SERVICIO FROM [dbo].[TBL_SERVICIO_REPORTE] SR INNER JOIN [dbo].[TBL_SERVICIOS] S ON S.ID = SR.ID_SERVICIO)
END





GO
/****** Object:  StoredProcedure [dbo].[PRO_Delete_Cliente_Contrato_Servicio]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Delete_Cliente_Contrato_Servicio] 
	@idCliente		BIGINT,
	@idContrato		BIGINT
AS
BEGIN
	SET NOCOUNT ON;
    DELETE FROM TBL_CLIENTE_CONTRATO_SERVICIO where ID_CLIENTE=@idCliente AND ID_CONTRATO=@idContrato;
END




GO
/****** Object:  StoredProcedure [dbo].[PRO_Delete_Header]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Delete_Header] 
	@id		INT
AS
BEGIN
	SET NOCOUNT ON;
    DELETE FROM TBL_HEADER where ID_HEADER=@id;
END




GO
/****** Object:  StoredProcedure [dbo].[PRO_Delete_Mapeo_Cliente_Contrato_Servicio]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Delete_Mapeo_Cliente_Contrato_Servicio] 
	@codigo		BIGINT
AS
BEGIN
	SET NOCOUNT ON;
    DELETE FROM TBL_MAPEO_CLI_CONTR_SERV where COD_SERVICIO=@codigo;
END




GO
/****** Object:  StoredProcedure [dbo].[PRO_Delete_Mapeo_Header]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Delete_Mapeo_Header] 
	@idMapeo	BIGINT
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM TBL_MAP_DATOS WHERE ID_MAPEO = @idMapeo;
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Delete_Reporte]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Delete_Reporte] 
	@id		INT
AS
BEGIN
	SET NOCOUNT ON;
    delete from TBL_REPORTES where ID=@id;
END




GO
/****** Object:  StoredProcedure [dbo].[PRO_Delete_Servicio]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Delete_Servicio] 
	@id		INT
AS
BEGIN
	SET NOCOUNT ON;
    delete from TBL_SERVICIOS where ID=@id;
END




GO
/****** Object:  StoredProcedure [dbo].[PRO_delete_sesionUsuario]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_delete_sesionUsuario]
	@token		varchar(255)

AS
BEGIN
	SET NOCOUNT ON;
	delete from TBL_Sesion_Usuario where token=@token
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_delete_TodasLasSesionesDeUsuario]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_delete_TodasLasSesionesDeUsuario]
	@login		varchar(255)

AS
BEGIN
	SET NOCOUNT ON;
	delete from TBL_Sesion_Usuario where login=@login
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_delete_token_recuperacion]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_delete_token_recuperacion]
	@token		varchar(255)
AS
BEGIN
	SET NOCOUNT ON;
	DELETE FROM  TBL_token_recuperacion where token=@token
	END


GO
/****** Object:  StoredProcedure [dbo].[PRO_Delete_Usuario]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Delete_Usuario] 
	@login		varchar(255)
AS
BEGIN
	SET NOCOUNT ON;

	DELETE FROM TBL_SESION_USUARIO WHERE LOGIN = @login;
    DELETE FROM TBL_USUARIO WHERE LOGIN=@login;
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Delete_Usuario_Servicio_Reporte]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Delete_Usuario_Servicio_Reporte] 
	@idUsuario BIGINT
AS
BEGIN
	SET NOCOUNT ON;
    DELETE FROM TBL_USUARIOS_CLIENTE_CONTRATO_SERVICIO_REPORTE where ID_USUARIO=@idUsuario;
END




GO
/****** Object:  StoredProcedure [dbo].[PRO_existeAdministrador]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_existeAdministrador]

AS
BEGIN
	SET NOCOUNT ON;

	select count(*) as n from TBL_USUARIO where activo='S' and es_admin='S'
	
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Get_Administrador]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Get_Administrador] 
	@login		varchar(255)
AS
BEGIN
	SET NOCOUNT ON;

	select count(*) as n from TBL_USUARIO where login = @login
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Get_Header_Por_Mapeo]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [dbo].[PRO_Get_Header_Por_Mapeo]
@idMapeo	BIGINT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT H.ID_HEADER,
		   H.NOMBRE_HEADER,
		   H.DESCRIPCION,
	CASE when  MP.ID_HEADER is null
		       THEN 'N'
			   else isnull(H.ACTIVO, '')
			   end as ACTIVO
	FROM TBL_MAP_DATOS MP 
	RIGHT JOIN TBL_HEADER H
	ON MP.ID_HEADER = H.ID_HEADER AND MP.ID_MAPEO = @idMapeo
	ORDER BY H.ID_HEADER;
END




GO
/****** Object:  StoredProcedure [dbo].[PRO_Get_Headers]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Get_Headers]
@filtro		varchar(255)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT * FROM TBL_HEADER
	WHERE (NOMBRE_HEADER LIKE '%'+lower(@filtro)+'%' OR TIPO LIKE '%'+lower(@filtro)+'%' OR FORMATO LIKE '%'+ lower(@filtro)+'%' OR DESCRIPCION LIKE '%'+lower(@filtro)+'%')
		ORDER BY ID_HEADER
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Get_Mapeo_Cliente_Contrato_Servicio]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Get_Mapeo_Cliente_Contrato_Servicio]
@filtro		varchar(255)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT * FROM TBL_MAPEO_CLI_CONTR_SERV
	WHERE (NOMBRE_SERVICIO LIKE '%'+lower(@filtro)+'%')
	ORDER BY COD_SERVICIO
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Get_Mapeos]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Get_Mapeos]
@filtro		varchar(255)
AS
BEGIN
	SET NOCOUNT ON;
	SELECT * FROM TBL_MAPEO_CLI_CONTR_SERV WHERE COD_SERVICIO IN (SELECT ID_MAPEO FROM TBL_MAP_DATOS) AND NOMBRE_SERVICIO LIKE '%'+lower(@filtro)+'%'
	ORDER BY COD_SERVICIO
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Get_Mapeos_Sin_Header]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Get_Mapeos_Sin_Header]
AS
BEGIN
	SET NOCOUNT ON;
	SELECT * FROM TBL_MAPEO_CLI_CONTR_SERV WHERE COD_SERVICIO NOT IN (SELECT DISTINCT ID_MAPEO FROM [dbo].[TBL_MAP_DATOS])
END





GO
/****** Object:  StoredProcedure [dbo].[PRO_Get_Reportes_Por_Servicio]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [dbo].[PRO_Get_Reportes_Por_Servicio]
@idServicio	BIGINT
AS
BEGIN
	SET NOCOUNT ON;
	select 
		  isnull(P.id,0) as id, P.nombre,
		  P.filtro,
		  P.url, 
		  CASE when  PP.ID_REPORTE is null
		       THEN 'N'
			   else isnull(P.activo, '')
			   end as activo,
		  isnull(PP.ID_REPORTE,0) as idReporte
	
	from TBL_SERVICIO_REPORTE PP RIGHT JOIN TBL_REPORTES P 	
	ON P.ID = PP.ID_REPORTE AND PP.ID_SERVICIO = @idServicio WHERE P.activo = 'S' ORDER BY P.ID 
END




GO
/****** Object:  StoredProcedure [dbo].[PRO_Get_Reportes_Por_Servicio_Adm]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [dbo].[PRO_Get_Reportes_Por_Servicio_Adm]
@idServicio	BIGINT
AS
BEGIN
	SET NOCOUNT ON;
	select 
		  isnull(P.id,0) as id, P.nombre,
		  P.filtro,
		  P.url, 
		  CASE when  PP.ID_REPORTE is null
		       THEN 'N'
			   else isnull(P.activo, '')
			   end as activo,
		  isnull(PP.ID_REPORTE,0) as idReporte
	
	from TBL_SERVICIO_REPORTE PP INNER JOIN TBL_REPORTES P 	
	ON P.ID = PP.ID_REPORTE AND PP.ID_SERVICIO = @idServicio WHERE P.activo = 'S' ORDER BY P.ID 
END




GO
/****** Object:  StoredProcedure [dbo].[PRO_Get_Reportes_Por_Usuario]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [dbo].[PRO_Get_Reportes_Por_Usuario]
@idServicio	BIGINT,
@idUsuario BIGINT
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT P.ID, P.NOMBRE, P.ACTIVO, SR.ID AS ID_CCSR FROM TBL_SERVICIO_REPORTE PP INNER JOIN TBL_REPORTES P 	
	ON P.ID = PP.ID_REPORTE AND PP.ID_SERVICIO = @idServicio 
	LEFT JOIN ( SELECT * FROM TBL_USUARIOS_CLIENTE_CONTRATO_SERVICIO_REPORTE WHERE ID_USUARIO =@idUsuario AND ID_SERVICIO =@idServicio) SR 
	ON SR.ID_REPORTE = PP.ID_REPORTE 
	WHERE P.activo = 'S' ORDER BY P.ID 
END
	




GO
/****** Object:  StoredProcedure [dbo].[PRO_Get_Reportes_Servicios_Usuario]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Get_Reportes_Servicios_Usuario]
@idUsuario BIGINT,
@idServicio BIGINT

AS
BEGIN
	SET NOCOUNT ON;

	SELECT R.ID, R.NOMBRE
	FROM [PR_Sixbell].[dbo].[TBL_USUARIOS_CLIENTE_CONTRATO_SERVICIO_REPORTE] CCSR 
	INNER JOIN [PR_Sixbell].[dbo].[TBL_REPORTES] R 
	ON CCSR.ID_REPORTE = R.ID 
	WHERE CCSR.ID_USUARIO = @idUsuario AND CCSR.ID_SERVICIO = @idServicio
END

GO
/****** Object:  StoredProcedure [dbo].[PRO_Get_Servicios_Usuario]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Get_Servicios_Usuario]
@idUsuario BIGINT

AS
BEGIN
	SET NOCOUNT ON;
	SELECT DISTINCT(S.ID),S.NOMBRE FROM TBL_USUARIOS_CLIENTE_CONTRATO_SERVICIO_REPORTE CCSR INNER JOIN TBL_SERVICIOS S ON CCSR.ID_SERVICIO = S.ID  WHERE ID_USUARIO = @idUsuario
END

GO
/****** Object:  StoredProcedure [dbo].[PRO_getPrivilegiosSesion]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_getPrivilegiosSesion]
@token		varchar(80)
AS
BEGIN
	SET NOCOUNT ON;

     select u.login as login, u.activo as activo, u.es_admin as admin 
                  from TBL_Usuario u, TBL_Sesion_Usuario s 
                 where u.email = s.login 
                   and s.token = @token
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_GetSesionUsuario]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_GetSesionUsuario]
@login		varchar(400)
AS
BEGIN
	SET NOCOUNT ON;

select token as token from TBL_Sesion_Usuario 
	where LOGIN=@login 
	
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_getUsuario]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [dbo].[PRO_getUsuario]
@login       varchar(255)
AS
BEGIN
	SET NOCOUNT ON;

   select id as id, nombres as nombre, email as email, activo as activo, es_admin as admin 
                  from tbl_Usuario where email=@login
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_GetUsuarioLoginoEmail]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================



CREATE PROCEDURE [dbo].[PRO_GetUsuarioLoginoEmail]
@login		varchar(200)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT ID, NOMBRES, LOGIN, PASSWORD, EMAIL, ACTIVO, ES_ADMIN, CLIENTE,CONTRATO, TIPO
	  FROM TBL_USUARIO
	where EMAIL=@login 
	
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_getUsuarios]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_getUsuarios]
@filtro		varchar(255),
@cliente	varchar(5),
@contrato	varchar(5)

AS
BEGIN
	SET NOCOUNT ON;
	IF @cliente ='' AND @contrato =''
		 select 
			USR.id as id,
			USR.login as login, 
			USR.nombres as nombre, 
			USR.EMAIL as email, 
			USR.activo as activo,
			USR.CONTRATO as contrato,
			USR.CLIENTE as cliente,
			USR.TIPO as tipo, 
			USR.es_admin as admin

		FROM tbl_Usuario USR
	 
		WHERE (lower(USR.LOGIN) like '%'+lower(@filtro)+'%' or lower(USR.NOMBRES) like '%'+ lower(@filtro)+'%' or lower(USR.EMAIL) like '%'+lower(@filtro)+'%')
		ORDER BY USR.NOMBRES
	ELSE
		select 
			USR.id as id,
			USR.login as login, 
			USR.nombres as nombre, 
			USR.EMAIL as email, 
			USR.activo as activo,
			USR.CONTRATO as contrato,
			USR.CLIENTE as cliente,
			USR.TIPO as tipo, 
			USR.es_admin as admin

		FROM tbl_Usuario USR
	 
		WHERE (lower(USR.LOGIN) like '%'+lower(@filtro)+'%' or lower(USR.NOMBRES) like '%'+ lower(@filtro)+'%' or lower(USR.EMAIL) like '%'+lower(@filtro)+'%') AND CLIENTE = @cliente AND CONTRATO = @contrato
		ORDER BY USR.NOMBRES



END



GO
/****** Object:  StoredProcedure [dbo].[PRO_InfoRecuperacion]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================



CREATE PROCEDURE [dbo].[PRO_InfoRecuperacion]
@token		varchar(400)
AS
BEGIN
	SET NOCOUNT ON;

	select r.login as login, u.nombres as nombre
                  from TBL_Token_Recuperacion r, TBL_Usuario u
                 where r.token = @token
                   and u.email = r.login
                   and u.activo = 'S'
	
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Pwd_recuperacion]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Pwd_recuperacion]
	@login		varchar(255),
	@pwd		varchar(255)
AS
BEGIN
	SET NOCOUNT ON;
	update TBL_Usuario set password=@pwd where login=@login
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Save_Administrador]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Save_Administrador] 
	@login		varchar(255)
AS
BEGIN	
	SET NOCOUNT ON;

    update TBL_USUARIO SET activo='S', es_admin='S'
	where login=@login;
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Save_Reporte]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Save_Reporte] 
	@nombre	    varchar(200),
	@filtro	varchar(200),
	@url	varchar(200),
	@activo     varchar(1)
AS
BEGIN
	SET NOCOUNT ON;
	  insert into TBL_REPORTES(nombre, filtro, url, activo)values (@nombre,@filtro, @url, @activo)		
END




GO
/****** Object:  StoredProcedure [dbo].[PRO_Set_Admin_Pwd]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Set_Admin_Pwd]
	@email		varchar(255),
	@pwd		varchar(255)
AS
BEGIN
	SET NOCOUNT ON;
	update TBL_USUARIO set password=@pwd where email=@email;
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Set_Codigo_Recuperacion_Admin]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Set_Codigo_Recuperacion_Admin] 
@email		varchar(255),
@codigo		varchar(100),
@tiempo		datetime
AS
BEGIN
	SET NOCOUNT ON;
	update TBL_USUARIO set codigo_recuperacion=@codigo, tiempo_recuperacion=@tiempo where email=@email;
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Set_Sesion]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Set_Sesion]
	@login      varchar(50),
	@token	    varchar(50)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO TBL_SESION_USUARIO(TOKEN,FECHA,login)
	VALUES(@token,getdate(),@login)
    
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Token_Recuperacion]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Token_Recuperacion]
	@token      varchar(50),
	@login	    varchar(50)
AS
BEGIN
	SET NOCOUNT ON;
	INSERT INTO TBL_TOKEN_RECUPERACION(TOKEN,FECHA_CREACION,login)
	VALUES(@token,getdate(),@login)
    
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Update_Cliente_Reporte_Servicio]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Update_Cliente_Reporte_Servicio]
	@idServicio BIGINT,
	@idContrato BIGINT,
	@idCliente BIGINT
AS
BEGIN
	DECLARE @contador INT;
	SET NOCOUNT ON;
		SET @contador = (SELECT COUNT(*) 
		FROM [PR_Sixbell].[dbo].[TBL_CLIENTE_CONTRATO_SERVICIO] 
		WHERE ID_CLIENTE = @idCliente AND ID_CONTRATO= @idContrato AND ID_SERVICIO = @idServicio )
		IF (@contador = 0)
			INSERT INTO TBL_CLIENTE_CONTRATO_SERVICIO (ID_SERVICIO, ID_CLIENTE, ID_CONTRATO) VALUES (@idServicio,@idCliente, @idContrato);
		ELSE
			DELETE FROM TBL_CLIENTE_CONTRATO_SERVICIO WHERE ID_SERVICIO = @idServicio AND ID_CLIENTE = @idCliente AND ID_CONTRATO = @idContrato;
END





GO
/****** Object:  StoredProcedure [dbo].[PRO_Update_Header]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Update_Header]
	@id bigint,
	@nombre varchar(200),
    @tipo	varchar(200),
	@formato varchar(200),
	@descripcion varchar(200),
	@activo     varchar(1)
AS
BEGIN
	SET NOCOUNT ON;
		UPDATE TBL_HEADER SET NOMBRE_HEADER=@nombre, TIPO=@tipo,FORMATO=@formato, DESCRIPCION=@descripcion, ACTIVO=@activo WHERE ID_HEADER = @id	
END





GO
/****** Object:  StoredProcedure [dbo].[PRO_Update_Mapeo_Cliente_Contrato_Servicio]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Update_Mapeo_Cliente_Contrato_Servicio]
	@codigo BIGINT,
	@nombre VARCHAR(50), 
	@idCliente BIGINT,
	@idContrato BIGINT,
	@tabla VARCHAR(50),
	@activo CHAR(1)
AS
BEGIN
	SET NOCOUNT ON;
	  UPDATE TBL_MAPEO_CLI_CONTR_SERV SET NOMBRE_SERVICIO = @nombre, NOMBRE_TABLA = @tabla, ID_CLIENTE = @idCliente, ID_CONTRATO = @idContrato, ACTIVO = @activo 
	  WHERE COD_SERVICIO = @codigo;			
END






GO
/****** Object:  StoredProcedure [dbo].[PRO_Update_Mapeo_Header]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Update_Mapeo_Header]
	@idMapeo BIGINT,
	@idHeader BIGINT
AS
BEGIN
	DECLARE @contador INT;
	SET NOCOUNT ON;
		SET @contador = (SELECT COUNT(*) 
		FROM [PR_Sixbell].[dbo].[TBL_MAP_DATOS] 
		WHERE ID_MAPEO = @idMapeo AND ID_HEADER = @idHeader)

		IF (@contador = 0)
			INSERT INTO TBL_MAP_DATOS (ID_MAPEO, ID_HEADER) VALUES (@idMapeo, @idHeader);
		ELSE
			DELETE FROM TBL_MAP_DATOS WHERE ID_MAPEO = @idMapeo AND ID_HEADER = @idHeader;
END





GO
/****** Object:  StoredProcedure [dbo].[PRO_Update_Reporte]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Update_Reporte]
	@id bigint,
    @nombre	varchar(200),
	@filtro	    varchar(200),	
	@url      varchar(200),
	@activo     varchar(1)
AS
BEGIN
	SET NOCOUNT ON;
		update TBL_REPORTES set nombre=@nombre, filtro=@filtro, url=@url, activo=@activo where id = @id	
END




GO
/****** Object:  StoredProcedure [dbo].[PRO_Update_Reportes_Por_Servicio]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Update_Reportes_Por_Servicio]
	@idServicio BIGINT,
	@idReporte BIGINT
AS
BEGIN
	DECLARE @contador INT;
	SET NOCOUNT ON;
		SET @contador = (SELECT COUNT(*) 
		FROM [PR_Sixbell].[dbo].[TBL_SERVICIO_REPORTE] 
		WHERE ID_SERVICIO = @idServicio AND ID_REPORTE = @idReporte)

		IF (@contador = 0)
			INSERT INTO TBL_SERVICIO_REPORTE (ID_SERVICIO, ID_REPORTE) VALUES (@idServicio, @idReporte);
		ELSE
			DELETE FROM TBL_SERVICIO_REPORTE WHERE ID_SERVICIO = @idServicio AND ID_REPORTE = @idReporte;
END





GO
/****** Object:  StoredProcedure [dbo].[PRO_Update_Servicio]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Update_Servicio]
	@id bigint,
    @nombre	varchar(200),
	@activo     varchar(1)
AS
BEGIN
	SET NOCOUNT ON;
		update TBL_SERVICIOS set nombre=@nombre, activo=@activo where id = @id	
END





GO
/****** Object:  StoredProcedure [dbo].[PRO_Update_Usuario]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Update_Usuario] 
    @nombres	varchar(255),
	@login	    varchar(255),	
	@email      varchar(255),
	@cliente	bigint,
	@contrato	bigint,
	@tipo		int,
	@activo     varchar(5),
	@esAdmin    varchar(5)
	

AS
BEGIN
	SET NOCOUNT ON;
		UPDATE TBL_Usuario SET nombres=@nombres, email=@email, activo=@activo, tipo=@tipo, cliente=@cliente, contrato =@contrato, es_admin=@esAdmin WHERE login = @login
			
END



GO
/****** Object:  StoredProcedure [dbo].[PRO_Update_Usuario_Reportes]    Script Date: 02/04/2019 11:17:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[PRO_Update_Usuario_Reportes]
	@idServicio BIGINT,
	@idReporte BIGINT,
	@idUsuario BIGINT
AS
BEGIN
	DECLARE @contador INT;
	SET NOCOUNT ON;

	DECLARE @idCliente bigint;
	DECLARE @idContrato bigint;


	SELECT @idCliente = [cliente], @idContrato = [contrato] FROM TBL_USUARIO WHERE ID = @idUsuario;
	
		SET @contador = (SELECT COUNT(*) 
		FROM [PR_Sixbell].[dbo].[TBL_USUARIOS_CLIENTE_CONTRATO_SERVICIO_REPORTE] 
		WHERE ID_USUARIO = @idUsuario AND ID_REPORTE = @idReporte AND ID_SERVICIO = @idServicio)
		
		SELECT @contador
		IF (@contador = 0)
			INSERT INTO [TBL_USUARIOS_CLIENTE_CONTRATO_SERVICIO_REPORTE] (ID_SERVICIO, ID_REPORTE, ID_USUARIO) VALUES (@idServicio, @idReporte, @idUsuario);
		ELSE
			DELETE FROM [TBL_USUARIOS_CLIENTE_CONTRATO_SERVICIO_REPORTE] WHERE ID_USUARIO = @idUsuario AND ID_SERVICIO = @idServicio AND ID_REPORTE = @idReporte;
END



GO

INSERT INTO [dbo].[TBL_USUARIO]
           ([NOMBRES]
           ,[LOGIN]
           ,[PASSWORD]
           ,[ACTIVO]
           ,[ES_ADMIN]
           ,[EMAIL]
           ,[CODIGO_RECUPERACION]
           ,[TIEMPO_RECUPERACION]
           ,[TIPO]
           ,[CLIENTE]
           ,[CONTRATO])
     VALUES
           ('Administrador General'
           ,'allendesfelipe01@gmail.com'
           ,'$2a$08$vAv2DP/eG.U79QkX30wkEOMVwI9jSmowHOz7BdHOZUbKwUVAI2f0q'
           ,'S'
           ,'S'
           ,'allendesfelipe01@gmail.com'
           ,
           ,
           ,0
           ,
           ,)
GO
