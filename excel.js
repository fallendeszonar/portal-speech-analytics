'use strict';
global.confPath = process.argv.length > 2?process.argv[2]:__dirname + "/config.json";
const express = require('express');
const app = express();
const CronJob = require('cron').CronJob;
const exlJs = require('exceljs');
const fs = require('fs');
const fe = require('fs-extra');
const shell = require('shelljs');
const SQLServer = require('./services/SQLServer').SQLServer;
const port = process.env.PORT || 3003;
const excelConfig = require("./services/Config").Config.instance.getConfig().excelConf;

app.listen(port, ()=>{
    console.log(`API REST escuchando en http://localhost:${port}`);
});
// procesaXls();
servicio();
// insertJson();
async function servicio(){// definicion de funcion servicio
    let ejec = false;//variable de control de ejecución
    const job = new CronJob('0 * * * * *', ()=>{// creación del cron cada $tanto tiempo
        procesaXls();// funcion que invoca el cron
    });
    if(ejec==false){// si no se esta ejecutando el cron, se empieza a ejecutar
        ejec = true;
        try{
            job.start();// comienzo del trabajo
            ejec = false;
        }catch(err){
            ejec = false;
            throw err;
        }
    }else{//una vez finalizado del trabajo, la ejecucion pasa a ser falsa para iniciar una nueva
        ejec = false;
    }
    
}
/*

actual:
    node --max-old-space-size=8192 --stack-size=1968 .\excel.js

Se ejecuta este comando en consola debido a que el archivo requiere un mayor espacio en memoria RAM para
poder ser procesado correctamente

*/
function confServicio(){// obtener serivicio parametro 
    return excelConfig.ServicioParam;
}

async function procesaXls(){// procesa por cada excel que se encuentre en el directorio
    let exfolder = excelConfig.directorios.Excels;
    let dirProc = excelConfig.directorios.Procesos;
    let inn = excelConfig.directorios.In;
    let out = excelConfig.directorios.Out;
    let errd = excelConfig.directorios.Err;
    //Comprueba existencia de los directorios
    if(!existeDir(exfolder)) throw Error('No existe el directorio'+exfolder+'');
    if(!existeDir(dirProc)) throw Error('No existe el directorio'+dirProc+'');
    if(!existeDir(inn)) throw Error('No existe el directorio'+inn+'');
    if(!existeDir(out)) throw Error('No existe el directorio'+out+'');
    if(!existeDir(errd)) throw Error('No existe el directorio'+errd+'');
    //Comienza a leer los archivos excels
    var wb = new exlJs.Workbook();
    let campos = [];
    let valores = [];
    var servicesOk = [];
    var columnasCoincidentes = [];
    var servicioParam = confServicio();
    var saltos = excelConfig.Saltos;
    let ready = false;
    if(servicesOk.length<1){
        servicesOk = await losServiciosTBL()
        .catch((err)=>{
            regErr("No hay servicio asociado a los headers: "+campos, err);
            console.log('No hay servicio asociado a los headers: '+campos, err);
            return err;
        });
    }
    fs.readdirSync(exfolder).forEach(async (file)=>{//Lectura de directorio
        if(!validaNombre(file))throw Error("No se puede leer el nombre del archivo: "+file);
        if(!moverArchivo(exfolder, file)) throw Error("No se puede procesar el archivo, esta siendo utilziado");
        wb.xlsx.readFile(exfolder+"/"+file)
        .then(async function(){//Lectura de archivo
            console.log("Procesando Archivo: "+file);
            var ws = wb.getWorksheet();
            let k = 0;
            ws.eachRow({includeEmpty:true}, async function(row, rowNumber){//Lectura de cada Fila
                if(rowNumber==saltos.deFila){
                    row.values.forEach(async(r,i)=>{// r es el campo/header
                        if(r!=undefined){
                            if(servicesOk.length>=1){
                                try{
                                    servicesOk.map(async(service, index)=>{
                                        let c = limpiaArray(r);// Header "limpio"
                                        if(service.nombreHead == c){
                                            columnasCoincidentes.push(index);
                                            console.log("Header '"+r+"' pertenece al Servicio: '"+service.nombreServ+"'");
                                            ready = true; 
                                        }else if(columnasCoincidentes.length<1){
                                            console.log("Header No pertenece al Servicio: "+service.nombreServ);
                                        }
                                    });
                                }catch(err){
                                    regErr("No hay Headers para el servicio: "+servicesOk, err);
                                    console.log(err);
                                    return err;
                                }
                            }else{
                                regErr("No hay registros de un servicio para los headers: "+campos);
                                console.log('No hay registros de un servicio para los headers');
                                return(Error('No hay registros de un servicio para los headers'));
                            }
                            if(ready==true){
                                if(typeof r !==undefined){
                                    campos.push(r);
                                }
                            }else{
                                console.log("No están todos los requisitos para continuar");
                                return false;
                            }
                        }else{
                            regErr("Archivo no contiene Servicio: '"+servicioParam+"'");
                            console.log("Archivo no contiene Servicio: '"+servicioParam+"'");
                            return(Error("Archivo no contiene Servicio: '"+servicioParam+"'"));
                        }
                    });
                }else if(rowNumber>saltos.deFila){
                    valores[k]=[];
                    for(let i = 0; i<row.values.length; i++){
                        if(i!=saltos.columna1 && i!=saltos.columna2 && i!=saltos.columna3){
                            valores[k].push(row.values[i]);
                        }
                    }
                    k++;
                }
            });
            var reto = [campos, valores, servicesOk];
            return (reto);
        })
        .then(async(res1)=>{
            var r1 = res1[0];
            var r2 = res1[1];
            var r3 = res1[2];
            var db = new SQLServer();
            var query ="";
            var nombreTBL = r3[0].nombreTabla;//primer servicio, ver dinamismo
            r2.map((val, i)=>{
                query = "INSERT INTO "+nombreTBL+"(id";
                r1.map((campo, j)=>{
                    let c = limpiaArray(campo);
                    query += ", "+c+"";
                    if(j==campos.length-1){
                        query += ") ";
                        return true;
                    }
                });
                query += "VALUES("+(i+1);

                for(let i=0; i<campos.length; i++){
                    if(typeof val[i] === undefined || val[i] == undefined || val[i]==null){
                        val[i] = "";
                    }
                    query += ", "+val[i]+"";
                    if(i==campos.length-1){
                        query += ")";
                        console.log(query.toString());//Insertar en la BD
                        return true;
                    }
                }
            });
        })
        .catch((err)=>{
            regErr("Error al Leer el Archivo: "+file, err);
            console.error(err);
            return err;
        });
    });
}

function regErr(string, dErr, postState){// Funcion que registra errores, con posibilidad de ser mostrados
    var erroresLog = {};
    var errores = [];
    var date = new Date();
    if(string!=null){
        erroresLog={
            nombreError: string,
            detalleError: dErr,
            fecha: date
        }
        errores.push(erroresLog);
    }
    if(postState!=false && errores.length>0){
        console.log("Errores: "+errores);
        return errores;
    }
}

async function losServiciosTBL(){//Funcion que busca Servicios en las BDs
    var db = new SQLServer();
    var servicesOk = [];
    let query = "SELECT s.COD_SERVICIO cod_servicio,"+
    " m.ID_MAPEO id_mapeo, m.ID_HEADER id_header, s.NOMBRE_TABLA nombre_tabla,"+
    " h.NOMBRE_HEADER nombre_header, h.FORMATO formato, h.TIPO tipo"+
    " FROM TBL_MAP_DATOS m, TBL_MAPEO_CLI_CONTR_SERV s, TBL_HEADER h"+
    " WHERE m.ID_HEADER=h.ID_HEADER";
    await db.executeDirect(query.toString())
    .then((res)=>{
        res.map((r,i)=>{
            let codServ = r.cod_servicio.replace(/ /g,'');
            let idMap = r.id_mapeo.replace(/ /g,'');
            let idHead = r.id_header;
            let nombreHead = r.nombre_header.replace(/ /g,'');
            let nombreTabla = r.nombre_tabla.replace(/ /g,'');
            if(codServ==1){
                let objServ = {
                    nombreServ: "S"+codServ,
                    codServ: codServ,
                    nombreTabla: nombreTabla,
                    idMapeo: idMap,
                    idHead: idHead,
                    nombreHead: nombreHead,
                    fomato: r.formato,
                    tipo: r.tipo
                }
                servicesOk.push(objServ);
                return true;
            }else{
                let objServ = {
                    nombreServ: "S"+codServ,
                    codServ: codServ,
                    idMapeo: idMap,
                    idHead: idHead,
                    nombreHead: nombreHead,
                    fomato: r.formato,
                    tipo: r.tipo
                }
                servicesOk.push(objServ);
                return true;
            }
        });
    })
    .catch((err)=>{
        regErr("No hay servicio asociado",err);
        console.log("No hay servicio asociado",err);
        return err;
        // process.exit(1);
    });
    return servicesOk;
}

function limpiaArray(campos){// funcion que "limpia" cadena de strings
    if(!campos) throw Error("Valores del arreglo a limpiar son nulos");
    var specialChars = "!@#$^&%*()+=-[]\/{}|:<>?.";
    var camposLimpios;
    for(let i = 0; i<specialChars.length; i++){
        camposLimpios = campos.replace(new RegExp("\\" + specialChars[i], 'gi'), '');
        camposLimpios = camposLimpios.toLowerCase();
        camposLimpios = camposLimpios.replace(/ /g,"_");
        camposLimpios = camposLimpios.replace(/á/gi,"a");
        camposLimpios = camposLimpios.replace(/é/gi,"e");
        camposLimpios = camposLimpios.replace(/í/gi,"i");
        camposLimpios = camposLimpios.replace(/ó/gi,"o");
        camposLimpios = camposLimpios.replace(/ú/gi,"u");
        camposLimpios = camposLimpios.replace(/ñ/gi,"ni");
    }
        
        
    return camposLimpios?camposLimpios:"No hay campos";
}

function moverArchivo(dir, file){// funcion valida si se puede mover un archivo
    let ok;
    if(existeArchivo(dir+"\\"+file)){// validar si hay archivos en el directorio
        ok = fe.move(dir+"\\"+file, dir+"\\"+file).then(() =>{
            console.log("Se puede mover el archivo");
            return true;
        }).catch(err => {
            console.log("Error al mover el archivo "+file+", no se procesa", "-Error: "+err); 
            return false;
        });
    }else{
        ok = false;
    }
    return ok;
}

function existeArchivo(file) {// funcion validar archivo existente en el directorio
    let flag = true;
    try {//existe el archivo
        fs.accessSync(file, fs.F_OK);
        console.log("Existe el archivo "+file);
    } catch (e) { //no existe archivo
        flag = false;
        console.log("No Existe el archivo "+file);
    }
    return flag;
}


function validaNombre(fileName) {// funcion para validar si el archivo es un XLSX
    let esValido = true;
    if (fileName == null) {
        esValido = false;
        console.log("El nombre de archivo '" + fileName + "' es inválido. El formato debe ser, por ejemplo: Ejemplo_1.xlsx o Ejemplo1.xlsx");
        throw ("El nombre de archivo '" + fileName + "' es inválido. El formato debe ser, por ejemplo: Ejemplo_1.xlsx o Ejemplo1.xlsx");
    }
    fileName = fileName.toUpperCase();
    if (!fileName.endsWith(".XLSX")) {
        esValido = false;
        console.log("El nombre de archivo '" + fileName + "' es inválido. El formato debe ser, por ejemplo: Ejemplo_1.xlsx o Ejemplo1.xlsx");
        throw ("El nombre de archivo '" + fileName + "' es inválido. El formato debe ser, por ejemplo: Ejemplo_1.xlsx o Ejemplo1.xlsx");
    }
    return esValido;
}

function existeDir(dir){// comprueba que el directorio exista, sino -> lo crea
    if(!fs.existsSync(dir)){
        shell.mkdir('-p', dir);
        console.log("Se creo el directorio "+dir);
        return true;
    }else return true;
}
